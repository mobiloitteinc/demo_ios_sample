//
//  NewOrderVC.m
//  Active Club Solutions -iPAD App
//
//  Created by Krishna Kant Kaira on 29/09/14.
//  Copyright (c) 2014 Mobiloitte Technologies. All rights reserved.
//

#import "NewOrderVC.h"
#import <StarIO_Extension/StarIoExtManager.h>
#import "OrderPanelTableViewCell.h"
#import "OrderPanelCategoryTableViewCell.h"
#import "CustomerSelectionPopOver.h"
#import "SelectInventoryViewController.h"
#import "ActivateGiftCardVCViewController.h"
#import "MPOPHelper.h"
#import "MarqueeLabel.h"
#import "SendItemsViewController.h"
#import "SplitItemsViewController.h"
#import "ItemDetailsViewController.h"


@interface NewOrderVC() <UIPopoverControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UITextFieldDelegate, RequestResponseHandlerDelegate, CustomPopUpDelegate, InventorySelectedDelegate, stateDelegate, ScannerDelegate,StarIoExtManagerDelegate, SplitItemsDelegate, ItemDetailsDelegate, AddGiftCardDelegate> {
    
    NSInteger selectedOrderIndex;
    NSString *locationID;
    NSString *locationName;
    
    BOOL isForSaveOrder;
    BOOL isForCustomerInformation;
    BOOL isGetAllInvetoryWithButtonClick;
    BOOL isForSendItems;
    BOOL isAddGiftCard;
    
    NSString *giftCardNumber;
    NSString *availablePoints;

}

@property (nonatomic, strong) UIPopoverController *popOver;

@property (nonatomic, strong) IBOutlet UITextField *textField_customerSearch;
@property (nonatomic, strong) IBOutlet ACSButton *button_delete;
@property (nonatomic, strong) IBOutlet ACSButton *button_saveOrder;
@property (nonatomic, strong) IBOutlet ACSButton *button_checkOut;
@property (nonatomic, strong) IBOutlet UIButton *button_inventory;
@property (nonatomic, strong) IBOutlet UIButton *button_activateGiftCard;
@property (strong, nonatomic) IBOutlet ACSButton *button_sendItems;

//created outlet for Refresh Button
@property (nonatomic, strong) IBOutlet UIButton *buttonRefreshDevices;
@property (nonatomic, strong) IBOutlet UITableView *tableView_newOrder;
@property (nonatomic, strong) IBOutlet UITableView *tableView_inventory;
@property (nonatomic, strong) IBOutlet UILabel *label_totalQuantity;
@property (nonatomic, strong) IBOutlet UILabel *label_totalAmount;
@property (nonatomic, strong) IBOutlet UILabel *label_membershipClub;
//added label for Customer notes
@property (nonatomic, strong) IBOutlet MarqueeLabel *label_customerNotes;
@property (strong, nonatomic) IBOutlet UILabel *label_subTotal;

@property (nonatomic, strong) NSMutableArray *array_inventryData;
@property (nonatomic, strong) NSMutableArray *array_newOrderData;
@property (nonatomic, assign) NSInteger currentSelectedIndex;

@property (nonatomic, strong) AppUserData *appUser;
@property (nonatomic, strong) TBL_UserOrder *userNewOrder;

@property (nonatomic, strong) completionBlock barcodeScannerBlock;

/**
 * holds the scanner's current status
 */
@property (nonatomic, assign) mPopScannerStatus barCodeScannerStatus;
@property (nonatomic, strong) NSString *selectedCustomerName;
@property (nonatomic, strong) NSString *selectedCustomerClub;
@property (nonatomic, strong) MBProgressHUD *progressHUD;

- (IBAction)CommonButtonAction:(id)sender;
- (IBAction)sendToButtonAction:(id)sender;

@end

@implementation NewOrderVC

#pragma mark - * * * * ViewController life cycle methods * * * *
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setupContollerDefaults];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[self navigationItem] setHidesBackButton:YES];
    [[self navigationController] setNavigationBarHidden:YES];
    //set navigationBar's title
    [[APPDELEGATE baseController] setScreenTitle:@"New Order"];
    [[APPDELEGATE baseController] setSelectedIndex:0];
    [self.button_activateGiftCard setTitle:[[[AppUserData sharedInstance] selectedLocation] useactiveclubgiftcards] ? @"Add/Reload Gift Card" : @"Activate/Reload Gift Card" forState:UIControlStateNormal];
    [self.button_activateGiftCard setHidden:![[[AppUserData sharedInstance] selectedLocation] enablegiftcards]];

    //Bool to Manage mPop Bar Code Scanner Initialization
    isGetAllInvetoryWithButtonClick = NO;
    
    if (self.selectedOrderID) {
        [self refrshViewWithOrder:self.selectedOrderID];
    }
    else {
        [self refreshViewForNewFreshOrder];
    }
    
    if (self.selectedCustomerItem) {
        [self refreshCustomerInfoWithObj:self.selectedCustomerItem];
        
        //if membership is not available, fetch customer detail
        if ([self.userNewOrder.userOrderToCustomer.customerMemberShipClub length] < 1) {
            isForCustomerInformation = YES;
            [self callWebAPIMethod:lookupCustomer withrequest:[SoapEnvelope getRequsetForLookupCustomerForCustomerWithID:self.userNewOrder.userOrderToCustomer.customerID]];
        }
    }
    
    AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appdelegate.scannerObject setScannerDelegate:self];
    
    StarIoExtManagerBarcodeReaderStatus status = [[APPDELEGATE starIoExtManager] barcodeReaderStatus];
    if (status == StarIoExtManagerBarcodeReaderStatusConnect) {
        self.barCodeScannerStatus = mPopScannerStatus_ReaderConnect;
    }else{
        self.barCodeScannerStatus = mPopScannerStatus_ReaderDisconnect;
    }
    if (self && self.mPopBarCodeStatusDelegate && [self.mPopBarCodeStatusDelegate respondsToSelector:@selector(updateStatusmPopBarCodeScanner:)])
        [self.mPopBarCodeStatusDelegate updateStatusmPopBarCodeScanner:self.barCodeScannerStatus];
    
    [[APPDELEGATE starIoExtManager] setDelegate:self];

    availablePoints = @"0";

}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    if (self.presentedViewController)
        [self dismissViewControllerAnimated:NO completion:nil];
    
    [self dismissPopOverIfVisible];
    [self saveNewOrdersToPendingOrder];
    
    AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appdelegate.scannerObject setScannerDelegate:nil];
    [[APPDELEGATE starIoExtManager] disconnect];
    [APPDELEGATE starIoExtManager].delegate = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - * * * * ScannerDelegate Method * * *
/**
 * callback to get the decoded data from scanner
 */
-(void)scannerDidReceivedDecodedData:(NSString*)decodedData {
    giftCardNumber = decodedData;
    [self callWebAPIMethod:lookupInventory withrequest:[SoapEnvelope getRequsetForLookupInventoryWithSKU:decodedData]];
}

#pragma mark - * * * * AddGiftCardDelegate Method * * *
- (void)dismissAddGiftCardControllerDelegateMethod:(AddGiftCardInfo *)giftCardObj withAmount:(NSString *)amount{
    self.giftCardObj = giftCardObj;
    self.amount = amount;
    isAddGiftCard = YES;
    [self callWebAPIMethod:calculateForCheckOut withrequest:[SoapEnvelope calculateOrderForCheckOut:self.userNewOrder withGiftCardData:self.giftCardObj withGiftCardAmount:self.amount]];
}

#pragma mark - * * * * Other Helper Methods * * *
-(void)setupContollerDefaults {
    
    self.array_newOrderData = [NSMutableArray array];
    self.array_inventryData = [NSMutableArray array];
    
    self.appUser = [AppUserData sharedInstance];
    
    [[self tableView_inventory] setDelegate:self];
    [[self tableView_inventory] setDataSource:self];
    [self.tableView_inventory setBackgroundView:nil];
    [self.tableView_inventory setBackgroundColor:[UIColor clearColor]];
    
    [[self textField_customerSearch] setDelegate:self];
    [[self tableView_newOrder] setDelegate:self];
    [[self tableView_newOrder] setDataSource:self];
    
    [self.button_checkOut setExclusiveTouch:YES];
    [self.button_delete setExclusiveTouch:YES];
    [self.button_saveOrder setExclusiveTouch:YES];
    [self.button_inventory setExclusiveTouch:YES];
    [self.button_sendItems setExclusiveTouch:YES];
    [self hideSendItemsButton];
}

-(void)hideSendItemsButton {
 
    BOOL disbleLacationPrinter = ![[[AppUserData sharedInstance]selectedLocation]enableLocationPrinters];
    if (disbleLacationPrinter) {
        
        [self.button_sendItems setHidden:YES];
        [self.label_subTotal setFrame:CGRectMake(19, 8, 250, 39)];
    }
}
-(void)refrshViewWithOrder: (NSString*)orderID {
    
    locationID = [kDef valueForKey:kLocationID];
    locationName = [kDef valueForKey:kLocationName];
    
    self.userNewOrder = [[[DBHelper instance] getOrderWithOrderId:orderID forUser:self.appUser.loggedInUser] firstObject];
    
    if (self.userNewOrder == nil) {
        self.userNewOrder = [[DBHelper instance] getNewOrderObjectForUser:self.appUser.loggedInUser withLocation:locationID andLocationName:locationName];
    }
    else {
        locationID = self.userNewOrder.locationID;
        locationName = self.userNewOrder.locationName;
    }
    
    self.userNewOrder.isNewOrder = YES;
    
    self.selectedCustomerName = self.userNewOrder.userOrderToCustomer.customerFullName;
    self.selectedCustomerClub = self.userNewOrder.userOrderToCustomer.customerMemberShipClub;
    
    [self.textField_customerSearch setText:(([self.selectedCustomerName isIdenticaleTo:@"POS Customer"]) ? @"" : self.selectedCustomerName)];
    [self.label_membershipClub setText:self.selectedCustomerClub];
    [self.label_customerNotes setText:[self.textField_customerSearch.text isEqualToString:@""] ? @"": self.userNewOrder.userOrderToCustomer.customerNotes];
    
    self.currentSelectedIndex = -1;
    selectedOrderIndex = -1;
    
    [self.array_newOrderData removeAllObjects];
    [self.array_newOrderData  addObjectsFromArray:[[self.userNewOrder userOrderToProduct] allObjects]];
    // If you refund the bottom item it puts the refunded item above it.  All items in green should stay at the top of the list, then the items in blue show up beneath them, then any new items added in black should be the last items shown. Mixing and matching the colors is confusing in the list.  They should be grouped together.
    //Green -> Blue -> Black
    //isSwap -> isRefunded -> shouldPayFor
    NSArray *sortDescriptor = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"isSwap" ascending:NO], [NSSortDescriptor sortDescriptorWithKey:@"isRefunded" ascending:YES], [NSSortDescriptor sortDescriptorWithKey:@"shouldPayFor" ascending:YES],[NSSortDescriptor sortDescriptorWithKey:@"sequenceNumber" ascending:NO], nil];
    
    
    [self.array_newOrderData sortUsingDescriptors:sortDescriptor];
    
    BOOL isPruductAvailable = (self.array_newOrderData.count > 0);
    
    [self.button_checkOut setEnabled:isPruductAvailable];
    [self.button_delete setEnabled:isPruductAvailable];
    [self.button_saveOrder setEnabled:isPruductAvailable];
    [self.button_sendItems setEnabled:isPruductAvailable];
    
    [self updateTotalPriceAndQuantity];
    
    //make service call to get inventories
    [self makeWebAPICallToFetchAllInventry];
    
    [self.tableView_inventory reloadData];
    [self.tableView_newOrder reloadData];
}

-(void)refreshViewForNewFreshOrder {
    
    locationID = [kDef valueForKey:kLocationID];
    locationName = [kDef valueForKey:kLocationName];
    
    self.selectedCustomerName = kDefaultcustomer;
    self.selectedCustomerClub = kDefaultcustomerClub;
    
    [self.textField_customerSearch setText:(([self.selectedCustomerName isIdenticaleTo:@"POS Customer"]) ? @"" : self.selectedCustomerName)];
    // 04-04-2016    If I choose a customer for an order that has notes and then process the order, when it navigates back the new order screen the customer is blank which is correct but the notes are still there from the previous order.
    [self.label_customerNotes setText:[self.textField_customerSearch.text isEqualToString:@""] ? @"": self.userNewOrder.userOrderToCustomer.customerNotes];
    [self.label_membershipClub setText:self.selectedCustomerClub];
    self.currentSelectedIndex = -1;
    selectedOrderIndex = -1;
    
    [self saveNewOrdersToPendingOrder];
    
    [self refreshUserNewOrderProducts];
    
    //make service call to get inventories
    [self makeWebAPICallToFetchAllInventry];
}

-(void)checkForOrderUpdate:(id)delegate ifNotPerformSelector:(SEL)selctor withObj:(id)obj {
    
    [self.label_customerNotes setText:@""];
    if ((self.userNewOrder.recalculateStatus.integerValue == 1) && (((self.userNewOrder.openOrderID.length > 0) && ([self.userNewOrder.openOrderID rangeOfString:@"TA" options:NSCaseInsensitiveSearch].location != NSNotFound)) || ((self.userNewOrder.orderID.length > 0) && ([self.userNewOrder.orderID rangeOfString:@"TA" options:NSCaseInsensitiveSearch].location != NSNotFound)))) {
        
        UIAlertView * confirmation = [[UIAlertView alloc] initWithTitle:@"Do you want to save the changes for this customer?" message:@"" cancelButtonTitle:nil otherButtonTitles:[NSArray arrayWithObjects:@"YES", @"NO",nil] onCancel:^{
            
        } onDismiss:^(NSInteger buttonIndex) {
            switch (buttonIndex) {
                case 0: {
                    //NO
                    if (selctor)
                        [delegate performSelector:selctor withObject:obj afterDelay:0];
                }
                    break;
                    
                case -1: {
                    //YES
                    [self checkAndSaveorderToSharedForSaveOnly:YES:NO];
                }
                    break;
                    
                default:
                    break;
            }
        }];
        
        [confirmation show];
        [APPDELEGATE setVisibleAlertView:confirmation];
        [APPDELEGATE setDismissButtonIndex:0];
    }
    else {
        
        if (selctor)
            [delegate performSelector:selctor withObject:obj afterDelay:0];
    }
}

-(void)checkAndSaveorderToSharedForSaveOnly:(BOOL)isForSaveOrceOnly :(BOOL)isForSendItemsOnly{
    
    if ([[[AppUserData sharedInstance] selectedLocation] enablecashdrawerfunctions]) {
        NSString *assignedCashDrawer = [[NSUserDefaults standardUserDefaults] valueForKey:kCashDrawerId];
        BOOL canCompleteOrder = (assignedCashDrawer && [assignedCashDrawer length]>0);
        if (canCompleteOrder) {
            
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"POS_Station"] length] < 1) {
                [APPDELEGATE displayAlertWithTitle:@"" andMessage:kErrorMessage_NoPOSStation];
                return;
            }
            
            isForSaveOrder = isForSaveOrceOnly;
            isForSendItems = isForSendItemsOnly;
            [self callWebAPIMethod:calculateForCheckOut withrequest:[SoapEnvelope calculateOrderForCheckOut:self.userNewOrder withGiftCardData:nil withGiftCardAmount:self.amount]];
        }
        else [APPDELEGATE displayAlertWithTitle:@"" andMessage:kErrorMessage_NoDrawerAssigned];
    }else {
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"POS_Station"] length] < 1) {
            [APPDELEGATE displayAlertWithTitle:@"" andMessage:kErrorMessage_NoPOSStation];
            return;
        }
        
        isForSaveOrder = isForSaveOrceOnly;
        isForSendItems = isForSendItemsOnly;
        [self callWebAPIMethod:calculateForCheckOut withrequest:[SoapEnvelope calculateOrderForCheckOut:self.userNewOrder withGiftCardData:nil withGiftCardAmount:self.amount]];
    }
}

-(void)saveNewOrdersToPendingOrder {
    
    //save every user order which is in status isNewOrder = YES.
    NSArray *usersNewOrders = [[[self.appUser.loggedInUser appUserToUserOrder] allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isNewOrder == YES"]];
    
    for (TBL_UserOrder *newOrder in usersNewOrders) {
        
        if ([[newOrder.userOrderToProduct allObjects] count] > 0) {
            newOrder.isNewOrder = NO;
        }
        else {
            [self.appUser.loggedInUser removeAppUserToUserOrderObject:newOrder];
        }
    }
    [[DBHelper instance] saveContext];
}

-(void)refreshUserNewOrderProducts {
    
    [self.array_newOrderData removeAllObjects];
    
    self.userNewOrder = [[DBHelper instance] getNewOrderObjectForUser:self.appUser.loggedInUser withLocation:locationID andLocationName:locationName];
    [self.array_newOrderData  addObjectsFromArray:[[self.userNewOrder userOrderToProduct] allObjects]];
    
    // If you refund the bottom item it puts the refunded item above it.  All items in green should stay at the top of the list, then the items in blue show up beneath them, then any new items added in black should be the last items shown. Mixing and matching the colors is confusing in the list.  They should be grouped together.
    //Green -> Blue -> Black
    //isSwap -> isRefunded -> shouldPayFor
    NSArray *sortDescriptor = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"isSwap" ascending:NO], [NSSortDescriptor sortDescriptorWithKey:@"isRefunded" ascending:YES], [NSSortDescriptor sortDescriptorWithKey:@"shouldPayFor" ascending:YES],[NSSortDescriptor sortDescriptorWithKey:@"sequenceNumber" ascending:NO], nil];
    
    [self.array_newOrderData sortUsingDescriptors:sortDescriptor];
    isAddGiftCard = NO;
    [self.tableView_newOrder reloadData];
    BOOL isPruductAvailable = (self.array_newOrderData.count > 0);
    [self.button_checkOut setEnabled:isPruductAvailable];
    [self.button_delete setEnabled:isPruductAvailable];
    [self.button_saveOrder setEnabled:isPruductAvailable];
    [self.button_sendItems setEnabled:isPruductAvailable];
    
    [self.tableView_inventory reloadData];
    [self.tableView_newOrder reloadData];
    
    [self updateTotalPriceAndQuantity];
}

-(void)updateTotalPriceAndQuantity {
    
    long double totalPrice = 0.0f;
    long long totalQty = 0;
    
    for (TBL_Product  *product in self.array_newOrderData) {
        if (product.shouldPayFor) {
            if ([product.priceOverride doubleValue] > 0)
                totalPrice += [product.productQuantity longLongValue] * [product.priceOverride doubleValue];
            else
                totalPrice += [product.productQuantity longLongValue] * [product.productUnitPrice doubleValue];
            
            totalQty += [product.productQuantity longLongValue];
        }
        else if (product.isRefunded) {
            if ([product.priceOverride doubleValue] > 0)
                totalPrice -= [product.productQuantity longLongValue] * [product.priceOverride doubleValue];
            else
                totalPrice -= [product.productQuantity longLongValue] * [product.productUnitPrice doubleValue];
        }
    }
    
    self.userNewOrder.orderItemQty = [NSString stringWithFormat:@"%lld",totalQty];
    self.userNewOrder.orderTotalPrice = [NSString stringWithFormat:@"%Lf",totalPrice];
    
    self.label_totalAmount.text = [NSString stringWithFormat:@"$%.2f",[self.userNewOrder.orderTotalPrice doubleValue]];
    self.label_totalQuantity.text = [NSString stringWithFormat:@"%lld", [self.userNewOrder.orderItemQty longLongValue]];
}

-(void)refreshCustomerInfoWithObj:(id)customer {
    
    if ([customer isKindOfClass:[CustomerItem class]]) {
        
        CustomerItem *customerObj = (CustomerItem*)customer;
        self.userNewOrder.userOrderToCustomer.customerID = customerObj.customerID;
        self.userNewOrder.userOrderToCustomer.customerFullName = customerObj.name;
        self.userNewOrder.userOrderToCustomer.customerFirstName = customerObj.firstname;
        self.userNewOrder.userOrderToCustomer.customerLastName = customerObj.lastname;
        self.userNewOrder.userOrderToCustomer.customerMemberShipClub = customerObj.memberships;
        self.userNewOrder.userOrderToCustomer.customerNotes = customerObj.notes;
        availablePoints = customerObj.pointsBalance;

    }
    else {
        
        self.userNewOrder.userOrderToCustomer.customerID = @"";
        self.userNewOrder.userOrderToCustomer.customerFullName = ((customer && [customer length]) ? customer : kDefaultcustomer);
        self.userNewOrder.userOrderToCustomer.customerFirstName = [self.userNewOrder.userOrderToCustomer.customerFullName getFirstName];
        self.userNewOrder.userOrderToCustomer.customerLastName = [self.userNewOrder.userOrderToCustomer.customerFullName getLastName];
        
        self.userNewOrder.userOrderToCustomer.customerMemberShipClub = kDefaultcustomerClub;
        self.userNewOrder.userOrderToCustomer.customerNotes = @"";
        availablePoints = @"0";
    }
    
    self.selectedCustomerName = self.userNewOrder.userOrderToCustomer.customerFullName;
    self.selectedCustomerClub = self.userNewOrder.userOrderToCustomer.customerMemberShipClub;
    
    [self.textField_customerSearch setText:(([self.selectedCustomerName isIdenticaleTo:@"POS Customer"]) ? @"" : self.selectedCustomerName)];
    [self.label_membershipClub setText:self.selectedCustomerClub];
    //    [self.label_customerNotes setText:self.userNewOrder.userOrderToCustomer.customerNotes];
    // 04-04-2016    If I choose a customer for an order that has notes and then process the order, when it navigates back the new order screen the customer is blank which is correct but the notes are still there from the previous order.
    [self.label_customerNotes setText:[self.textField_customerSearch.text isEqualToString:@""] ? @"" : self.userNewOrder.userOrderToCustomer.customerNotes];
    [self.userNewOrder setRecalculateStatus:@"1"];
    [[DBHelper instance] saveContext];
}

-(void)addNewInventoryItem:(InventryStockItems *)inventory toOrder:(TBL_UserOrder*)order{
    
    NSString *insertStatus = [[DBHelper instance] insertNewProductToOrder:self.userNewOrder withData:inventory];
    
    if ([insertStatus isEqualToString:kAddedd]) {
        self.userNewOrder.recalculateStatus = @"1";
        [self refreshUserNewOrderProducts];
    }
    else {
        
        [APPDELEGATE displayAlertWithTitle:@"" andMessage:(([inventory.onhand integerValue]>0) ? [NSString stringWithFormat:@"Maximum available stock limit reached for '%@'.",inventory.productName] : [NSString stringWithFormat:@"'%@' is out of stock right now.",inventory.productName])];
    }
}

//Present Picker for Device selection for printing
-(void)showStatePickerViewWithContentOfArray:(NSArray *)deviceList andSender:(UIButton *)sender{
    
    NSMutableArray *mArrayDeviceList = [NSMutableArray array];
    
    for (PortInfo *portInfoObj in deviceList) {
        [mArrayDeviceList addObject:portInfoObj];
    }
    if (deviceList == nil) {
        
        [self dismissPopOverIfVisible];
        return;
    }
    
    [self dismissPopOverIfVisible];
    
    //drop down button
    StatePickerViewController *statePickerViewController=[[StatePickerViewController alloc] initWithNibName:@"StatePickerViewController" bundle:nil];
    statePickerViewController.delegate=self;
    statePickerViewController.dataArray = mArrayDeviceList;
    statePickerViewController.preferredContentSize = CGSizeMake(320.0, 270.0);
    self.popOver=[[UIPopoverController alloc] initWithContentViewController:statePickerViewController];
    [[self.popOver contentViewController] setPreferredContentSize:CGSizeMake(320.0, 270.0)];
    [self.popOver presentPopoverFromRect:[[[APPDELEGATE navigationController] view] bounds] inView:[[APPDELEGATE navigationController] view] permittedArrowDirections:0 animated:YES];
}


#pragma mark - * * * * IBAction * * * *
- (IBAction)CommonButtonAction:(id)sender {
    
    NSInteger index= [sender tag];
    [self.view endEditing:YES];
    
    switch (index) {
            
        case 54: {
            isGetAllInvetoryWithButtonClick = YES;
            //Inventory Button Action
            [self makeWebAPICallToFetchAllInventry];
        }
            break;
            
        case 55: {
            //Check out
            [self checkAndSaveorderToSharedForSaveOnly:NO:NO];
        }
            break;
            
        case 56: {
            //Delete Order
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete this order?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES", @"NO", nil];
            alert.tag = 9;
            [alert show];
            
            [APPDELEGATE setVisibleAlertView:alert];
            [APPDELEGATE setDismissButtonIndex:1];
        }
            break;
            
        case 57: {
            //Select Inventory Button Action
            
            SelectInventoryViewController *selectInventoryVC=[[SelectInventoryViewController alloc]initWithNibName:@"SelectInventoryViewController" bundle:nil];
            selectInventoryVC.delegate = self;
            [selectInventoryVC setModalPresentationStyle:UIModalPresentationFormSheet];
            [selectInventoryVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            selectInventoryVC.preferredContentSize = CGSizeMake(675.0, 400.0);
            [self presentViewController:selectInventoryVC animated:NO completion:nil];
        }
            break;
            
        case 58: {
            if ([[[AppUserData sharedInstance] selectedLocation] useactiveclubgiftcards]) {
                
                //Add/Reload Gift Card
                AddGiftCardViewController *addGiftCard=[[AddGiftCardViewController alloc]initWithNibName:@"AddGiftCardViewController" bundle:nil];
                
                if (([self.userNewOrder.userOrderToCustomer.customerID length] > 0) || ([self.userNewOrder.userOrderToCustomer.customerFirstName length] > 0)) {
                    CustomerItem *customer = [[CustomerItem alloc] init];
                    customer.customerID = self.userNewOrder.userOrderToCustomer.customerID;
                    customer.firstname = self.userNewOrder.userOrderToCustomer.customerFirstName;
                    customer.lastname = self.userNewOrder.userOrderToCustomer.customerLastName;
                    addGiftCard.customer = customer;
                }
                addGiftCard.delegate = self;
                [addGiftCard setModalPresentationStyle:UIModalPresentationFormSheet];
                [addGiftCard setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                addGiftCard.preferredContentSize = CGSizeMake(710.0, 440.0);
                
                [self presentViewController:addGiftCard animated:NO completion:nil];
            }else {
                //Activate/Reload Gift Card
                ActivateGiftCardVCViewController *activateGiftCard=[[ActivateGiftCardVCViewController alloc]initWithNibName:@"ActivateGiftCardVCViewController" bundle:nil];
                
                if (([self.userNewOrder.userOrderToCustomer.customerID length] > 0) || ([self.userNewOrder.userOrderToCustomer.customerFirstName length] > 0)) {
                    CustomerItem *customer = [[CustomerItem alloc] init];
                    customer.customerID = self.userNewOrder.userOrderToCustomer.customerID;
                    customer.firstname = self.userNewOrder.userOrderToCustomer.customerFirstName;
                    customer.lastname = self.userNewOrder.userOrderToCustomer.customerLastName;
                    activateGiftCard.customer = customer;
                }
                [activateGiftCard setModalPresentationStyle:UIModalPresentationFormSheet];
                [activateGiftCard setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                activateGiftCard.preferredContentSize = CGSizeMake(690.0, 440.0);
                [self presentViewController:activateGiftCard animated:NO completion:nil];
            }
        }
            break;
            
        case 60: {
            //Save Order
            if (self.array_newOrderData.count > 0) {
                [self.label_customerNotes setText:@""];
                [self checkAndSaveorderToSharedForSaveOnly:YES:NO];
            }
        }
            break;
        case 61:{
            //            [self getBarcodeScannerDevicesAndInitiateScaner:sender];
            
        }
            break;
        default:
            break;
    }
}

- (IBAction)sendToButtonAction:(id)sender {
    
    if (self.array_newOrderData.count > 0) {
        [self.label_customerNotes setText:@""];
        [self checkAndSaveorderToSharedForSaveOnly:NO:YES];
    }
}

- (void)scannerDidScan:(completionBlock)compltion{
    if ([[APPDELEGATE starIoExtManager]connect]) {
        self.barcodeScannerBlock = compltion;
    }
}

#pragma mark - Barcode Scanner Delegate methtods
- (void)didBarcodeDataReceive:(NSData *)data {
    
    if([APPDELEGATE starIoExtManager].delegate != nil) {
        NSMutableString *text = [NSMutableString stringWithString:@""];
        
        const uint8_t *p = [data bytes];
        
        for (int i = 0; i < data.length; i++) {
            uint8_t ch = *(p + i);
            
            if(ch >= 0x20 && ch <= 0x7f) {
                [text appendFormat:@"%c", (char) ch];
            }
            else if (ch == 0x0d) {
                if (self.barcodeScannerBlock) {
                    self.barcodeScannerBlock(text,nil);
                }
            }
        }
    }
}

- (void)didBarcodeReaderImpossible{
    [self updateScannerStatusFormPopScanner: mPopScannerStatus_ReaderImpossible];
}

- (void)didBarcodeReaderConnect{
    [self updateScannerStatusFormPopScanner: mPopScannerStatus_ReaderConnect];
}

- (void)didBarcodeReaderDisconnect{
    [self updateScannerStatusFormPopScanner:mPopScannerStatus_ReaderDisconnect];
}

-(mPopScannerStatus)getmPopBarCodeScannerStatus {
    
    return self.barCodeScannerStatus;
}
-(void)updateScannerStatusFormPopScanner:(mPopScannerStatus)scannerStatus {
    self.barCodeScannerStatus = scannerStatus;
    if (self && self.mPopBarCodeStatusDelegate && [self.mPopBarCodeStatusDelegate respondsToSelector:@selector(updateStatusmPopBarCodeScanner:)])
        [self.mPopBarCodeStatusDelegate updateStatusmPopBarCodeScanner:self.barCodeScannerStatus];
}

#pragma mark - * * * * InventorySelectedDelegate * * * *
-(void)inventorySelected:(InventryStockItems*)newInventoryAdded {
    
    if (newInventoryAdded) {
        
        NSString *insertStatus = [[DBHelper instance] insertNewProductToOrder:self.userNewOrder withData:newInventoryAdded];
        
        if ([insertStatus isEqualToString:kAddedd]) {
            self.userNewOrder.recalculateStatus = @"1";
            [self refreshUserNewOrderProducts];
        }
        else {
            
            [APPDELEGATE displayAlertWithTitle:@"" andMessage:(([newInventoryAdded.onhand integerValue]>0) ? [NSString stringWithFormat:@"Maximum available stock limit reached for '%@'.",newInventoryAdded.productName] : [NSString stringWithFormat:@"'%@' is out of stock right now.",newInventoryAdded.productName])];
        }
    }
}

#pragma mark - * * * * Custome TableCell Button action * * * *
-(void)deleteProduct:(id)sender {
    [self.view endEditing:YES];
    
    selectedOrderIndex = [sender tag];
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete this item?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES", @"NO", nil];
    alert.tag= 19;
    [alert show];
    
    [APPDELEGATE setVisibleAlertView:alert];
    [APPDELEGATE setDismissButtonIndex:1];
}

-(void)increaseProductCount :(id)sender {
    [self.view endEditing:YES];
    
    self.userNewOrder.recalculateStatus = @"1";
    
    TBL_Product*product = [self.array_newOrderData objectAtIndex:[sender tag]];
    product.productQuantity = [NSString stringWithFormat:@"%d",(int)([product.productQuantity integerValue]+1)];
    [self refreshUserNewOrderProducts];
}

-(void)reduceProductCount :(id)sender {
    [self.view endEditing:YES];
    
    TBL_Product*product = [self.array_newOrderData objectAtIndex:[sender tag]];
    if ([product.productQuantity integerValue] == 1)
        [[DBHelper instance] deleteProduct:product fromOrder:self.userNewOrder];
    else
        product.productQuantity = [NSString stringWithFormat:@"%d",(int)([product.productQuantity integerValue]-1)];
    
    self.userNewOrder.recalculateStatus = @"1";
    [self refreshUserNewOrderProducts];
}

-(void)refundProductItem :(id)sender {
    
    [self.view endEditing:YES];
    
    TBL_Product*product = [self.array_newOrderData objectAtIndex:[sender tag]];
    if (![[DBHelper instance] insertNewRefundItemTo:self.userNewOrder withProduct:product])
        [APPDELEGATE displayAlertWithTitle:@"" andMessage:@"Maximum refund quantity reached."];
    else
        self.userNewOrder.recalculateStatus = @"1";
    
    [self refreshUserNewOrderProducts];
}

-(void)splitProductItem:(UIButton *)sender {
    [self.view endEditing:YES];
    TBL_Product *productItem = [self.array_newOrderData objectAtIndex:sender.tag];
    SelectedInventoryItem *inventory = [[SelectedInventoryItem alloc] init];
    inventory.Name = productItem.productName;
    inventory.Quantity = productItem.productQuantity;
    selectedOrderIndex = sender.tag;
    SplitItemsViewController *splitVC = [[SplitItemsViewController alloc] initWithNibName:@"SplitItemsViewController" bundle:nil];
    [splitVC setInventoryItem:inventory];
    [splitVC setDelegate:self];
    [splitVC setPreferredContentSize:CGSizeMake(537.0, 300.0)];
    [splitVC setModalPresentationStyle:UIModalPresentationFormSheet];
    [splitVC setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self presentViewController:splitVC animated:YES completion:nil];
}

#pragma mark - SplitItemsDelegate method
-(void)splitSlectedItemsWithOldQuantity:(NSString *)oldQuantity andNewQuantity:(NSString *)newQuantity {
    TBL_Product *productItem = [self.array_newOrderData objectAtIndex:selectedOrderIndex];
    productItem.productQuantity = oldQuantity;
    
    TBL_Product *objProducts = (TBL_Product *)[NSEntityDescription insertNewObjectForEntityForName:@"TBL_Product" inManagedObjectContext:[[DBHelper instance] managedObjectContext]];
    objProducts.productID = productItem.productID;
    objProducts.productName = productItem.productName;
    objProducts.productQuantity = newQuantity;
    objProducts.productUnitPrice = productItem.productUnitPrice;
    objProducts.isSwap = productItem.isSwap;
    objProducts.isRefunded = productItem.isRefunded;
    objProducts.shouldPayFor = productItem.shouldPayFor;
    objProducts.itemDiscount = productItem.itemDiscount;
    objProducts.inventryID = productItem.inventryID;
    objProducts.onHand = productItem.onHand;
    objProducts.stockValue = productItem.stockValue;
    objProducts.priceOverride = productItem.priceOverride;
    objProducts.isNewProductAdded = productItem.isNewProductAdded;
    objProducts.sequenceNumber = productItem.sequenceNumber;
    objProducts.productDetails = productItem.productDetails;
//    objProducts.productNote = productItem.productNote;
    [self.userNewOrder addUserOrderToProductObject:objProducts];
    [self.array_newOrderData addObject:objProducts];
    selectedOrderIndex = -1;
    [[DBHelper instance] saveContext];
    self.userNewOrder.recalculateStatus = @"1";
    [self.tableView_newOrder reloadData];
}

#pragma mark - * * * * UIAlertDelegate Methods * * * *
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        
        switch (alertView.tag) {
                
            case 19: {
                //Delete item from current order
                self.userNewOrder.recalculateStatus = @"1";
                [[DBHelper instance] deleteProduct:[self.array_newOrderData objectAtIndex:selectedOrderIndex] fromOrder:self.userNewOrder];
                [self refreshUserNewOrderProducts];
            }
                break;
                
            case 9: {
                //Delete Order
                
                if ([self.userNewOrder.orderID rangeOfString:@"TA" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                    [self callWebAPIMethod:deleteOrder withrequest:[SoapEnvelope getSOAPRequestToDeleteOrder:self.userNewOrder.orderID]];
                }
                else {
                    [[DBHelper instance] deleteOrder:self.userNewOrder forUser:self.appUser.loggedInUser];
                    [self refreshViewForNewFreshOrder];
                }
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - * * * * UITableviewDelegateMethods * * * *
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger numberOfSections = 1;
    
    if ([tableView isEqual:self.tableView_inventory]) {
        numberOfSections = [self.array_inventryData count];
    }
    
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numberOfRows = 0;
    
    if ([tableView isEqual:self.tableView_inventory]) {
        
        if (section == self.currentSelectedIndex) {
            
            InventryCategory *objCategory = [self.array_inventryData objectAtIndex:section];
            numberOfRows = [objCategory.inventryProductsArray count]+1;
        }
        else
            numberOfRows = ([self.array_inventryData count] ? 1 : 0);
    }
    else {
        numberOfRows = [self.array_newOrderData count];
    }
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([tableView isEqual:self.tableView_inventory]) {
        
        if (indexPath.row == 0) {
            
            static NSString *ParentCellIdentifier=@"OrderPanelCategoryTableViewCellID";
            OrderPanelCategoryTableViewCell *categoryCell = (OrderPanelCategoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:ParentCellIdentifier];
            
            if(categoryCell == nil) {
                
                static NSString *cellNib = @"OrderPanelCategoryTableViewCell" ;
                NSArray *topLevelObjectsProducts = [[NSBundle mainBundle] loadNibNamed:cellNib owner:self options:nil];
                categoryCell = (OrderPanelCategoryTableViewCell *)[topLevelObjectsProducts objectAtIndex:0];
                [categoryCell setSelectionStyle:UITableViewCellSelectionStyleNone];
            }
            
            InventryCategory *objCategory = [self.array_inventryData objectAtIndex:indexPath.section];
            
            categoryCell.labelCategory.text = [NSString stringWithFormat:@"%@",objCategory.categoryName];
            categoryCell.imageViewArrow.image=[UIImage imageNamed:((self.currentSelectedIndex == indexPath.section) ? @"arrow_icon-up.png" : @"arrow_icon-down.png")];
            
            return categoryCell;
        }
        else {
            
            static NSString *ChildCellIdentifier=@"OrderPanelTableViewCellID";
            OrderPanelTableViewCell *childCell = (OrderPanelTableViewCell *)[tableView dequeueReusableCellWithIdentifier:ChildCellIdentifier];
            
            if(childCell == nil) {
                
                static NSString *cellNib = @"OrderPanelTableViewCell" ;
                NSArray *topLevelObjectsProducts = [[NSBundle mainBundle] loadNibNamed:cellNib owner:self options:nil];
                childCell = (OrderPanelTableViewCell *)[topLevelObjectsProducts objectAtIndex:0];
            }
            
            InventryCategory *objCategory = [self.array_inventryData objectAtIndex:indexPath.section];
            if ([objCategory.inventryProductsArray count]) {
                
                InventryStockItems *objItems = [objCategory.inventryProductsArray objectAtIndex:indexPath.row-1];
                childCell.labelPrice.text = [NSString stringWithFormat:@"$%.2f", [objItems.priceValue doubleValue]];
                childCell.labelProduct.text = [NSString stringWithFormat:@"%@", objItems.productName];
                childCell.labelOnHand.text = [NSString stringWithFormat:@"%@", objItems.onhand];
                childCell.labelAvailable.text = [NSString stringWithFormat:@"%@", objItems.stockValue];
                
            }
            return childCell;
        }
    }
    else {
        
        static NSString *CellIdentifier=@"NewOrderTableViewCellID";
        NewOrderTableViewCell *orderCell = (NewOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(orderCell == nil) {
            
            static NSString *cellNib = @"NewOrderTableViewCell" ;
            NSArray *topLevelObjectsProducts = [[NSBundle mainBundle] loadNibNamed:cellNib owner:self options:nil];
            orderCell = (NewOrderTableViewCell *)[topLevelObjectsProducts objectAtIndex:0];
            
            [orderCell.buttonDelete addTarget:self action:@selector(deleteProduct:) forControlEvents:UIControlEventTouchUpInside];
            [orderCell.buttonAdd addTarget:self action:@selector(increaseProductCount:) forControlEvents:UIControlEventTouchUpInside];
            [orderCell.buttonRemove addTarget:self action:@selector(reduceProductCount:) forControlEvents:UIControlEventTouchUpInside];
            [orderCell.button_refund addTarget:self action:@selector(refundProductItem:) forControlEvents:UIControlEventTouchUpInside];
            
            [orderCell.button_split addTarget:self action:@selector(splitProductItem:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        UIColor *bgColo = ((indexPath.row%2)==0) ? kAppLightGrayColor : kAppGrayColor;
        [orderCell setBackgroundColor:bgColo];
        [orderCell.contentView setBackgroundColor:bgColo];
        
        orderCell.buttonAdd.tag = indexPath.row;
        orderCell.buttonRemove.tag = indexPath.row;
        orderCell.buttonDelete.tag = indexPath.row;
        orderCell.button_refund.tag = indexPath.row;
        orderCell.button_split.tag  = indexPath.row;
        orderCell.textFieldQuantity.tag = indexPath.row+999;
        
        orderCell.textFieldQuantity.delegate=self;
        
        TBL_Product *productItem = [self.array_newOrderData objectAtIndex:indexPath.row];
        UIColor *textColor = (productItem.isSwap ? (productItem.isRefunded ? ([productItem.productToUserOrder.openOrderID length]?kAppBlueColor :[UIColor blackColor] ): kAppGreenColor) : [UIColor blackColor]);
        BOOL canEditProduct = (productItem.shouldPayFor || productItem.isRefunded);
        
        [orderCell.button_refund setHidden:canEditProduct];
        [orderCell.button_refund setEnabled:!productItem.isNewProductAdded];
        [orderCell.buttonAdd setHidden:!canEditProduct];
        [orderCell.buttonDelete setHidden:!canEditProduct];
        [orderCell.buttonRemove setHidden:!canEditProduct];
        orderCell.textFieldQuantity.enabled=canEditProduct;
        
        if ([productItem.productQuantity integerValue] > 1) {
            [orderCell.button_split setHidden:!canEditProduct];
        }else {
            [orderCell.button_split setHidden:YES];
        }
        
        [orderCell.labelProductName setTextColor:textColor];
        [orderCell.textFieldQuantity setTextColor:textColor];
        [orderCell.labelTotalCost setTextColor:textColor];
        
        orderCell.labelProductName.text = productItem.productName;
        if (productItem.isRefunded) {
            orderCell.labelTotalCost.text = [NSString stringWithFormat:@"$-%.2f",[productItem.productUnitPrice doubleValue]*[productItem.productQuantity intValue]];
        }
        else {
            orderCell.labelTotalCost.text = [NSString stringWithFormat:@"$%.2f",[productItem.productUnitPrice doubleValue]*[productItem.productQuantity intValue]];
        }
        orderCell.textFieldQuantity.text = [NSString stringWithFormat:@"%@",productItem.productQuantity];
        [orderCell.buttonAdd setEnabled:([productItem.productQuantity integerValue] < [productItem.onHand integerValue])];
        [orderCell.buttonRemove setEnabled:([productItem.productQuantity integerValue] > 1)];
        
        return orderCell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([tableView isEqual:self.tableView_inventory]) {
        
        [self.view endEditing:YES];
        
        if(indexPath.row > 0) {
            
            InventryCategory *objCategory = [self.array_inventryData objectAtIndex:indexPath.section];
            if ([objCategory.inventryProductsArray count]) {
                InventryStockItems *objItems = [objCategory.inventryProductsArray objectAtIndex:indexPath.row-1];
                // 03/01/2017 > okay, just found an issue that we'll need to look at.  If I add an item and put notes in it then click on the same item on the left it doesn't create a new item in the selected list.  It allows me to split the item and doesn't copy the notes, which is working properly.  But that means there will always be an extra step.
                
                if (self.array_newOrderData.count > 0) {
                    BOOL isNote = YES;
                    for (TBL_Product *product in self.array_newOrderData) {
                        if ([product.inventryID isEqualToString:objItems.inventryID] && ![product.productNote length]) {
                            isNote = NO;
                            break;
                        }
                    }
                    if (isNote) {
                        TBL_Product *objProducts = (TBL_Product *)[NSEntityDescription insertNewObjectForEntityForName:@"TBL_Product" inManagedObjectContext:[[DBHelper instance] managedObjectContext]];
                        
                        objProducts.productID = [NSString stringWithFormat:@"%@",objItems.productID];
                        objProducts.productName = [NSString stringWithFormat:@"%@",objItems.productName];
                        objProducts.stockValue = [NSString stringWithFormat:@"%@",objItems.stockValue];
                        objProducts.onHand = [NSString stringWithFormat:@"%@",objItems.onhand];
                        objProducts.productQuantity = @"1";
                        objProducts.productUnitPrice = [NSString stringWithFormat:@"%@",objItems.priceValue];
                        objProducts.inventryID = [NSString stringWithFormat:@"%@",objItems.inventryID];
                        objProducts.priceOverride = @"";
                        objProducts.itemDiscount = @"";
                        objProducts.isSwap = NO;
                        objProducts.isRefunded = NO;
                        objProducts.shouldPayFor = YES;
                        objProducts.productDetails = objItems.details;
                        
                        [self.userNewOrder addUserOrderToProductObject:objProducts];
                        [self.array_newOrderData addObject:objProducts];
                        [[DBHelper instance] saveContext];
                        self.userNewOrder.recalculateStatus = @"1";
                        [self refreshUserNewOrderProducts];
                        [self.tableView_newOrder reloadData];
                    }else {
                        [self addNewInventoryItem:objItems toOrder:self.userNewOrder];
                    }
                }else {
                    [self addNewInventoryItem:objItems toOrder:self.userNewOrder];
                }
            }
        }
        else {
            
            self.currentSelectedIndex = ((self.currentSelectedIndex == indexPath.section) ? -1 : indexPath.section);
            [tableView reloadData];
        }
    }else {
        TBL_Product *productItem = [self.array_newOrderData objectAtIndex:indexPath.row];
        selectedOrderIndex = indexPath.row;
        ItemDetailsViewController *controller = [[ItemDetailsViewController alloc] initWithNibName:@"ItemDetailsViewController" bundle:nil];
        controller.product = productItem;
        [controller setDelegate:self];
        [controller setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [controller setModalPresentationStyle:UIModalPresentationFormSheet];
        [controller setPreferredContentSize:CGSizeMake(800, 670)];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 0.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectZero];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectZero];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat rowHeight = 0.0;
    if ([tableView isEqual:self.tableView_inventory]) {
        rowHeight = ((indexPath.row>0) ? 70.0 : 44.0);
    }
    else {
        rowHeight = 46.0;
    }
    
    return rowHeight;
}
#pragma mark -**************** Helper Methods *****************-
#pragma mark - **** Progress HUD Helper ****

-(void)addProgressHUDToController :(UIViewController*)controller {
    
    [MBProgressHUD hideAllHUDsForView:controller.view animated:YES];
    self.progressHUD = [MBProgressHUD showHUDAddedTo:controller.view animated:YES];
    [self.progressHUD setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.0]];
    [self.progressHUD setColor:[UIColor colorWithWhite:1.0 alpha:0.9]];
}

-(void)removeProgressHUDFromController:(UIViewController*)controller {
    
    //not hidding HUD in case of 'getHTMLReceiptForPrinting' method
    //handled from the controller class
    [MBProgressHUD hideAllHUDsForView:controller.view animated:YES];
    self.progressHUD = nil;
}

#pragma mark - * * * * ItemDetailsDelegate Methods* * * *
-(void)getNoteWith:(NSString *)strNote {
    if (strNote) {
        TBL_Product *productItem = [self.array_newOrderData objectAtIndex:selectedOrderIndex];
        [productItem setProductNote:strNote];
        [[DBHelper instance] saveContext];
    }
    selectedOrderIndex = -1;
}


#pragma mark - * * * * UITextFieldDelegate Methods* * * *
-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    if ([textField isEqual:self.textField_customerSearch]) {
        
        [self refreshCustomerInfoWithObj:[textField.text removeWhiteSpacesFromString]];
    }
    else {
        if([textField.text isVAlidNumberFormat]) {
            self.userNewOrder.recalculateStatus = @"1";
            TBL_Product *productItem = [self.array_newOrderData objectAtIndex:(textField.tag-999)];
            NSInteger qty = [textField.text integerValue];
            if (qty < 1) {
                qty = 1;
                textField.text = @"1";
            }
            
            productItem.productQuantity = [NSString stringWithFormat:@"%ld",(long)qty];
            [self refreshUserNewOrderProducts];
        }
        else [APPDELEGATE displayAlertWithTitle:@"Error!" andMessage:@"Number only."];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField isEqual:self.textField_customerSearch] && (textField.text.length > 0)) {
        isForCustomerInformation = NO;
        [self callWebAPIMethod:lookupCustomer withrequest:[SoapEnvelope getRequsetForLookupCustomerForCustomer:textField.text]];
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:self.textField_customerSearch]) {
        return YES;
    }
    else {
        NSString *newTextToBe =  [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if (textField.tag >= 999) {
            
            TBL_Product*productItem = [self.array_newOrderData objectAtIndex:textField.tag-999];
            return ([newTextToBe isVAlidNumberFormat] && ([newTextToBe integerValue] <= [productItem.onHand integerValue]));
        }
        
        return (([newTextToBe length] < 10) && [newTextToBe isVAlidNumberFormat]);
    }
}

#pragma mark - * * * * CustomPopUpDelegate Method * * * *
-(void)popUpValueSelectedAtIndex:(id)selectedCustomer {
    
    if ([selectedCustomer isKindOfClass:[CustomerItem class]])
        [self refreshCustomerInfoWithObj:selectedCustomer];
    
    [self dismissPopOverIfVisible];
}

-(void)dismissPopOverIfVisible {
    
    if ([self.popOver isPopoverVisible])
        [self.popOver dismissPopoverAnimated:YES];
    [self.popOver setDelegate:nil];
}

#pragma mark - * * * * ScanInventoryDelegate Method * * * *
-(void)scanInventoryCompleted:(NSMutableArray*)inventory {
    
    if (inventory == nil) {
        
        [self dismissPopOverIfVisible];
        return;
    }
    
    if ([inventory count] == 1) {
        InventryStockItems *inventoryItem = [InventryStockItems alloc];

        if (isAddGiftCard) {
            NSDictionary *dict = [inventory lastObject];
            inventoryItem.productName = [dict getTextValueForKey:@"Name"];
            inventoryItem.quantity = [dict getTextValueForKey:@"Quantity"];
            inventoryItem.priceValue = [dict getTextValueForKey:@"Price"];
            inventoryItem.selectionID = [dict getTextValueForKey:@"SelectionId"];
            
            inventoryItem.productID = self.giftCardObj.inventoryId;
            inventoryItem.onhand = @"1";
        }else {
            inventoryItem = [inventory firstObject];
        }
        if ([inventoryItem.onhand integerValue] < 1) {
            [APPDELEGATE displayAlertWithTitle:nil andMessage:[NSString stringWithFormat:@"%@ has an On Hand quantity of 0. To enable selling this item, please go to Tools > Add/Update Inventory and adjust the On Hand Quantity.",inventoryItem.productName]];
        }
        else {
            [self addNewInventoryItem:inventoryItem toOrder:self.userNewOrder];
        }
    }
    else {
        [self dismissPopOverIfVisible];
        
        StatePickerViewController *statePickerViewController=[[StatePickerViewController alloc] initWithNibName:@"StatePickerViewController" bundle:nil];
        statePickerViewController.delegate=self;
        statePickerViewController.dataArray = inventory;
        statePickerViewController.preferredContentSize = CGSizeMake(320.0, 270.0);
        self.popOver=[[UIPopoverController alloc] initWithContentViewController:statePickerViewController];
        [[self.popOver contentViewController] setPreferredContentSize:CGSizeMake(320.0, 270.0)];
        [self.popOver presentPopoverFromRect:[[[APPDELEGATE navigationController] view] bounds] inView:[[APPDELEGATE navigationController] view] permittedArrowDirections:0 animated:YES];
    }
}

#pragma mark - * * * * stateDelegate Method * * * *
-(void)selectedState :(id) selectedItem {
    
    if([selectedItem isKindOfClass:[InventryStockItems class]]){
        if (selectedItem) {
            InventryStockItems *inventoryItem = (InventryStockItems *)selectedItem;
            if ([inventoryItem.onhand integerValue] < 1) {
                [APPDELEGATE displayAlertWithTitle:nil andMessage:[NSString stringWithFormat:@"%@ has an On Hand quantity of 0. To enable selling this item, please go to Tools > Add/Update Inventory and adjust the On Hand Quantity.",inventoryItem.productName]];
            }
            else {
                [self addNewInventoryItem:inventoryItem toOrder:self.userNewOrder];
                [self dismissPopOverIfVisible];
            }
        }
    }
    else if([selectedItem isKindOfClass:[PortInfo class]]) {
        
        [self dismissPopOverIfVisible];
    }
    else{
        [self dismissPopOverIfVisible];
    }
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    return NO;
}

#pragma mark - * * * * Webservices Calling * * * *

-(void)makeWebAPICallToFetchAllInventry {
    
    RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
    objHandler.delegate = self;
    [objHandler call_SOAP_POSTMethodWithRequest:[SoapEnvelope getInventryListItems:locationID] andMethodName:getInventoryWithCategories andController:nil];
}

-(void)callWebAPIMethod:(WebMethodType)methodType withrequest:(NSString*)request {
    
    RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
    objHandler.delegate = self;
    [objHandler call_SOAP_POSTMethodWithRequest:request andMethodName:methodType andController:[APPDELEGATE navigationController]];
}

#pragma mark - * * * * RequestResponseHandlerDelegate * * * *
-(void)ResponseContentFromServer:(RequestResponseHandler *)responseObj withData:(id)jsonData forMethod:(WebMethodType)methodType {
    
    if ([responseObj.responseCode intValue] == SERVER_RESPONSE_SUCCESS_CODE) {
        
        switch (methodType) {
                
            case getInventoryWithCategories: {
                
                [self.array_inventryData removeAllObjects];
                
                NSArray *categoryListList = [[jsonData objectForKey:@"InventoryCategoryArray"] objectForKey:@"InventoryCategory"];
                if ([categoryListList isKindOfClass:[NSDictionary class]]) {
                    [self.array_inventryData addObject:[InventryCategory getInventoryCategoryFrom:(NSDictionary*)categoryListList]];
                }
                else {
                    for (NSDictionary *item in categoryListList)
                        [self.array_inventryData addObject:[InventryCategory getInventoryCategoryFrom:item]];
                }
                
                [self.tableView_inventory reloadData];
                
                if(!isGetAllInvetoryWithButtonClick) {
                    
                    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
                    if (self) {
                        [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
                        [self addProgressHUDToController:self];
                    }
                    
                    [APPDELEGATE starIoExtManager].delegate = self;
                    [self scannerDidScan:^(id response, NSString *error) {
                        
                        if(response && [response isKindOfClass:[NSString class]]){
                            
                            [self callWebAPIMethod:lookupInventory withrequest:[SoapEnvelope getRequsetForLookupInventoryWithSKU:response]];
                        }
                        if(error){
                            [APPDELEGATE displayAlertWithTitle:@"Error!" andMessage:error];
                        }
                    }];
                    
                    [self removeProgressHUDFromController:self];
                    isGetAllInvetoryWithButtonClick = YES;
                }
            }
                break;
                
            case calculateForCheckOut: {
                
                NSMutableArray *inventory = [NSMutableArray array];
                NSMutableArray *inventoryIsGiftCard = [NSMutableArray array];

                NSArray *inventoryData = [[jsonData objectForKey:@"InventoryArray"] objectForKey:@"InventorySelection"];
                
                if ([inventoryData isKindOfClass:[NSDictionary class]]) {
                    if (isAddGiftCard) {
                        [inventoryIsGiftCard addObject:[[jsonData objectForKey:@"InventoryArray"] objectForKey:@"InventorySelection"]];
                    }else {
                        SelectedInventoryItem *item = [SelectedInventoryItem getInventoryItemfrom:(NSDictionary*)inventoryData withOrderStatus:@""];
                        [inventory addObject:item];
                    }
                }
                else {
                    if (isAddGiftCard) {
                        [inventoryIsGiftCard addObject:[inventoryData lastObject]];
                    }else {
                        for (NSDictionary *data in inventoryData)
                            [inventory addObject:[SelectedInventoryItem getInventoryItemfrom:data withOrderStatus:@""]];
                    }
                }
                
                if (isAddGiftCard) {
                    [self scanInventoryCompleted:inventoryIsGiftCard];
                    
                    TransactionRecord *transactionRec = [TransactionRecord getTransactionrecordFrom:[jsonData objectForKey:@"TransactionRecord"]];
                    transactionRec.CustomerName = self.userNewOrder.userOrderToCustomer.customerFullName;
                    transactionRec.CustomerFirstName = self.userNewOrder.userOrderToCustomer.customerFirstName;
                    transactionRec.CustomerLastName = self.userNewOrder.userOrderToCustomer.customerLastName;
                    transactionRec.CustomerNotes = self.userNewOrder.userOrderToCustomer.customerNotes;
                    
                    [self.userNewOrder setIsNewOrder:true];
                    self.userNewOrder.orderID = transactionRec.orderID;
                    self.userNewOrder.recalculateStatus = @"0";
                    self.userNewOrder.paymentAmount = @"0.00";
                    self.userNewOrder.recalculateStatus = @"0";
                    [[DBHelper instance] saveContext];

                }else {
                    
                    NSMutableArray *selectionIdArray = [NSMutableArray array];
                    for (TBL_Product *product in [self.userNewOrder.userOrderToProduct allObjects]) {
                        if (product.selectionID) {
                            [selectionIdArray addObject:product.selectionID];
                        }
                    }
                    NSString *tempSelectionId;
                    for (TBL_Product *product in [self.userNewOrder.userOrderToProduct allObjects]) {
                        if (!product.selectionID) {
                            for (SelectedInventoryItem *item in inventory) {
                                if ([product.productID isIdenticaleTo:item.InventoryStockId] && ([selectionIdArray count]?(![selectionIdArray containsObject:item.SelectionId]):([item.SelectionId integerValue] > [tempSelectionId integerValue]))) {
                                    product.selectionID = item.SelectionId;
                                    tempSelectionId = item.SelectionId;
                                    break;
                                }
                            }
                        }
                    }
                    for (SelectedInventoryItem *item in inventory) {
                        for (TBL_Product *productItem in [self.userNewOrder.userOrderToProduct allObjects]) {
                            if ([productItem.productID isIdenticaleTo:item.InventoryStockId] && [productItem.selectionID isIdenticaleTo:item.SelectionId]) {
                                item.overriddenPrice = productItem.priceOverride;
                                item.overriddenDiscount = productItem.itemDiscount;
                                break;
                            }
                        }
                    }

                    TransactionRecord *transactionRec = [TransactionRecord getTransactionrecordFrom:[jsonData objectForKey:@"TransactionRecord"]];
                    transactionRec.CustomerName = self.userNewOrder.userOrderToCustomer.customerFullName;
                    transactionRec.CustomerFirstName = self.userNewOrder.userOrderToCustomer.customerFirstName;
                    transactionRec.CustomerLastName = self.userNewOrder.userOrderToCustomer.customerLastName;
                    transactionRec.CustomerNotes = self.userNewOrder.userOrderToCustomer.customerNotes;
                    
                    [self.userNewOrder setIsNewOrder:isForSendItems];
                    self.userNewOrder.orderID = transactionRec.orderID;
                    self.userNewOrder.recalculateStatus = @"0";
                    self.userNewOrder.paymentAmount = @"0.00";
                    self.userNewOrder.recalculateStatus = @"0";
                    [[DBHelper instance] saveContext];
                    
                    if (isForSaveOrder) {
                        isForSaveOrder = NO;
                        
                        [self refreshViewForNewFreshOrder];
                    }else if (isForSendItems) {
                        isForSendItems = NO;
                        [self callWebAPIMethod:getOrderDetails withrequest:[SoapEnvelope getSOAPRequestForOrderDetailsForOrder:self.userNewOrder.orderID isFromPendingOrder:@"0"]];
                    }
                    else {
                        self.giftCardObj = nil;
                        CheckOutVC *checkOutVC = [[CheckOutVC alloc] initWithNibName:@"CheckOutVC" bundle:nil];
                        checkOutVC.transactionRecord = transactionRec;
                        checkOutVC.inventoryArray = inventory;
                        checkOutVC.selectedAvailablePoints = availablePoints;
                        transactionRec.recalculateStatus = @"0";
                        
                        [[self navigationController] pushViewController:checkOutVC animated:YES];
                    }
                }
            }
                break;
                
            case lookupCustomer: {
                
                NSArray *customerData = [[jsonData objectForKey:@"CustomerArray"] objectForKey:@"CustomerLookup"];
                
                NSMutableArray *array_customerData = [NSMutableArray array];
                
                
                if ([customerData isKindOfClass:[NSDictionary class]]) {
                    [array_customerData addObject:[CustomerItem getCustomerFrom:(NSDictionary*)customerData]];
                }
                else {
                    for (NSDictionary *item in customerData)
                        [array_customerData addObject:[CustomerItem getCustomerFrom:item]];
                }
                
                if (isForCustomerInformation) {
                    isForCustomerInformation = NO;
                    [self refreshCustomerInfoWithObj:[array_customerData firstObject]];
                }
                else {
                    if ([array_customerData count] == 0)
                        [APPDELEGATE displayAlertWithTitle:@"" andMessage:@"No customer found with specified keyword."];
                    else {
                        //need to show pop over with customer data
                        CustomerSelectionPopOver *customerslectionVC = [[CustomerSelectionPopOver alloc] initWithNibName:@"CustomerSelectionPopOver" bundle:nil];
                        customerslectionVC.array_commonData = array_customerData;
                        [customerslectionVC setDelegate:self];
                        self.popOver = [[UIPopoverController alloc] initWithContentViewController:customerslectionVC];
                        [self.popOver setPopoverContentSize:CGSizeMake(320.0, 500.0)];
                        [self.popOver presentPopoverFromRect:self.textField_customerSearch.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
                    }
                }
            }
                break;
                
            case lookupCustomerWithLoyaltyCard: {
                
                NSArray *customerData = [[jsonData objectForKey:@"CustomerArray"] objectForKey:@"CustomerLookup"];
                
                NSMutableArray *array_customerData = [NSMutableArray array];
                
                
                if ([customerData isKindOfClass:[NSDictionary class]]) {
                    [array_customerData addObject:[CustomerItem getCustomerFrom:(NSDictionary*)customerData]];
                }
                else {
                    for (NSDictionary *item in customerData)
                        [array_customerData addObject:[CustomerItem getCustomerFrom:item]];
                }
                
                if (isForCustomerInformation) {
                    isForCustomerInformation = NO;
                    [self refreshCustomerInfoWithObj:[array_customerData firstObject]];
                }
                else {
                    if ([array_customerData count] == 0)
                        [APPDELEGATE displayAlertWithTitle:@"" andMessage:[@"Could not find Inventory or Customer with " stringByAppendingString:giftCardNumber]];
                    else {
                        //need to show pop over with customer data
                        CustomerSelectionPopOver *customerslectionVC = [[CustomerSelectionPopOver alloc] initWithNibName:@"CustomerSelectionPopOver" bundle:nil];
                        customerslectionVC.array_commonData = array_customerData;
                        [customerslectionVC setDelegate:self];
                        self.popOver = [[UIPopoverController alloc] initWithContentViewController:customerslectionVC];
                        [self.popOver setPopoverContentSize:CGSizeMake(320.0, 500.0)];
                        [self.popOver presentPopoverFromRect:self.textField_customerSearch.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
                    }
                }
            }
                break;
                
            case deleteOrder: {
                [[DBHelper instance] deleteOrder:self.userNewOrder forUser:self.appUser.loggedInUser];
                [self refreshViewForNewFreshOrder];
            }
                break;
            case lookupInventory: {
                NSArray *inventoryData = [[jsonData objectForKey:@"InventoryArray"] objectForKey:@"InventoryDetail"];
                NSMutableArray *searchedInventory = [NSMutableArray array];
                
                if ([inventoryData isKindOfClass:[NSDictionary class]]) {
                    [searchedInventory addObject:[InventryStockItems getInventoryFromData:(NSDictionary*)inventoryData]];
                }
                else {
                    for (NSDictionary *item in inventoryData)
                        [searchedInventory addObject:[InventryStockItems getInventoryFromData:item]];
                }
                
                if ([searchedInventory count] > 0)
                    [self scanInventoryCompleted:searchedInventory];
                else{
                    if ([[[AppUserData sharedInstance] selectedLocation] useloyaltycards]) {
                        [self callWebAPIMethod:lookupCustomerWithLoyaltyCard withrequest:[SoapEnvelope getSOAPRequestForLookupCustomerWithLoyaltyCard:self.userNewOrder.userOrderToCustomer.customerID andGiftCardNumber:giftCardNumber withLoyaltyCardNumber:giftCardNumber andIsCheckOut:nil]];
                    }else {
                        [APPDELEGATE displayAlertWithTitle:@"" andMessage:[@"Could not find Inventory with " stringByAppendingString:giftCardNumber]];
                    }

                }
            }
                break;
                
            case getOrderDetails: {
                
                NSMutableArray *inventory = [NSMutableArray array];
                NSArray *inventoryData = [[[jsonData objectForKey:@"OrderDetail"]objectForKey:@"InventoryArray"] objectForKey:@"InventorySelection"];
                
                if ([inventoryData isKindOfClass:[NSDictionary class]]) {
                    [inventory addObject:[SelectedInventoryItem getInventoryItemfrom:(NSDictionary*)inventoryData withOrderStatus:@""]];
                }
                else {
                    for (NSDictionary *data in inventoryData)
                        [inventory addObject:[SelectedInventoryItem getInventoryItemfrom:data withOrderStatus:@""]];
                }
                SendItemsViewController *sendVC = [[SendItemsViewController alloc] initWithNibName:@"SendItemsViewController" bundle:nil];
                sendVC.array_itemOrdered = inventory;
                sendVC.orderID = [[jsonData objectForKey:@"OrderDetail"] getTextValueForKey:@"Id"];
                [sendVC setModalPresentationStyle:UIModalPresentationFormSheet];
                [sendVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                [sendVC setPreferredContentSize:CGSizeMake(550, 500)];
                [self presentViewController:sendVC animated:NO completion:nil];
            }
                break;
                
            default:
                break;
        }
    }
    else {
        [APPDELEGATE displayAlertWithTitle:@"" andMessage:responseObj.responseMessage];
    }
}

-(void)navigateToTools {
    
    [[APPDELEGATE baseController] moveToAddUpdateInventory:@""];
}

@end
