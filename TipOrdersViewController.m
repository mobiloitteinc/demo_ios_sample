//
//  TipOrdersViewController.m
//  Active Club Solutions -iPAD App
//
//  Created by Krishna Kant Kaira on 02/03/15.
//  Copyright (c) 2015 Mobiloitte Technologies. All rights reserved.
//

#import "TipOrdersViewController.h"
#import "OrderPanelCategoryTableViewCell.h"
#import "PendingPopoverCustomCell.h"
#import "TipOrderDetailsCustomCell.h"
#import "WaitingTipOrderItem.h"
#import "UpdatePaymentVC.h"
#import "StarPrinterHelper.h"
#import "HtmlToImage.h"
#import "EnterPINViewController.h"

@interface TipOrdersViewController ()<UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate, RequestResponseHandlerDelegate, UpdatePaymentDelegate, HtmlToImageDelegate> {
    
    NSInteger selectedIndex;
    NSInteger selectedPaymentLine;
    NSInteger lastRecieptIndex;
    ReceiptType selectedReceiptType;
}

@property (nonatomic, strong) HtmlToImage *htmlConverter;

@property (nonatomic, strong) NSString *receiptPathForCustomer;
@property (nonatomic, strong) NSString *receiptPathForMerchant;

@property (nonatomic, strong) NSString *receiptForCustomer;
@property (nonatomic, strong) NSString *receiptForMerchant;
@property (nonatomic, strong) IBOutlet UITableView *tableView_tipOrderList;
@property (nonatomic, strong) IBOutlet UITableView *tableView_tipOrderDetails;

@property (nonatomic, strong) IBOutlet UILabel *label_amountCount;
@property (nonatomic, strong) IBOutlet UILabel *label_tipValue;
@property (nonatomic, strong) IBOutlet UILabel *label_totalCount;
@property (nonatomic, strong) IBOutlet UIButton *btn_print;
@property (nonatomic, strong) IBOutlet UIButton *btn_close_tipOrders;

@property (nonatomic, strong) NSMutableArray *array_tipOrderList;
@property (nonatomic, strong) UIPopoverController *popOver_updatePayment;

-(IBAction)closeBtn:(UIButton *)sender;
-(IBAction)printButtonAction:(UIButton *)sender;
-(IBAction)waitingOnTipsButtonAction:(id)sender;

@end

@implementation TipOrdersViewController

#pragma mark - *********** Life Cycle Methods ***************
- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self controllerDefaulSetting];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[self navigationItem] setHidesBackButton:YES];
    [[self navigationController] setNavigationBarHidden:YES];
    [[APPDELEGATE baseController] setScreenTitle:@"Tip Orders"];
    [[APPDELEGATE baseController] setSelectedIndex:2];
    
    [self refreshTipOrderData];
    
    if ([APPDELEGATE isPrinterActive])
        [self.btn_print setEnabled:self.btn_close_tipOrders.enabled];
    else
        [self.btn_print setEnabled:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(awakeFromBackground) name:UIApplicationDidBecomeActiveNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];

    if (self.presentedViewController)
        [self dismissViewControllerAnimated:NO completion:nil];
    
    [self dismissPopOver];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Event Action for UIApplicationDidBecomeActiveNotification
-(void)awakeFromBackground {
    
    if ([APPDELEGATE isPrinterActive])
        [self.btn_print setEnabled:self.btn_close_tipOrders.enabled];
    else
        [self.btn_print setEnabled:NO];
}

#pragma mark - ********** UIPopoverControllerDelegate Methods ************

/* Called on the delegate when the popover controller will dismiss the popover. Return NO to prevent the dismissal of the view.
 */
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    return NO;
}

#pragma mark - ********** Other Helper Methods ************
-(void)refreshTipOrderData {
    
    [self.array_tipOrderList removeAllObjects];
    selectedIndex = -1;
    selectedPaymentLine = -1;
    lastRecieptIndex = -1;

    [self.btn_close_tipOrders setEnabled:(selectedIndex>=0)];
    if ([APPDELEGATE isPrinterActive])
        [self.btn_print setEnabled:self.btn_close_tipOrders.enabled];
    else
        [self.btn_print setEnabled:NO];
    
    [self.tableView_tipOrderList reloadData];
    [self.tableView_tipOrderDetails reloadData];
    [self callWebAPIWithRequest:[SoapEnvelope getOrdersWaitingForTip] forMethod:getOrdersWaitingForTips];
}

-(void)dismissPopOver {
    
    if ([self.popOver_updatePayment isPopoverVisible])
        [self.popOver_updatePayment dismissPopoverAnimated:NO];
}

-(void)controllerDefaulSetting {
    
    self.array_tipOrderList = [NSMutableArray array];

    [self.btn_close_tipOrders setExclusiveTouch:YES];
    [self.btn_print setExclusiveTouch:YES];

    [[self tableView_tipOrderList] setDelegate:self];
    [[self tableView_tipOrderList] setDataSource:self];
    [self.tableView_tipOrderList setBackgroundView:nil];
    [self.tableView_tipOrderList setBackgroundColor:[UIColor clearColor]];
    [self.tableView_tipOrderList setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [[self tableView_tipOrderDetails] setDelegate:self];
    [[self tableView_tipOrderDetails] setDataSource:self];
    [self.tableView_tipOrderDetails setBackgroundView:nil];
    [self.tableView_tipOrderDetails setBackgroundColor:[UIColor clearColor]];
    [self.tableView_tipOrderDetails setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

-(void)updateBottomTotals {
    
    WaitingTipOrderItem *orderItem ;
    if (selectedIndex >= 0) {
        orderItem = [self.array_tipOrderList objectAtIndex:selectedIndex];
    }
    [self.btn_close_tipOrders setEnabled:(selectedIndex>=0)];
    
    if ([APPDELEGATE isPrinterActive])
        [self.btn_print setEnabled:self.btn_close_tipOrders.enabled];
    else
        [self.btn_print setEnabled:NO];
    double amount = 0.0;
    double tip = 0.0;
    double total = 0.0;
    
    for (PaymentItem*payment in orderItem.array_payment) {
        amount += [payment.Amount doubleValue];
        tip += [payment.Tip doubleValue];
        total += [payment.TotalAmount doubleValue];
    }
    
    self.label_amountCount.text = [NSString stringWithFormat:@"$%.2f",amount];
    self.label_tipValue.text = [NSString stringWithFormat:@"$%.2f",tip];
    self.label_totalCount.text = [NSString stringWithFormat:@"$%.2f",total];
}

#pragma mark - ******* TableView DataSource Methods **************
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numberOfRows = 0;
    if ([tableView isEqual:self.tableView_tipOrderList]) {
        numberOfRows = [self.array_tipOrderList count];
    }
    else if ([tableView isEqual:self.tableView_tipOrderDetails] && selectedIndex >= 0) {
        WaitingTipOrderItem *orderItem = [self.array_tipOrderList objectAtIndex:selectedIndex];
        numberOfRows = [orderItem.array_payment count];
    }
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([tableView isEqual:self.tableView_tipOrderList]) {
        
        static NSString *PendingPopoverCustomCellID= @"PendingPopoverCustomCellID";
        PendingPopoverCustomCell *tipOrdersCell = (PendingPopoverCustomCell *)[tableView dequeueReusableCellWithIdentifier:PendingPopoverCustomCellID];
        
        if(tipOrdersCell == nil) {
            
            NSArray *topLevelObjectsProducts = [[NSBundle mainBundle] loadNibNamed:@"PendingPopoverCustomCell" owner:self options:nil];
            tipOrdersCell = (PendingPopoverCustomCell *)[topLevelObjectsProducts objectAtIndex:0];
        }
        
        WaitingTipOrderItem *orderItem = [self.array_tipOrderList objectAtIndex:indexPath.row];
        
        tipOrdersCell.labelUserName.text = orderItem.RepName;
        tipOrdersCell.labelOrderNumber.text = orderItem.orderId;
        tipOrdersCell.labelCustomer.text = orderItem.CustomerName;
        tipOrdersCell.labelCost.text = [NSString stringWithFormat:@"Total: %.2f",[orderItem.SaleTotal doubleValue]];
        
        BOOL isSelectedOrder = (indexPath.row == selectedIndex);
        [tipOrdersCell.contentView setBackgroundColor:(isSelectedOrder ? kAppBlueColor : kAppLightGrayColor)];

        //The order selected on Tip Orders screen needs to be highlighted
        UIColor *textColor = (isSelectedOrder ? [UIColor whiteColor] : [UIColor blackColor]);
        tipOrdersCell.labelCustomer.textColor = textColor;
        tipOrdersCell.labelOrderNumber.textColor = textColor;
        tipOrdersCell.labelUserName.textColor = textColor;
        tipOrdersCell.labelCost.textColor = (isSelectedOrder ? kAppGreenColor : kAppThemeColor);
        
        return tipOrdersCell;
    }
    else {
        
        static NSString *CellIdentifier=@"TipOrderDetailsCustomCellID";
        
        TipOrderDetailsCustomCell *tipOrderDetailsCell = (TipOrderDetailsCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if(tipOrderDetailsCell == nil) {
            
            NSArray *topLevelObjectsProducts = [[NSBundle mainBundle] loadNibNamed:@"TipOrderDetailsCustomCell" owner:self options:nil];
            tipOrderDetailsCell = (TipOrderDetailsCustomCell *)[topLevelObjectsProducts objectAtIndex:0];
        }
        
        UIColor *bgColo = ((indexPath.row%2)==0) ? kAppLightGrayColor : kAppGrayColor;
        [tipOrderDetailsCell setBackgroundColor:bgColo];
        [tipOrderDetailsCell.contentView setBackgroundColor:bgColo];
        
        WaitingTipOrderItem *orderItem = [self.array_tipOrderList objectAtIndex:selectedIndex];
        PaymentItem *paymentItem = [orderItem.array_payment objectAtIndex:indexPath.row];
        
        tipOrderDetailsCell.label_paymentNumber.text = paymentItem.PaymentId;
        tipOrderDetailsCell.label_methodName.text = paymentItem.PaymentMethod;
        tipOrderDetailsCell.label_status.text = paymentItem.PaymentStatus;
        tipOrderDetailsCell.label_amount.text = [NSString stringWithFormat:@"$%.2f",[paymentItem.Amount doubleValue]];
        tipOrderDetailsCell.label_tip.text = [NSString stringWithFormat:@"$%.2f",[paymentItem.Tip doubleValue]];
        tipOrderDetailsCell.label_total.text = [NSString stringWithFormat:@"$%.2f",[paymentItem.TotalAmount doubleValue]];
        
        return tipOrderDetailsCell;
    }
}

#pragma mark - * * * * UITableViewDelegate Method * * * *
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([tableView isEqual:self.tableView_tipOrderList]) {
        
        selectedIndex = indexPath.row;
        [self.btn_close_tipOrders setEnabled:(selectedIndex>=0)];
        
        if ([APPDELEGATE isPrinterActive])
            [self.btn_print setEnabled:self.btn_close_tipOrders.enabled];
        else
            [self.btn_print setEnabled:NO];
        
        WaitingTipOrderItem *orderItem = [self.array_tipOrderList objectAtIndex:indexPath.row];
        [self callWebAPIWithRequest:[SoapEnvelope getGetPaymentsForOrder:orderItem.orderId withPayemnt:nil isForReceipt:NO] forMethod:getPaymentsForOrder];
        
        [tableView reloadData];
    }
    else {
        
        selectedPaymentLine = indexPath.row;
        
        WaitingTipOrderItem *orderItem = [self.array_tipOrderList objectAtIndex:selectedIndex];
        PaymentItem *paymentItem = [orderItem.array_payment objectAtIndex:selectedPaymentLine];
        
        if (![paymentItem.PaymentStatus isIdenticaleTo:@"Paid"] && ![paymentItem.PaymentStatus isIdenticaleTo:@"Charged"]) {
            UpdatePaymentVC *paymentVC = [[UpdatePaymentVC alloc] initWithNibName:@"UpdatePaymentVC" bundle:nil];
            paymentVC.amountVal = paymentItem.Amount;
            paymentVC.tipVal = paymentItem.Tip;
            [paymentVC setPaymentDelegate:self];
            paymentVC.isFromTipOrders = YES;
            self.popOver_updatePayment = [[UIPopoverController alloc] initWithContentViewController:paymentVC];
            [self.popOver_updatePayment setDelegate:self];
            [self.popOver_updatePayment setPopoverContentSize:CGSizeMake(620.0, 380.0)];
            [self.popOver_updatePayment presentPopoverFromRect:[[self view] frame] inView:[self view] permittedArrowDirections:0 animated:NO];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return ([tableView isEqual:self.tableView_tipOrderList] ? 62.0 : 46.0);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectZero];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectZero];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0;
}

#pragma mark - UIAlertViewDelegate Method * * * *

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        
        switch (alertView.tag) {
                
            case 12: {
                if ([[[AppUserData sharedInstance] selectedLocation] forcepinatcheckout]) {
                    
                    [[EnterPINViewController sharedInstance] getForcedPINForcheckout:^(NSDictionary *dict, NSString *error) {

                        WaitingTipOrderItem *orderItem;
                        if (selectedIndex >= 0)
                            orderItem = [self.array_tipOrderList objectAtIndex:selectedIndex];
                        
                        [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestToCloseOrder:orderItem.orderId withForcePIN:dict andKeepOrder:NO andFuturePickUp:NO]    forMethod:closeOrder];
                    }];
                }
                else {

                    WaitingTipOrderItem *orderItem;
                    if (selectedIndex >= 0)
                        orderItem = [self.array_tipOrderList objectAtIndex:selectedIndex];
                    
                    [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestToCloseOrder:orderItem.orderId withForcePIN:nil andKeepOrder:NO andFuturePickUp:NO] forMethod:closeOrder];
                }
            }
                break;
                
            default: break;
        }
    }
}

#pragma mark - * * * * IBAction Method * * * *
-(IBAction)closeBtn:(UIButton *)sender {
    //Close Order
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Are you sure you want to close this order?" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
    alert.tag=12;
    [alert show];
    [APPDELEGATE setVisibleAlertView:alert];
    [APPDELEGATE setDismissButtonIndex:1];
}

-(IBAction)printButtonAction:(UIButton *)sender {
    
    if ([APPDELEGATE isPrinterActive]) {
        
        WaitingTipOrderItem *orderItem;
        if (selectedIndex >= 0)
            orderItem = [self.array_tipOrderList objectAtIndex:selectedIndex];
        if ([orderItem.array_payment count] > 0)
            [self callForPrintReciept:0];
        else
            [APPDELEGATE displayAlertWithTitle:@"Sorry" andMessage:@"No payment line available."];
    }
}

-(IBAction)waitingOnTipsButtonAction:(id)sender {
    
    [self refreshTipOrderData];
}

#pragma mark - * * * *  UpdatePaymentDelegate Method * * * *
-(void)paymentUpdatedWithAmount:(NSString*)amount andTip:(NSString*)tip {
    
    [self dismissPopOver];
    
    if ((amount != nil) && (tip != nil)) {
        
        WaitingTipOrderItem *orderItem ;
        if (selectedIndex >= 0)
            orderItem = [self.array_tipOrderList objectAtIndex:selectedIndex];
        PaymentItem *paymentItem = [orderItem.array_payment objectAtIndex:selectedPaymentLine];
        
        paymentItem.Amount = amount;
        paymentItem.Tip = tip;
        paymentItem.TotalAmount = [NSString stringWithFormat:@"%.2f",[amount doubleValue]+[tip doubleValue]];
        
        [self.tableView_tipOrderDetails reloadData];
        [self updateBottomTotals];
        
        [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestToClosePayment:paymentItem forOrder:orderItem] forMethod:closePayment];
    }
}

#pragma mark - * * * * Webservices Calling * * * *
-(void)callWebAPIWithRequest:(NSString *)soapContent forMethod:(WebMethodType)method {
    
    RequestResponseHandler *objHandler = [[RequestResponseHandler alloc] init];
    objHandler.delegate = self;
    
    [objHandler call_SOAP_POSTMethodWithRequest:soapContent andMethodName:method andController:[APPDELEGATE navigationController]];
}

#pragma mark - * * * * RequestResponseHandlerDelegate * * * *
-(void)ResponseContentFromServer:(RequestResponseHandler *)responseObj withData:(id)jsonData forMethod:(WebMethodType)methodType {
    
    if ([responseObj.responseCode intValue] == SERVER_RESPONSE_SUCCESS_CODE) {
        
        switch (methodType) {
                
            case getOrdersWaitingForTips: {
                
                [self.array_tipOrderList removeAllObjects];
                selectedIndex = -1;
                selectedPaymentLine = -1;
                
                NSArray *orderList = [[jsonData objectForKey:@"OrderArray"] objectForKey:@"Order"];
                if (orderList && [orderList isKindOfClass:[NSDictionary class]]) {
                    [self.array_tipOrderList addObject:[WaitingTipOrderItem getWaitingTipOrderItemfrom:(NSDictionary*)orderList]];
                }
                else {
                    for (NSDictionary *item in orderList)
                        [self.array_tipOrderList addObject:[WaitingTipOrderItem getWaitingTipOrderItemfrom:item]];
                }
            }
                break;
                
            case getPaymentsForOrder: {
                
                if (selectedIndex >= 0) {
                    
                    WaitingTipOrderItem *orderItem = [self.array_tipOrderList objectAtIndex:selectedIndex];
                    [orderItem updatePaymentDetailWith:jsonData];
                }
            }
                break;
                
            case closeOrder: {
                
                [self.array_tipOrderList removeObjectAtIndex:selectedIndex];
                selectedIndex = -1;
                selectedPaymentLine = -1;
                
            }
                break;
                
            case closePayment: {
                
                WaitingTipOrderItem *orderItem;
                if (selectedIndex >= 0)
                    orderItem = [self.array_tipOrderList objectAtIndex:selectedIndex];
                PaymentItem *paymentItem = [orderItem.array_payment objectAtIndex:selectedPaymentLine];
                paymentItem.PaymentStatus = [[jsonData objectForKey:@"PaymentInfo"] getTextValueForKey:@"PaymentStatus"];
                
                selectedPaymentLine = -1;
            }
                break;
                
            case getHTMLReceiptForPrinting: {
                
                if ([responseObj.responseCode intValue] == SERVER_RESPONSE_SUCCESS_CODE) {
                    
                    self.receiptPathForCustomer = nil;
                    self.receiptPathForMerchant = nil;
                    
                    self.receiptForCustomer = [[jsonData objectForKey:@"ReceiptDetailRecord"] getTextValueForKey:@"HTMLReceiptCustomerCopy"];
                    self.receiptForMerchant = [[jsonData objectForKey:@"ReceiptDetailRecord"] getTextValueForKey:@"HTMLReceiptMerchantCopy"];
                    
                        
                    if ([self.receiptForCustomer length]) {
                        
                        WaitingTipOrderItem *orderItem;
                        if (selectedIndex >= 0)
                            orderItem = [self.array_tipOrderList objectAtIndex:selectedIndex];

                        PaymentItem *payment = [orderItem.array_payment objectAtIndex:lastRecieptIndex];
                        NSString *copyName = [NSString stringWithFormat:@"%@_%@",orderItem.orderId,payment.PaymentId];
                        
                        self.htmlConverter = [HtmlToImage createFileWithHTMLContent:self.receiptForCustomer
                                                                        pathForFile:[pathForOrderRecieptWithID(orderItem.orderId) stringByAppendingPathComponent:getFileNameForCustomerCopy(copyName)]
                                                                           delegate:self
                                                                           pageSize:CGSizeMake(kReceiptWidth, MAXFLOAT)
                                                                            margins:UIEdgeInsetsZero];
                    }
                    else {
                        [self callForPrintReciept:(lastRecieptIndex+1)];
                    }
                }
                else
                    [self callForPrintReciept:(lastRecieptIndex+1)];
            }
                break;

            default:  break;
        }
    }
    else {
        selectedIndex = -1;
        selectedPaymentLine = -1;
        
        [APPDELEGATE displayAlertWithTitle:@"" andMessage:responseObj.responseMessage];
    }
    
    [self updateBottomTotals];
    [self.tableView_tipOrderList reloadData];
    [self.tableView_tipOrderDetails reloadData];
}

#pragma mark - * * * * HTML receipt Printing helper methods * * * *

-(void)callForPrintReciept:(NSInteger)index {
    
    WaitingTipOrderItem *orderItem;
    if (selectedIndex >= 0)
        orderItem = [self.array_tipOrderList objectAtIndex:selectedIndex];
    
    if ((orderItem.array_payment.count > 0) && (index <= (orderItem.array_payment.count-1))) {
        lastRecieptIndex = index;
        
        PaymentItem *payment = [orderItem.array_payment objectAtIndex:lastRecieptIndex];
        [self callWebAPIWithRequest:[SoapEnvelope getReceiptForOrder:payment.OrderId paymentID:payment.PaymentId cash:payment.CashReceived andChange:payment.ChangeGiven forSoapAction:@"GetHTMLReceiptForPrinting"] forMethod:getHTMLReceiptForPrinting];
    }
    else {
        lastRecieptIndex = -1;
        [MBProgressHUD hideAllHUDsForView:[APPDELEGATE navigationController].view animated:YES];
    }
}

#pragma mark - * * * * HtmlToImageDelegate * * * *
- (void)HTMLtoImageOrPDFDidSucceed:(HtmlToImage*)htmlToPDF {
    
    if (self.receiptPathForCustomer == nil) {
        self.receiptPathForCustomer = htmlToPDF.imagePath;
        
        self.htmlConverter = nil;
        
        if ([self.receiptForMerchant length]) {
            
            WaitingTipOrderItem *orderItem;
            if (selectedIndex >= 0)
                orderItem = [self.array_tipOrderList objectAtIndex:selectedIndex];
            
            PaymentItem *payment = [orderItem.array_payment objectAtIndex:lastRecieptIndex];
            NSString *copyName = [NSString stringWithFormat:@"%@_%@",orderItem.orderId,payment.PaymentId];
            
            self.htmlConverter = [HtmlToImage createFileWithHTMLContent:self.receiptForMerchant
                                                            pathForFile:[pathForOrderRecieptWithID(orderItem.orderId) stringByAppendingPathComponent:getFileNameForMerchantCopy(copyName)]
                                                               delegate:self
                                                               pageSize:CGSizeMake(kReceiptWidth, MAXFLOAT)
                                                                margins:UIEdgeInsetsZero];
        }
        else {
            [self printFiles:[NSArray arrayWithObjects:self.receiptPathForCustomer, nil]];
        }
    }
    else {
        self.receiptPathForMerchant = htmlToPDF.imagePath;
        [self printFiles:[NSArray arrayWithObjects:self.receiptPathForMerchant, self.receiptPathForCustomer, nil]];
    }
}

- (void)HTMLtoImageOrPDFDidFail:(HtmlToImage*)htmlToPDF {
    
    [APPDELEGATE displayAlertWithTitle:@"Sorry!" andMessage:@"Unable to print receipt. Please try again later."];
    self.receiptPathForCustomer = nil;
    self.receiptPathForMerchant = nil;
}

-(void)printFiles:(NSArray*)pathArray {
    
    BOOL isSuccess = [PrinterAdaptor printReceipt:[UIImage imageWithContentsOfFile:[pathArray firstObject]] shouldOpenDrawer:NO shouldPromptOnFailure:YES];
    if (isSuccess && ([pathArray count] > 1))
            isSuccess = [PrinterAdaptor printReceipt:[UIImage imageWithContentsOfFile:[pathArray objectAtIndex:1]] shouldOpenDrawer:NO shouldPromptOnFailure:YES];
    if (!isSuccess) {
        
        [self retryForReceiptPrint];
    }else {
        [self callForPrintReciept:(lastRecieptIndex+1)];
    }
    
}

#pragma mark - Method for Retry operation when Printer is not connected successfully with iPad
-(void)retryForReceiptPrint {
    
    [MBProgressHUD hideAllHUDsForView:[APPDELEGATE navigationController].view animated:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Printer is unavailable..."
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleAlert];
    NSAttributedString *message = [[NSAttributedString alloc] initWithString:@"Press Retry to resubmit print job." attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15.0f]}];
    [alert setValue:message forKey:@"attributedMessage"];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Retry"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {                                                              [self  callForPrintReciept:lastRecieptIndex];
                                                          }];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               lastRecieptIndex = -1;
                                                           }];
    
    [alert addAction:secondAction];
    [alert addAction:firstAction];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
