//
//  RecordCardViewController.m
//  Active Club Solutions -iPAD App
//
//  Created by Krishna Kant Kaira on 09/06/15.
//  Copyright (c) 2015 Mobiloitte Technologies. All rights reserved.
//

#import "RecordCardViewController.h"
#import "AppDelegate.h"
#import "IDTechHelper.h"
#import "RamblerHelper.h"
#import "EVOHelper.h"
#import "EVODTransaction.h"
#import "WorldPayHelper.h"

@interface RecordCardViewController () <IDTechHelperDelegate, RamblerHelperDelegate, EVOHelperDelegate, WorldPayHelperDelegate,RequestResponseHandlerDelegate> {
    
    NSString *selectedReaderType;
}

@property (strong, nonatomic) IBOutlet UILabel *labelReaderStatus;
@property (nonatomic, strong) MBProgressHUD *progressHUD;

-(IBAction)cancelButtonAction:(id)sender;
-(IBAction)testButtonAction:(id)sender;

@end

@implementation RecordCardViewController

#pragma mark - UIViewController Life cycle methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)
        self.view.superview.bounds = CGRectMake(0.0, 0.0, 400.0, 200.0);
    
    self.view.layer.cornerRadius = 4.0f;
    [self.view.layer setMasksToBounds:YES];
    
    self.view.superview.layer.cornerRadius = 4.0f;
    [self.view.superview.layer setMasksToBounds:YES];
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(awakeFromBackground) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //Check selected card type in settings
    selectedReaderType = [[NSUserDefaults standardUserDefaults] valueForKey:@"POS_Card"];
    
    [self.labelReaderStatus setText:@"Initializing Card Reader . . ."];
    [self checkAndActivateSwiper];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [[IDTechHelper sharedManger] umsdk_deactivate];
    
    [[RamblerHelper sharedManger] deactivateReader];
    [[RamblerHelper sharedManger] setDelegate:nil];
    [[EVOHelper sharedInstance] setDelegate:nil];
    [[WorldPayHelper sharedInstance] setWorldPayDelegate:nil];
    [[WorldPayHelper sharedInstance] disconnectReader];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Event Action for UIApplicationDidBecomeActiveNotification
-(void)awakeFromBackground {
    
    NSString *currentReader = [[NSUserDefaults standardUserDefaults] valueForKey:@"POS_Card"];
    if (![currentReader isEqualToString:selectedReaderType]) {
        selectedReaderType = currentReader;
        
        [self checkAndActivateSwiper];
    }
}

#pragma mark - Other Helper class methods

-(void)checkAndActivateSwiper {
    
    if ([selectedReaderType isEqualToString:@"Shuttle"]) {
        //Shuttle
        
        //deactivate rambler
        [[RamblerHelper sharedManger] cancelSwipe];
        [[RamblerHelper sharedManger] setDelegate:nil];
        
        //deactivate EVO
        [[EVOHelper sharedInstance] setDelegate:nil];
        
        //activate IDTech
        [[IDTechHelper sharedManger] setDelegate:self];
        [[IDTechHelper sharedManger] umsdk_activate];
        [[IDTechHelper sharedManger] swipeCard];
    }
    else if ([selectedReaderType isEqualToString:@"Rambler 3"]) {
        //Rambler 3
        
        //deactivate IDTech
        [[IDTechHelper sharedManger] cancelSwipe];
        [[IDTechHelper sharedManger] setDelegate:nil];
        
        //deactivate EVO
        [[EVOHelper sharedInstance] setDelegate:nil];
        
        //activate Rambler
        [[RamblerHelper sharedManger] setupAndActivateReader];
        [[RamblerHelper sharedManger] setDelegate:self];
        [[RamblerHelper sharedManger] swipeCard];
    }else if ([selectedReaderType isEqualToString:@"EVO"]) {
        
        //deactivate rambler
        [[RamblerHelper sharedManger] cancelSwipe];
        [[RamblerHelper sharedManger] setDelegate:nil];
        
        //deactivate IDTech
        [[IDTechHelper sharedManger] cancelSwipe];
        [[IDTechHelper sharedManger] setDelegate:nil];
        
        [[EVOHelper sharedInstance] setDelegate:self];
        [[[EVOHelper sharedInstance] eVOTransaction] setAmount:[NSDecimalNumber decimalNumberWithString:@"0.01"]];
        [[[EVOHelper sharedInstance] eVOTransaction] setType:TransactionTypeRequestAuthorization];
        [[[EVOHelper sharedInstance] eVOTransaction] setIsForSaveDetails:YES];
        [[[EVOHelper sharedInstance] eVOTransaction] setOrderId:nil];
        [[EVOHelper sharedInstance] initializeSwiper];
        [self.labelReaderStatus setText:[[EVOHelper sharedInstance] getConnectionState]];
    }else if ([selectedReaderType isEqualToString:@"Worldpay"]) {
        
        //deactivate rambler
        [[RamblerHelper sharedManger] cancelSwipe];
        [[RamblerHelper sharedManger] setDelegate:nil];
        
        //deactivate IDTech
        [[IDTechHelper sharedManger] cancelSwipe];
        [[IDTechHelper sharedManger] setDelegate:nil];
        
        //deactivate EVO
        [[EVOHelper sharedInstance] setDelegate:nil];
        
        // Activate Worldpay
        [[WorldPayHelper sharedInstance] setWorldPayDelegate:self];
        [[WorldPayHelper sharedInstance] setPayAmount:[NSDecimalNumber decimalNumberWithString:@"0.01"]];
        [[WorldPayHelper sharedInstance] setIsForSaveCardDetails:YES];
        [[WorldPayHelper sharedInstance] initializeSwiper];
    }
}

-(void)enableSwiper {
    
    if ([selectedReaderType isEqualToString:@"Shuttle"]) {
        //Shuttle
        
        if ([[IDTechHelper sharedManger] isDeviceConnected])
            [[IDTechHelper sharedManger] swipeCard];
        else
            [APPDELEGATE displayAlertWithTitle:@"" andMessage:@"Device not detected. Try reconnecting the device."];
    }
    else if ([selectedReaderType isEqualToString:@"Rambler 3"]) {
        //Rambler 3
        
        if ([[RamblerHelper sharedManger] isDeviceConnected])
            [[RamblerHelper sharedManger] swipeCard];
        else
            [APPDELEGATE displayAlertWithTitle:@"" andMessage:@"Device not detected. Try reconnecting the device."];
    }else if ([selectedReaderType isEqualToString:@"EVO"]) {
        if ([[EVOHelper sharedInstance] isDeviceConnected]) {
            [[[EVOHelper sharedInstance] eVOTransaction] setAmount:[NSDecimalNumber decimalNumberWithString:@"0.01"]];
            [[[EVOHelper sharedInstance] eVOTransaction] setType:TransactionTypeRequestAuthorization];
            [[[EVOHelper sharedInstance] eVOTransaction] setIsForSaveDetails:YES];
            [[[EVOHelper sharedInstance] eVOTransaction] setOrderId:nil];
        }else {
            [APPDELEGATE displayAlertWithTitle:@"" andMessage:@"Device not detected. Try reconnecting the device."];
        }
    }else if ([selectedReaderType isEqualToString:@"Worldpay"]) {
        if ([[WorldPayHelper sharedInstance] isDeviceConnected]) {
            [[WorldPayHelper sharedInstance] setPayAmount:[NSDecimalNumber decimalNumberWithString:@"0.01"]];
            [[WorldPayHelper sharedInstance] setIsForSaveCardDetails:YES];
        }else {
            [APPDELEGATE displayAlertWithTitle:@"" andMessage:@"Device not detected. Try reconnecting the device."];
        }
    }
}

- (void)recordLogginRequestWithRequestOrResponse:(NSString *)logStr andRequestOrResponse:(NSString *)str{
    
    if ([[kDef valueForKey:kLogging] isEqualToString:@"1"] && ([[[NSUserDefaults standardUserDefaults] valueForKey:@"POS_Card"] isEqualToString:@"OpenEdge"] || [[[NSUserDefaults standardUserDefaults] valueForKey:@"POS_Card"] isEqualToString:@"EVO"])) {
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:str forKey:@"LogType"];
        [dict setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"POS_Card"] forKey:@"LogDescription"];
        [dict setValue:logStr forKey:@"Log"];
        
        [self callWebAPIMethod:recordLogging withrequest:[SoapEnvelope recordLoggingWith:dict]];
    }
}

#pragma mark - IBAction Methods
-(IBAction)testButtonAction:(id)sender {
    
    NSMutableDictionary *readerDataInfo = [NSMutableDictionary dictionary];
    [readerDataInfo setValue:@"AtUAgB83IwCDgyUqNjAxMSoqKioqKioqOTQyNF5ESVNDT1ZFUiBURVNUIENBUkReMTgwNCoqKioqKioqKioqPyo7NjAxMSoqKioqKioqOTQyND0xODA0KioqKioqKioqKio/KvmlhPEnEh0RdWrU0Vl0hMy9nMwacHoPSQVXRszMrchshND2pf4lXIyDjHcgabjDDm3p4dfsc+OI7sQdVhgN8PpTZht3V8G+X0iS5/ITeL1qfCVnfRx21TirGAXeT/WwpDQzM1QyNjcxMDZimUmWKAABYADHu0MD" forKey:@"CardEncodedInfo"];
    [readerDataInfo setValue:@"Shuttle" forKey:@"CardReader"];
    [readerDataInfo setValue:@"" forKey:@"Track1Length"];
    [readerDataInfo setValue:@"" forKey:@"Track2Length"];
    [readerDataInfo setValue:@"" forKey:@"KeySerialNumber"];
    [readerDataInfo setValue:@"" forKey:@"ExpirationYear"];
    [readerDataInfo setValue:@"" forKey:@"ExpirationMonth"];
    
    if (self && self.delegate && [self.delegate respondsToSelector:@selector(cardInfoRecordedWith:andReaderType:)])
        [self.delegate cardInfoRecordedWith:readerDataInfo andReaderType:selectedReaderType];
}

-(IBAction)cancelButtonAction:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - * * * * IDTechHelperDelegate Methods * * * *

-(void)cardReaderStatusDidChangedWithMessage:(NSString *)message {
    [self.labelReaderStatus setText:message];
}

-(void)cardReaderstateChanged:(BOOL)isAvailable {
    
    if (isAvailable)
        [self.labelReaderStatus setText:(isAvailable ? @"Waiting for card swipe . . ." : @"Initializing Card Reader . . .")];
}

-(void)cardRawDetails:(NSData *)cardData {
    
    //Check selected card type in settings
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"POS_Card"] isEqualToString:@"Shuttle"]) {
        //Shuttle
        
        NSString *cardInfo = nil;
        if ([[[[[AppUserData sharedInstance] selectedLocation] ConvertShuttleData] lowercaseString] isEqualToString:@"yes"]) {
            cardInfo = [[AppUtility sharedUtility] getEncryptedCardData:[cardData hexadecimalString]];
        }
        else {
            cardInfo = [cardData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn];
        }
        
        NSMutableDictionary *readerDataInfo = [NSMutableDictionary dictionary];
        [readerDataInfo setValue:cardInfo forKey:@"CardEncodedInfo"];
        [readerDataInfo setValue:selectedReaderType forKey:@"CardReader"];
        [readerDataInfo setValue:@"" forKey:@"Track1Length"];
        [readerDataInfo setValue:@"" forKey:@"Track2Length"];
        [readerDataInfo setValue:@"" forKey:@"KeySerialNumber"];
        [readerDataInfo setValue:@"" forKey:@"ExpirationYear"];
        [readerDataInfo setValue:@"" forKey:@"ExpirationMonth"];
        
        if (self && self.delegate && [self.delegate respondsToSelector:@selector(cardInfoRecordedWith:andReaderType:)])
            [self.delegate cardInfoRecordedWith:readerDataInfo andReaderType:selectedReaderType];
        
        [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    }
    else {
        //Rambler 3
        
       // NSLog(@"ERROR :: Rambler 3 reader should get the data!");
    }
}

#pragma mark - RamblerHelperDelegate

-(void)cardReader:(RamblerHelper*)reader didDetectedSwipeWithCardDetails:(NSDictionary *)cardData {
    
    //Check selected card type in settings
    if ([selectedReaderType isEqualToString:@"Rambler 3"]) {
        //Rambler 3
        
        NSMutableDictionary *readerDataInfo = [NSMutableDictionary dictionary];
        [readerDataInfo setValue:[cardData objectForKey:@"encTracks"] forKey:@"CardEncodedInfo"];
        [readerDataInfo setValue:selectedReaderType forKey:@"CardReader"];
        [readerDataInfo setValue:[cardData objectForKey:@"track1Length"] forKey:@"Track1Length"];
        [readerDataInfo setValue:[cardData objectForKey:@"track1Length"] forKey:@"Track2Length"];
        [readerDataInfo setValue:[cardData objectForKey:@"ksn"] forKey:@"KeySerialNumber"];
        
        NSString *expDate = [cardData objectForKey:@"expiryDate"];
        if (expDate.length > 1)
            [readerDataInfo setValue:[expDate substringToIndex:2] forKey:@"ExpirationYear"];
        if (expDate.length > 3)
            [readerDataInfo setValue:[expDate substringFromIndex:2] forKey:@"ExpirationMonth"];
        
        if (self && self.delegate && [self.delegate respondsToSelector:@selector(cardInfoRecordedWith:andReaderType:)])
            [self.delegate cardInfoRecordedWith:readerDataInfo andReaderType:selectedReaderType];
        
        [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    }
    else {
        //Shuttle
        
       // NSLog(@"ERROR :: Shuttle reader should get the data!");
    }
}

-(void)cardReader:(RamblerHelper*)reader statusDidChangedWithMessage:(NSString*)message {
    [self.labelReaderStatus setText:message];
}

-(void)cardReader:(RamblerHelper*)reader stateChanged:(BOOL)isAvailable {
//    if (isAvailable)
//        [self.labelReaderStatus setText:(isAvailable ? @"Waiting for card swipe . . ." : @"Initializing Card Reader . . .")];
}


#pragma mark - EVOHelperDelegate methods
-(void)eMVcardReader:(EVOHelper *)reader statusDidChangedWithMessage:(NSString *)message {
    [self.labelReaderStatus setText:message];
}

-(void)eMVcardReader:(EVOHelper *)reader didFailWithError:(UIAlertController *)alert {
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)rMVcardReader:(EVOHelper *)reader didDetectedEMVWith:(ESBankcardTransactionResponse *)middlewareResponse andCardDetials:(NSString *)cardHolderName {
    
    if ([selectedReaderType isEqualToString:@"EVO"]) {
        //EVO
        NSString *response = [NSString stringWithFormat:@"\nRESPONSE STRING:=================\n%@\n",middlewareResponse];
        [self recordLogginRequestWithRequestOrResponse:response andRequestOrResponse:@"Response"];

        NSMutableDictionary *readerDataInfo = [NSMutableDictionary dictionary];
        [readerDataInfo setValue:[middlewareResponse cardType] forKey:@"CardType"];
        [readerDataInfo setValue:[middlewareResponse paymentAccountDataToken] forKey:@"paymentAccountDataToken"];
        [readerDataInfo setValue:[[middlewareResponse maskedPAN] substringFromIndex:12] forKey:@"maskPAN"];
        [readerDataInfo setValue:[[middlewareResponse maskedPAN] stringByReplacingOccurrencesOfString:[[middlewareResponse maskedPAN] substringToIndex:6] withString:@"XXXXXX"] forKey:@"cardnum"];
        NSString *expDate = [middlewareResponse expire];
        if (expDate.length > 1)
            [readerDataInfo setValue:[expDate substringToIndex:2] forKey:@"ExpirationMonth"];
        if (expDate.length > 3)
            [readerDataInfo setValue:[expDate substringFromIndex:2] forKey:@"ExpirationYear"];
        
        NSArray *arrayName = [cardHolderName componentsSeparatedByString:@" "];
        ;
        
        NSMutableArray *arrayTempName = [NSMutableArray array];
        for (int count = 0; count < arrayName.count; count++) {
            if (count == 0) {
                [readerDataInfo setValue:[arrayName firstObject] forKey:@"CustomerFirstName"];
            }else {
                [arrayTempName addObject:arrayName[count]];
            }
        }
        [readerDataInfo setValue:[arrayTempName componentsJoinedByString:@" "] forKey:@"CustomerLastName"];
        
        if (self && self.delegate && [self.delegate respondsToSelector:@selector(cardInfoRecordedWith:andReaderType:)])
            [self.delegate cardInfoRecordedWith:readerDataInfo andReaderType:selectedReaderType];
        
        [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    }
}

-(void)manageHudWith:(BOOL)showHud {
    if (showHud) {
        [self addProgressHUDToController:self];
    }else {
        [self removeProgressHUDFromController:self];
    }
}

#pragma mark - WorldPayHelperDelegate methods

-(void)manageProgressHudWith:(BOOL)showHud {
    if (showHud) {
        [self addProgressHUDToController:self];
    }else {
        [self removeProgressHUDFromController:self];
    }
}

-(void)readerConnectionStatusDidChangedWithMessage:(NSString*)message {
    [self.labelReaderStatus setText:message];
}

-(void)cardInfomationWith:(NSDictionary *)responseDict {
    
    NSString *strMessage = @"Something went wrong. Please try again!";
    if ([[responseDict objectForKey:@"Success"] integerValue] == 0) {
        
        if ([[responseDict objectForKey:@"ResponseCode"] integerValue] == 4) {
            strMessage = @"Transaction has been cancelled from the terminal. Please try again!";
        } else {
            strMessage = [responseDict objectForKey:@"ResponseMessage"];
        }
        // }
        UIAlertController *alertVC = [UIAlertController
                                      alertControllerWithTitle:@"" message:strMessage
                                      preferredStyle:UIAlertControllerStyleAlert];
        [alertVC addAction:[UIAlertAction
                            actionWithTitle:@"Ok"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction* action) {
                                
                                [alertVC dismissViewControllerAnimated:YES completion:nil];
                            }
                            ]];
        [self presentViewController:alertVC animated:YES completion:nil];
        return;
    }
    
    if ([selectedReaderType isEqualToString:@"Worldpay"]) {
        //Worlpay
        
        NSMutableDictionary *readerDataInfo = [NSMutableDictionary dictionary];
        NSDictionary *cardDetails = [[responseDict objectForKey:@"Transaction"] objectForKey:@"Card"];
        NSDictionary *receiptData = [responseDict objectForKey:@"ReceiptData"];
        [readerDataInfo setValue:[cardDetails objectForKey:@"Brand"] forKey:@"CardType"];
        [readerDataInfo setValue:[[receiptData objectForKey:@"MaskedPan"] substringFromIndex:12] forKey:@"maskPAN"];
        [readerDataInfo setValue:[[receiptData objectForKey:@"MaskedPan"] stringByReplacingOccurrencesOfString:[[receiptData objectForKey:@"MaskedPan"] substringToIndex:12] withString:@"XXXXXXXXXXXX"] forKey:@"cardnum"];
        [readerDataInfo setValue:[cardDetails objectForKey:@"ExpirationMonth"] forKey:@"ExpirationMonth"];
        [readerDataInfo setValue:[cardDetails objectForKey:@"ExpirationYear"] forKey:@"ExpirationYear"];
        
        NSArray *arrayName = [[receiptData objectForKey:@"CardHolderName"] componentsSeparatedByString:@" "];
        ;
        NSMutableArray *arrayTempName = [NSMutableArray array];
        for (int count = 0; count < arrayName.count; count++) {
            if (count == 0) {
                [readerDataInfo setValue:[arrayName firstObject] forKey:@"CustomerFirstName"];
            }else {
                [arrayTempName addObject:arrayName[count]];
            }
        }
        [readerDataInfo setValue:[arrayTempName componentsJoinedByString:@" "] forKey:@"CustomerLastName"];
        
        if (self && self.delegate && [self.delegate respondsToSelector:@selector(cardInfoRecordedWith:andReaderType:)])
            [self.delegate cardInfoRecordedWith:readerDataInfo andReaderType:selectedReaderType];
        
        [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    }
}

#pragma mark - **** Progress HUD Helper ****
-(void)addProgressHUDToController :(UIViewController*)controller {
    
    [MBProgressHUD hideAllHUDsForView:controller.view animated:YES];
    self.progressHUD = [MBProgressHUD showHUDAddedTo:controller.view animated:YES];
    [self.progressHUD setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.0]];
    [self.progressHUD setColor:[UIColor colorWithWhite:1.0 alpha:0.9]];
}

-(void)removeProgressHUDFromController:(UIViewController*)controller {
    [MBProgressHUD hideAllHUDsForView:controller.view animated:YES];
    self.progressHUD = nil;
}

#pragma mark - * * * * Webservices Calling * * * *

-(void)callWebAPIMethod:(WebMethodType)methodType withrequest:(NSString*)request {
    
    RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
    objHandler.delegate = self;
    [objHandler call_SOAP_POSTMethodWithRequest:request andMethodName:methodType andController:[APPDELEGATE navigationController]];
}

#pragma mark - * * * * RequestResponseHandlerDelegate * * * *
-(void)ResponseContentFromServer:(RequestResponseHandler *)responseObj withData:(id)jsonData forMethod:(WebMethodType)methodType {
    
    if ([responseObj.responseCode intValue] == SERVER_RESPONSE_SUCCESS_CODE) {
        
        switch (methodType) {
                
                
            case recordLogging: {
                
                NSDictionary *responseDict = [jsonData objectForKey:@"ResponseStatus"];
                NSLog(@"%@", responseDict);
                
            }
                break;
                
            default:
                break;
        }
    }
    else {
        
        [APPDELEGATE displayAlertWithTitle:@"" andMessage:responseObj.responseMessage];
    }
}


@end
