//
//  CustomerDetailsVC.m
//  Active Club Solutions -iPAD App
//
//  Created by Krishna Kant Kaira on 16/09/14.
//  Copyright (c) 2014 Mobiloitte Technologies. All rights reserved.
//

#define kTabBarButton(x) (UIButton*)[self.viewTabbar viewWithTag:x]

#import "CustomerDetailsVC.h"
#import "CustomerSelectionPopOver.h"
#import "BillingViewController.h"
#import "MailingViewController.h"
#import "WholesaleCustomer.h"
#import "MembershipVC.h"

@interface CustomerDetailsVC () <UITextFieldDelegate, UIPopoverControllerDelegate, RequestResponseHandlerDelegate, CustomPopUpDelegate, MembershipDelegate> {
    BOOL isToUpdateMemberShipONLY;
    CustomerItem *selectedcustomer;
}

@property (nonatomic, strong) CustomerItem *customerForNewOrder;

@property (nonatomic, strong) UIPopoverController *popOver;
@property (nonatomic, assign) BOOL isCancelled;

@property (nonatomic, strong) UINavigationController *navController;
@property (nonatomic, strong) IBOutlet UIView *viewTabbar;
@property (nonatomic, strong) IBOutlet UIView *containerView;

@property (nonatomic, strong) IBOutlet UIButton *button_profile;
@property (nonatomic, strong) IBOutlet UIButton *button_shipping;
@property (nonatomic, strong) IBOutlet UIButton *button_billing;
@property (nonatomic, strong) IBOutlet UIButton *button_mailing;
@property (nonatomic, strong) IBOutlet UIButton *button_orderHistory;
@property (nonatomic, strong) IBOutlet UIButton *button_continueOrder;
@property (nonatomic, strong) IBOutlet UIButton *button_save;
@property (nonatomic, strong) IBOutlet UIButton *button_clear;

@property (nonatomic, strong) IBOutlet UIButton *button_cancel;
@property (strong, nonatomic) IBOutlet UIButton *button_newOrder;

@property (nonatomic, strong) IBOutlet UIButton *button_addCustomer;
@property (nonatomic, strong) IBOutlet UIButton *button_zoomInOutButton;
@property (nonatomic, strong) IBOutlet UITextField *textField_customerSearch;
@property (strong, nonatomic) IBOutlet UIView *view_bottom;

@property (nonatomic, strong) ProfileViewController *controller_profile;
@property (nonatomic, strong) ShippingInfoVC *controller_shippingInfo;
@property (nonatomic, strong) BillingViewController *controller_billing;
@property (nonatomic, strong) MailingViewController *controller_mailing;
@property (nonatomic, strong) CustomerOrderHistoryVC *controller_orderHistory;
@property (nonatomic, strong) MembershipVC *membershipVC;

-(IBAction)commonTabButtonAction:(UIButton*)sender;
-(IBAction)otherButtonAction:(UIButton*)sender;
-(IBAction)zoomInOutButtonAction:(UIButton *)sender;

@end

@implementation CustomerDetailsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.controller_profile = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
        self.controller_shippingInfo = [[ShippingInfoVC alloc] initWithNibName:@"ShippingInfoVC" bundle:nil];
        self.controller_billing = [[BillingViewController alloc] initWithNibName:@"BillingViewController" bundle:nil];
        self.controller_mailing = [[MailingViewController alloc] initWithNibName:@"MailingViewController" bundle:nil];
        self.controller_orderHistory = [[CustomerOrderHistoryVC alloc] initWithNibName:@"CustomerOrderHistoryVC" bundle:nil];
        self.membershipVC = [[MembershipVC alloc] initWithNibName:@"MembershipVC" bundle:nil];
    }
    
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    isToUpdateMemberShipONLY = NO;
    
    [self.button_addCustomer setExclusiveTouch:YES];
    [self.button_zoomInOutButton setExclusiveTouch:YES];
    [self.button_profile setExclusiveTouch:YES];
    [self.button_shipping setExclusiveTouch:YES];
    [self.button_billing setExclusiveTouch:YES];
    [self.button_mailing setExclusiveTouch:YES];
    [self.button_orderHistory setExclusiveTouch:YES];
    [self.button_signForOrder setExclusiveTouch:YES];
    [self.button_save setExclusiveTouch:YES];
    [self.button_clear setExclusiveTouch:YES];

    [self.button_cancel setExclusiveTouch:YES];
    [self.button_signForOrder setExclusiveTouch:YES];
    [self.button_void setExclusiveTouch:YES];
    [self.button_swap setExclusiveTouch:YES];
    
    [self refreshPendingCustomer];
    
    if (self.isFromCheckOut) {
        
        [self refreshInfoForCustomer:self.customerToBeAddOrEdit];
        [self.textField_customerSearch setText:[self.customerToBeAddOrEdit name]];
        
        if (self && self.delegate && [self.delegate respondsToSelector:@selector(didSelectedCustomer:)])
            [self.delegate didSelectedCustomer:self.customerToBeAddOrEdit];
        
        NSString *custID = [self.customerToBeAddOrEdit customerID];
        if (custID && [custID length]>0)
            [self callWebAPIMethod:getCustomers withrequest:[SoapEnvelope getCustomersListFor:custID]];

    }
    
    [self.button_continueOrder setHidden:!self.isFromCheckOut];
    
    [self prepareContainerController:self.controller_profile];
    [self prepareNavigationBar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backToCustomerDetailsVC:) name:@"CustomerDetailsVCNotification" object:nil];

}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.button_billing setHidden:![[[AppUserData sharedInstance] selectedLocation] enablecustomerbilling]];
    [self prepareNavigationBar];
    
    // Hide bottom right buttons when comming for wholesale section***********
    if ([[[AppUserData sharedInstance] selectedLocation] enableWholesaleTab]) {
        [self.view_bottom setHidden:NO];
        [self.button_save setFrame:CGRectMake(0, 380, 174, 40)];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    if (self.presentedViewController)
        [self dismissViewControllerAnimated:NO completion:nil];
    
    if ([self.popOver isPopoverVisible])
        [self.popOver dismissPopoverAnimated:NO];
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"OpenEdgeDataNotification" object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//The method that gets called when a CustomerDetailsNotification has been posted by an object you observe for
- (void)backToCustomerDetailsVC:(NSNotification *)notification {
    
    [self callWebAPIMethod:getCustomers withrequest:[SoapEnvelope getCustomersListWithLoyaltyCardNumber:notification.object]];


}

#pragma mark - * * * Class private methods * * *
-(void)prepareContainerController:(id)contrller {
    
    self.navController = [[UINavigationController alloc] initWithRootViewController:contrller];
    [self addChildViewController:self.navController];
    [self.navController.view setFrame:self.containerView.bounds];
    [self.containerView addSubview:self.navController.view];
    [self.view setNeedsLayout];
    
    [self manageInitialSelection:contrller];
    [self.navController didMoveToParentViewController:self];
}

-(void)manageInitialSelection:(id)controller {
    
    [self setDelegate:controller];
    
    [self.button_signForOrder setHidden:YES];
    [self.button_void setHidden:YES];
    [self.button_swap setHidden:YES];
    [self.button_zoomInOutButton setHidden:YES];
    
    [self.button_save setHidden:NO];
    [self.button_clear setHidden:NO];

    [self.button_cancel setHidden:NO];
    
    if ([controller isKindOfClass:[ProfileViewController class]]) {
        [self setSelectedIndex:0];
    }
    else if ([controller isKindOfClass:[MailingViewController class]]) {
        [self setSelectedIndex:1];
    }
    else if ([controller isKindOfClass:[BillingViewController class]]) {
        [self setSelectedIndex:2];
    }
    else if ([controller isKindOfClass:[ShippingInfoVC class]]) {
        [self setSelectedIndex:3];
    }else if ([controller isKindOfClass:[MembershipVC class]]) {
//        [self.button_save setHidden:YES];

        [self setSelectedIndex:6];
    }
    else if ([controller isKindOfClass:[CustomerOrderHistoryVC class]]) {
        
        [self.button_signForOrder setHidden:NO];
        [self.button_void setHidden:NO];
        [self.button_swap setHidden:NO];
        [self.button_zoomInOutButton setHidden:NO];
        
        [self.button_save setHidden:YES];
        [self.button_clear setHidden:YES];

        [self.button_cancel setHidden:YES];
        
        
        [self setSelectedIndex:4];
    }
    else {
        //used for no selection
        [self setSelectedIndex:10];
    }
}

-(void)setSelectedIndex:(NSInteger)index {
    
    int baseTag = 50;
    for (int i = baseTag; i<baseTag+7; i++) {
        UIButton *btn = kTabBarButton(i);
        [btn setSelected:((index+baseTag) == i)];
        [btn setExclusiveTouch:YES];
    }
}

-(void)prepareNavigationBar {
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    
    [[self navigationItem] setHidesBackButton:YES];
    [[self navigationController] setNavigationBarHidden:YES];
    
    //set navigationBar's title
    [[APPDELEGATE baseController] setScreenTitle:@"Customers"];
    [[APPDELEGATE baseController] setSelectedIndex:5];
    
    self.selectedPickUpLocation = [[[AppUserData sharedInstance] selectedLocation] locationName];
}

#pragma mark - Validation
-(InvalidFieldToBeFirstResponder)isAnyInvalidField {
    
    InvalidFieldToBeFirstResponder invalidTextFieldType = InvalidFieldToBeFirstResponder_None;
//    [self saveData];
    
    NSString *errMsg = nil;
    if ([self.pendingCustomer.firstname length] > 0) {
        if ([self.pendingCustomer.lastname length] > 0) {
            if ([self.pendingCustomer.emailaddress length] > 0) {
                if (1 > 0) {
                    
                    if ([self.pendingCustomer.isgift isIdenticaleTo:@"Yes"]) {
                        if ([self.pendingCustomer.emailaddress2 length] > 0) {
                            if ([self.pendingCustomer.emailaddress2 isValidEmail]) {
                                if (![self.pendingCustomer.giftnotes isBlank]) {
                                    
                                }
                                else {
                                    invalidTextFieldType = InvalidFieldToBeFirstResponder_GifteeNotes;
                                    errMsg = @"Gift Note cannot be blank.";
                                }
                            }
                            else {
                                invalidTextFieldType = InvalidFieldToBeFirstResponder_GifteeEmail;
                                errMsg = @"Please enter valid Giftee E-mail.";
                            }
                        }
                        else{
                            invalidTextFieldType = InvalidFieldToBeFirstResponder_GifteeEmail;
                            errMsg = @"Giftee E-mail Address cannot be blank.";
                        }
                    }
                    
                    if ([self.pendingCustomer.emailaddress2 length] > 0) {
                        if (![self.pendingCustomer.emailaddress2 isValidEmail]) {
                            
                            invalidTextFieldType = InvalidFieldToBeFirstResponder_GifteeEmail;
                            errMsg = @"Please enter valid Giftee E-mail.";
                        }
                    }
                }
                else {
                }
            }
            else {
                invalidTextFieldType = InvalidFieldToBeFirstResponder_Email;
                errMsg = @"E-mail Address cannot be blank.";
            }
        }
        else {
            invalidTextFieldType = InvalidFieldToBeFirstResponder_LastName;
            errMsg = @"Last Name cannot be blank.";
        }
    }
    else {
        invalidTextFieldType = InvalidFieldToBeFirstResponder_FirstName;
        errMsg = @"First Name cannot be blank.";
    }
    
    if (errMsg)
        [APPDELEGATE displayAlertWithTitle:@"Error!" andMessage:errMsg];
    
    return invalidTextFieldType;
}

#pragma mark - * * * * Button Action * * * * *
-(IBAction)commonTabButtonAction:(UIButton*)sender {
    
    // return if tab is already selected
    if (sender.isSelected) {
        return;
    }
    
    [[APPDELEGATE window] endEditing:YES];
    
    [self.navController popToRootViewControllerAnimated:NO];
    id controller = nil;
    
    switch ([sender tag] % 50) {
            
        case 0: {
            //Profile
            controller = self.controller_profile;
        }
            break;
            
        case 1: {
            //Mailing
            
            controller = self.controller_mailing;
        }
            break;
            
        case 2: {
            //Billing
            //When adding a new customer it needs to copy the first and last name to the shipping screen so they don't have to enter it twice
            if (self.pendingCustomer.customerID.length == 0) {
                self.pendingCustomer.customerfirstname = self.pendingCustomer.firstname;
                self.pendingCustomer.customerlastname = self.pendingCustomer.lastname;
            }
            controller = self.controller_billing;
        }
            break;
            
            
        case 3: {
            //Shipping
            
            //When adding a new customer it needs to copy the first and last name to the shipping screen so they don't have to enter it twice
            if (self.pendingCustomer.customerID.length == 0) {
                
                self.pendingCustomer.shipfirstname = self.pendingCustomer.firstname;
                self.pendingCustomer.shiplastname = self.pendingCustomer.lastname;
            }
            
            controller = self.controller_shippingInfo;
        }
            break;
            
        case 4: {
            //Order History
            controller = self.controller_orderHistory;
        }
            break;
            
        case 6: {
            //Membership
            //            [APPDELEGATE displayAlertWithTitle:@"" andMessage:@"Work in progress"];
            if (selectedcustomer != nil) {
                self.membershipVC.selectedCustomer = selectedcustomer;
            }
            controller = self.membershipVC;
            self.membershipVC.membershipDelegate = self;
        }
            break;

            
        default:
            break;
    }
    
    if (controller) {
        [self.navController setViewControllers:[NSArray arrayWithObject:controller] animated:NO];
    }
    
    [self manageInitialSelection:controller];
}

-(void)navigatebackToCheckout {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    if (self && self.delegate && [self.delegate respondsToSelector:@selector(didTappedButton:)])
        [self.delegate didTappedButton:ButtonType_CONTINUE_ORDER];
    
    if (!self.isCancelled) {
    }
    
    if (self && self.customerAddEditdelegate && [self.customerAddEditdelegate respondsToSelector:@selector(customerUpdated:isCancelled:)])
        [self.customerAddEditdelegate customerUpdated:self.customerToBeAddOrEdit isCancelled:self.isCancelled];
}

-(IBAction)otherButtonAction:(UIButton*)sender {
    
    switch ([sender tag]) {
            
        case 70: {
            //Continue Order...
            self.isCancelled = NO;
            [self checkForCustomerUpdate:self ifNotPerformSelector:@selector(navigatebackToCheckout) withObj:nil];
        }
            break;
            
        case 71: {
            //Save
            
            InvalidFieldToBeFirstResponder invalidField = [self isAnyInvalidField];
            if (invalidField == InvalidFieldToBeFirstResponder_None) {
                [self savePendingCustomer];
            }
            else {
                if (![self.button_profile isSelected]) {
                    [self.navController popToRootViewControllerAnimated:NO];
                    [self.navController setViewControllers:[NSArray arrayWithObject:self.controller_profile] animated:NO];
                    [self manageInitialSelection:self.controller_profile];
                }
                
                [self.controller_profile makeFirstResponder:invalidField];
            }
        }
            break;
            
        case 72: {
            //Clear
            [self addNewCustomer];
//            if (self && self.delegate && [self.delegate respondsToSelector:@selector(didTappedButton:)])
//                [self.delegate didTappedButton:ButtonType_CANCEL];
        }
            break;
            
        case 73: {
            //Sign For Order
            
            if (self && self.delegate && [self.delegate respondsToSelector:@selector(didTappedButton:)])
                [self.delegate didTappedButton:ButtonType_SIGN_FOR_ORDER];
        }
            break;
            
        case 74: {
            //Void
            
            if (self && self.delegate && [self.delegate respondsToSelector:@selector(didTappedButton:)])
                [self.delegate didTappedButton:ButtonType_VOID];
        }
            break;
            
        case 75: {
            //Swap
            
            if (self && self.delegate && [self.delegate respondsToSelector:@selector(didTappedButton:)])
                [self.delegate didTappedButton:ButtonType_SWAP];
        }
            break;
            
        case 55: {
            //New Order...
            //this button will take the user to the New Order Screen but will also add the Customer's name and id to the new order automatically.
            if ([[[AppUserData sharedInstance] selectedLocation]enableWholesaleTab]) {
                WholesaleCustomer *customer = [[WholesaleCustomer alloc]init];
                customer.companyName = self.pendingCustomer.companyname;
                customer.customerId = self.pendingCustomer.customerID;
                customer.notes = self.pendingCustomer.notes;
                [[APPDELEGATE baseController] navigateToNewWholesale:customer];
            }else {
                self.customerForNewOrder = [[CustomerItem alloc] init];
                self.customerForNewOrder.customerID = self.pendingCustomer.customerID;
                
                if (self.pendingCustomer.firstname) {
                    
                    if (self.pendingCustomer.lastname)
                        self.customerForNewOrder.name = [NSString stringWithFormat:@"%@ %@",self.pendingCustomer.firstname,self.pendingCustomer.lastname];
                    else
                        self.customerForNewOrder.name = self.pendingCustomer.firstname;
                }
                else
                    self.customerForNewOrder.name = @"";
                self.customerForNewOrder.firstname = self.pendingCustomer.firstname;
                self.customerForNewOrder.lastname = self.pendingCustomer.lastname;
                
                [self checkForCustomerUpdate:self ifNotPerformSelector:@selector(navigateToNewOrder) withObj:nil];
            }
        }
            break;
            
        case 80: {
            //Add Customer
            
            [self checkForCustomerUpdate:self ifNotPerformSelector:@selector(addNewCustomer) withObj:nil];
        }
            break;
            
        default:
            break;
    }
}

-(IBAction)zoomInOutButtonAction:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    
    if (self && self.delegate && [self.delegate respondsToSelector:@selector(didTappedButton:)])
        [self.delegate didTappedButton:(sender.selected ? ButtonType_ZOOM_IN : ButtonType_ZOOM_OUT)];
}

-(BOOL)areMendatoryFieldsAvailable {
    
    BOOL isVerified = NO;
    
    NSString *errMsg = nil;
    if ([self.pendingCustomer.firstname length] > 0) {
        if ([self.pendingCustomer.lastname length] > 0) {
            if ([self.pendingCustomer.emailaddress length] > 0) {
                if ([self.pendingCustomer.emailaddress isValidEmail] > 0) {
                    isVerified = YES;
                    
                    if ([self.pendingCustomer.isgift isIdenticaleTo:@"Yes"]) {
                        isVerified = NO;
                        if ([self.pendingCustomer.emailaddress2 length] > 0) {
                            if ([self.pendingCustomer.emailaddress2 isValidEmail]) {
                                if (![self.pendingCustomer.giftnotes isBlank]) {
                                    
                                    isVerified = YES;
                                }
                                else {
                                    errMsg = @"Gift Note cannot be blank.";
                                }
                            }
                            else {
                                errMsg = @"Please enter valid Giftee E-mail.";
                            }
                        }
                        else{
                            errMsg = @"Giftee E-mail Address cannot be blank.";
                        }
                    }
                    
                    if ([self.pendingCustomer.emailaddress2 length] > 0) {
                        if (![self.pendingCustomer.emailaddress2 isValidEmail]) {
                            isVerified = NO;
                            errMsg = @"Please enter valid Giftee E-mail.";
                        }
                    }
                }
                else {
                    errMsg = @"Please enter a valid E-mail Address.";
                }
            }
            else {
                errMsg = @"E-mail Address cannot be blank.";
            }
        }
        else {
            errMsg = @"Last Name cannot be blank.";
        }
    }
    else {
        errMsg = @"First Name cannot be blank.";
    }
    
    if (errMsg)
        [APPDELEGATE displayAlertWithTitle:@"" andMessage:errMsg];
    
    return isVerified;
}

-(void)checkForCustomerUpdate:(id)delegate ifNotPerformSelector:(SEL)selctor withObj:(id)obj {
    
    if (self.pendingCustomer.isCustomerUpdated) {
        
        UIAlertView * confirmation = [[UIAlertView alloc] initWithTitle:@"Do you want to save the changes for this customer?" message:@"" cancelButtonTitle:nil otherButtonTitles:[NSArray arrayWithObjects:@"YES", @"NO",nil] onCancel:^{
            
        } onDismiss:^(NSInteger buttonIndex) {
            switch (buttonIndex) {
                case 0: {
                    //NO
                    [self refreshPendingCustomer];
                    self.isCancelled = YES;
                    if (selctor)
                        [delegate performSelector:selctor withObject:obj afterDelay:0];
                }
                    break;
                    
                case -1: {
                    //YES
                    if ([self areMendatoryFieldsAvailable])
                        [self savePendingCustomer];
                }
                    break;
                    
                default:
                    break;
            }
        }];
        
        [confirmation show];
        [APPDELEGATE setVisibleAlertView:confirmation];
        [APPDELEGATE setDismissButtonIndex:0];
    }
    else {
        if (selctor)
            [delegate performSelector:selctor withObject:obj afterDelay:0];
    }
}

-(void)refreshPendingCustomer {
    
    self.pendingCustomer = [[CustomerItem alloc] init];
    
    self.pendingCustomer.isCustomerUpdated = NO;
    
    //6) When adding a new customer the birthdate should default to exactly 21 years old on that day. (Ref: mail, Time: Tue, Jan 6, 2015 at 7:54 AM)
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.year = -21;
    NSDate *defaultDOB = [[NSCalendar currentCalendar] dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
    self.pendingCustomer.birthdate = convertToString(defaultDOB, @"yyyy/MM/dd");
    NSString *defaultShipMethod = [kDef valueForKey:kDefaultShipMethod];
    self.pendingCustomer.billshippingto = (defaultShipMethod ? defaultShipMethod : @"Ship");
    self.pendingCustomer.isgift = @"No";
    
    //3.  When adding a new customer the Residential checkbox should always be checked.
    self.pendingCustomer.residentialaddress = @"Yes";
    
    if (self && self.delegate && [self.delegate respondsToSelector:@selector(didTappedButton:)])
        [self.delegate didTappedButton:ButtonType_ADD];
}

-(void)addNewCustomer {
    
    [self.textField_customerSearch setText:@""];
    [self refreshPendingCustomer];
    
    //1) Pressing Add Customer should navigate to Profile Screen (Ref: mail, Time: Tue, Jan 6, 2015 at 7:54 AM)
    if (![self.button_profile isSelected]) {
        [self.navController popToRootViewControllerAnimated:NO];
        [self.navController setViewControllers:[NSArray arrayWithObject:self.controller_profile] animated:NO];
        [self manageInitialSelection:self.controller_profile];
        
        [self.controller_profile makeFirstResponder:InvalidFieldToBeFirstResponder_FirstName];
    }
}

-(void)navigateToNewOrder {
    
    [[APPDELEGATE baseController] moveToNewOrderWithCustomer:self.customerForNewOrder];
}

#pragma  mark - * * * * UITextFieldDelegate Methods* * * *
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    BOOL canEdit = !(self.pendingCustomer.isCustomerUpdated);
    if (!canEdit)
        [self checkForCustomerUpdate:self.textField_customerSearch ifNotPerformSelector:@selector(becomeFirstResponder) withObj:nil];
    
    return canEdit;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    if ([textField isEqual:self.textField_customerSearch]) {
        
        self.pendingCustomer.customerID = @"";
        if ((self.button_orderHistory.selected  || self.button_shipping.selected) &&  self && self.delegate && [self.delegate respondsToSelector:@selector(didSelectedCustomer:)])
            [self.delegate didSelectedCustomer:nil];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([[[AppUserData sharedInstance] selectedLocation] enableWholesaleTab]) {
        [self callWebAPIMethod:lookupWholesaleCustomers withrequest:[SoapEnvelope lookUpWholesaleCustomerWith:self.textField_customerSearch.text]];

    }else {
        if ([textField isEqual:self.textField_customerSearch] && (textField.text.length > 0)) {
            isToUpdateMemberShipONLY = NO;
            [self callWebAPIMethod:lookupCustomer withrequest:[SoapEnvelope getRequsetForLookupCustomerForCustomer:textField.text]];
        }
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    self.pendingCustomer.isCustomerUpdated = self.isFromCheckOut;
    return YES;
}

#pragma mark - * * * * Save pending customer * * * *

-(void)savePendingCustomer {
    
    [self callWebAPIMethod:addUpdateCustomerRecord withrequest:[SoapEnvelope getaddUpdateCustomerRecord:[NSMutableArray arrayWithObject:self.pendingCustomer] withPickupLocation:self.selectedPickUpLocation]];
}

#pragma mark - * * * * Webservices Calling * * * *

-(void)callWebAPIMethod:(WebMethodType)methodType withrequest:(NSString*)request {
    
    RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
    objHandler.delegate = self;
    [objHandler call_SOAP_POSTMethodWithRequest:request andMethodName:methodType andController:[APPDELEGATE navigationController]];
}

#pragma mark - * * * * RequestResponseHandlerDelegate * * * *
-(void)ResponseContentFromServer:(RequestResponseHandler *)responseObj withData:(id)jsonData forMethod:(WebMethodType)methodType {
    
    if ([responseObj.responseCode intValue] == SERVER_RESPONSE_SUCCESS_CODE) {
        
        switch (methodType) {
                
            case lookupCustomer:
            case lookupWholesaleCustomers: {
                
                NSArray *customerData = [[jsonData objectForKey:@"CustomerArray"] objectForKey:@"CustomerLookup"];
                
                NSMutableArray *array_customerData = [NSMutableArray array];
                
                if ([customerData isKindOfClass:[NSDictionary class]]) {
                    [array_customerData addObject:[CustomerItem getCustomerFrom:(NSDictionary*)customerData]];
                }
                else {
                    for (NSDictionary *item in customerData)
                        [array_customerData addObject:[CustomerItem getCustomerFrom:item]];
                }
                
                if (isToUpdateMemberShipONLY) {
                    self.customerToBeAddOrEdit.memberships = [(CustomerItem*)[array_customerData firstObject] memberships];
                }
                else {
                    if ([array_customerData count] == 0)
                        [APPDELEGATE displayAlertWithTitle:@"" andMessage:@"No customer found with specified keyword."];
                    else {
                        //need to show pop over with customer data
                        CustomerSelectionPopOver *customerslectionVC = [[CustomerSelectionPopOver alloc] initWithNibName:@"CustomerSelectionPopOver" bundle:nil];
                        customerslectionVC.array_commonData = array_customerData;
                        [customerslectionVC setDelegate:self];
                        self.popOver = [[UIPopoverController alloc] initWithContentViewController:customerslectionVC];
                        [self.popOver setDelegate:self];
                        [self.popOver setPopoverContentSize:CGSizeMake(320.0, 500.0)];
                        [self.popOver presentPopoverFromRect:self.textField_customerSearch.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
                    }
                }
            }
                break;
                
            case getCustomers: {
                
                [self.textField_customerSearch setText:@""];
                CustomerItem *customer;
                NSArray *customerData = [[jsonData objectForKey:@"CustomerArray"] objectForKey:@"Customer"];
                if (customerData == nil) {
                    break;
                }
                NSDictionary *customerDict ;
                if ([customerData isKindOfClass:[NSDictionary class]]) {
                    customerDict = (NSDictionary*)customerData;
                }
                else {
                    customerDict = [customerData firstObject];
                }
                
                customer = [CustomerItem getCustomerFrom:customerDict];
                selectedcustomer = customer;
                
                [self refreshInfoForCustomer:customer];
                
                if (self.isFromCheckOut) {
                    
                    if ([self.customerToBeAddOrEdit.customerID integerValue] == [customer.customerID integerValue])
                        customer.memberships = self.customerToBeAddOrEdit.memberships;
                    self.customerToBeAddOrEdit = customer;
                }
                
                if (self && self.controller_profile && [self.controller_profile respondsToSelector:@selector(didFetchedDetailsForCustomer:withData:fromSender:)]) {
                    [self.controller_profile didFetchedDetailsForCustomer:customer withData:customerDict fromSender:self];
                }
                
                if (self && self.delegate && [self.delegate respondsToSelector:@selector(didFetchedDetailsForCustomer:withData:fromSender:)]){
                    [self.delegate didFetchedDetailsForCustomer:customer withData:customerDict fromSender:self];
                }
            }
                break;
                
            case addUpdateCustomerRecord: {
                
                self.pendingCustomer.customerID = [jsonData getTextValueForKey:@"APSCustomerId"];
                
                if (self.isFromCheckOut) {
                    
                    self.customerToBeAddOrEdit.customerID = self.pendingCustomer.customerID;
                    self.customerToBeAddOrEdit.name = [NSString stringWithFormat:@"%@ %@",self.pendingCustomer.firstname,self.pendingCustomer.lastname];
                    self.customerToBeAddOrEdit.firstname = self.pendingCustomer.firstname;
                    self.customerToBeAddOrEdit.lastname = self.pendingCustomer.lastname;
                    self.customerToBeAddOrEdit.phone = self.pendingCustomer.phone;
                    self.customerToBeAddOrEdit.emailaddress = self.pendingCustomer.emailaddress;
                    self.customerToBeAddOrEdit.birthdate = self.pendingCustomer.birthdate;
                    self.customerToBeAddOrEdit.notes = self.pendingCustomer.notes;
                    self.customerToBeAddOrEdit.isgift = self.pendingCustomer.isgift;
                    self.customerToBeAddOrEdit.emailaddress2 = self.pendingCustomer.emailaddress2;
                    self.customerToBeAddOrEdit.spousename = self.pendingCustomer.spousename;
                    //31/05/2016
                    self.customerToBeAddOrEdit.type = self.pendingCustomer.type;
                    self.customerToBeAddOrEdit.shiptype = self.pendingCustomer.shiptype;
                    self.customerToBeAddOrEdit.shipfirstname = self.pendingCustomer.shipfirstname;
                    self.customerToBeAddOrEdit.shiplastname = self.pendingCustomer.shiplastname;
                    self.customerToBeAddOrEdit.shipaddress1 = self.pendingCustomer.shipaddress1;
                    self.customerToBeAddOrEdit.shipaddress2 = self.pendingCustomer.shipaddress2;
                    self.customerToBeAddOrEdit.shipcity = self.pendingCustomer.shipcity;
                    self.customerToBeAddOrEdit.shipstate = self.pendingCustomer.shipstate;
                    self.customerToBeAddOrEdit.shipZIP = self.pendingCustomer.shipZIP;
                    self.customerToBeAddOrEdit.paymentTerms = self.pendingCustomer.paymentTerms;
                    isToUpdateMemberShipONLY = YES;
                    [self callWebAPIMethod:lookupCustomer withrequest:[SoapEnvelope getRequsetForLookupCustomerForCustomerWithID:self.customerToBeAddOrEdit.customerID]];
                    
                }
                
                [APPDELEGATE displayAlertWithTitle:@"" andMessage:responseObj.responseMessage];
                
                //delete saved customer from local DB and refresh screen with new customer created.
                //                [[DBHelper instance] deletePendingCustomerfromLocalDB:self.pendingCustomer];
                self.pendingCustomer.isCustomerUpdated = NO;
                [[DBHelper instance] saveContext];
            }
                break;
                
            default:
                break;
        }
    }
    else {
        
        [APPDELEGATE displayAlertWithTitle:@"" andMessage:responseObj.responseMessage];
    }
}

-(void)refreshInfoForCustomer:(CustomerItem*)selectedCustomer {
    //set values for selected customer
    
    self.pendingCustomer = selectedCustomer;
//    return;
    
    self.pendingCustomer.customerID = [(CustomerItem*)selectedCustomer customerID];
    
    self.pendingCustomer.firstname = [(CustomerItem*)selectedCustomer firstname];
    self.pendingCustomer.lastname = [(CustomerItem*)selectedCustomer lastname];
    self.pendingCustomer.phone = [(CustomerItem*)selectedCustomer phone];
    self.pendingCustomer.emailaddress = [(CustomerItem*)selectedCustomer emailaddress];
    self.pendingCustomer.birthdate = [(CustomerItem*)selectedCustomer birthdate];
    if (self.pendingCustomer.birthdate.length < 1) {
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.year = -21;
        NSDate *defaultDOB = [[NSCalendar currentCalendar] dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
        self.pendingCustomer.birthdate = convertToString(defaultDOB, @"yyyy/MM/dd");
    }
    // 31/05/2016
    self.pendingCustomer.type = [(CustomerItem *)selectedCustomer type];
    self.pendingCustomer.notes= [(CustomerItem*)selectedCustomer notes];
    self.pendingCustomer.isgift = [(CustomerItem*)selectedCustomer isgift];
    self.pendingCustomer.emailaddress2 = [(CustomerItem*)selectedCustomer emailaddress2];
    self.pendingCustomer.giftnotes= [(CustomerItem*)selectedCustomer giftnotes];
    self.pendingCustomer.spousename= [(CustomerItem*)selectedCustomer spousename];
    
    self.pendingCustomer.billshippingto = [(CustomerItem*)selectedCustomer billshippingto];
    self.pendingCustomer.shipfirstname = [(CustomerItem*)selectedCustomer shipfirstname];
    self.pendingCustomer.shiplastname = [(CustomerItem*)selectedCustomer shiplastname];
    self.pendingCustomer.shipcompanyname = [(CustomerItem*)selectedCustomer shipcompanyname];
    self.pendingCustomer.shipaddress1 = [(CustomerItem*)selectedCustomer shipaddress1];
    self.pendingCustomer.shipaddress2= [(CustomerItem*)selectedCustomer shipaddress2];
    self.pendingCustomer.shipcity = [(CustomerItem*)selectedCustomer shipcity];
    self.pendingCustomer.shipstate = [(CustomerItem*)selectedCustomer shipstate];
    self.pendingCustomer.shipZIP= [(CustomerItem*)selectedCustomer shipZIP];
    self.pendingCustomer.paymentTerms = [(CustomerItem *)selectedCustomer paymentTerms];
    
}

#pragma mark - * * * * CustomPopUpDelegate Method * * * *
-(void)popUpValueSelectedAtIndex:(id)selectedCustomer {
    
    [self.popOver dismissPopoverAnimated:YES];
    
    [self refreshInfoForCustomer:selectedCustomer];
    
    [self.textField_customerSearch setText:[[[AppUserData sharedInstance] selectedLocation] enableWholesaleTab] ? [(CustomerItem *)selectedCustomer companyname] : [(CustomerItem*)selectedCustomer name]];
    
    if (self && self.delegate && [self.delegate respondsToSelector:@selector(didSelectedCustomer:)])
        [self.delegate didSelectedCustomer:selectedCustomer];
    
    [self callWebAPIMethod:getCustomers withrequest:[SoapEnvelope getCustomersListFor:[(CustomerItem*)selectedCustomer customerID]]];

}

#pragma mark - * * * * MembershipDelegate Method * * * *

-(void)membershipDelegateMethod:(NSString *)loyaltyCardNumber {
//    [self callWebAPIMethod:getCustomers withrequest:[SoapEnvelope getCustomersListWithLoyaltyCardNumber:loyaltyCardNumber]];
}

// DElegate methods===========
-(void)checkForUserUpdateDetails:(id)delegate ifNotPerformSelector:(SEL)selctor withObj:(id)obj {
    
    if (self.pendingCustomer.isCustomerUpdated) {
        
        UIAlertView * confirmation = [[UIAlertView alloc] initWithTitle:@"Do you want to save the changes for this customer?" message:@"" cancelButtonTitle:nil otherButtonTitles:[NSArray arrayWithObjects:@"YES", @"NO",nil] onCancel:^{
            
        } onDismiss:^(NSInteger buttonIndex) {
            switch (buttonIndex) {
                case 0: {
                    //NO
                    [self refreshPendingCustomer];
                    self.isCancelled = YES;
                    if (selctor)
                        [delegate performSelector:selctor withObject:obj afterDelay:0];
                }
                    break;
                    
                case -1: {
                    //YES
                    if ([self areMendatoryFieldsAvailable])
                        [self savePendingCustomer];
                }
                    break;
                    
                default:
                    break;
            }
        }];
        
        [confirmation show];
        [APPDELEGATE setVisibleAlertView:confirmation];
        [APPDELEGATE setDismissButtonIndex:0];
    }
    else {
        if (selctor)
            [delegate performSelector:selctor withObject:obj afterDelay:0];
    }
}

@end

@implementation UIViewController(CustomerDetailsVC)

- (CustomerDetailsVC*)customerBaseController {
    
    UIViewController *parent = self;
    Class revealClass = [CustomerDetailsVC class];
    
    while ( nil != (parent = [parent parentViewController]) && ![parent isKindOfClass:revealClass] ) {
        
    }
    
    return (id)parent;
}

@end
