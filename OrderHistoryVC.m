//
//  OrderHistoryVC.m
//  Active Club Solutions -iPAD App
//
//  Created by Krishna Kant Kaira on 16/09/14.
//  Copyright (c) 2014 Mobiloitte Technologies. All rights reserved.
//

#import "OrderHistoryVC.h"
#import "OrderHistoryListCell.h"
#import "OrderHistoryDetailCell.h"
#import "SignaturePanelViewController.h"
#import "RefundItemsViewController.h"
#import "NSDictionary+NullChecker.h"
#import "StarPrinterHelper.h"
#import "HtmlToImage.h"
#import "EnterPINViewController.h"
#import "ReceiptViewController.h"
#import "AddNoteViewController.h"
#import "SearchPopUpViewController.h"
#import "CustomerSelectionPopOver.h"
#import "WholesalePaymentViewController.h"

#define dateFormateYear @"yyyy-MM-dd"

@interface OrderHistoryVC () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, SignatureCapturedDelegate, RequestResponseHandlerDelegate, RefundItemsDelegate, HtmlToImageDelegate, ReceiptDelegate, NotesAddedDelegate, SearchPopUpDelegate> {
    
    NSMutableArray *array_recieptOrders;
    NSInteger lastRecieptIndex, selectedButtonTag;
    BOOL isForUpdateOrderStatus;
    
    NSInteger sortedButtonTag;
    BOOL isForCheckedoutOrder;
    BOOL shouldScrollToMakeSelectedIndexVisible;
    
    NSInteger selectedIndexForNotes;
    NSArray *selectedItems;
}

@property (nonatomic, strong) NSString *selectedTitle;
@property (nonatomic, strong) NSString *orderID;
@property (nonatomic, assign) NSInteger selectedOrderIndex;

@property (nonatomic, strong) HtmlToImage *htmlConverter;

@property (strong, nonatomic) IBOutlet UILabel *label_lastName;
@property (strong, nonatomic) IBOutlet UILabel *label_firstName;
@property (nonatomic, strong) IBOutlet UIButton *button_zoomInOut;
@property (nonatomic, strong) IBOutlet UITableView *tableView_orderList;
@property (nonatomic, strong) IBOutlet UITableView *tableView_orderDetail;
@property (nonatomic, strong) IBOutlet UITableView *tableView_orderTotal;
@property (nonatomic, strong) IBOutlet UILabel *label_orderNumber;
@property (nonatomic, strong) IBOutlet UILabel *label_orderCount;
@property (nonatomic, strong) IBOutlet UILabel *label_orderItemCount;
@property (nonatomic, strong) IBOutlet UIView *view_orderListHeader;
@property (nonatomic, strong) IBOutlet UIView *view_orderDetailHeader;
@property (nonatomic, strong) IBOutlet UIView *view_screenHeader;
@property (nonatomic, strong) IBOutlet UIButton *button_modifyOrder;
@property (nonatomic, strong) IBOutlet UIButton *button_checkOut;
@property (nonatomic, strong) IBOutlet UIButton *button_printReceipt;
@property (nonatomic, strong) IBOutlet UIButton *button_refundItems;
@property (nonatomic, strong) IBOutlet UIButton *button_refundOrder;
@property (nonatomic, strong) IBOutlet UIButton *button_signForOrder;
@property (nonatomic, strong) IBOutlet UIButton *button_void;
@property (nonatomic, strong) IBOutlet UIButton *button_swap;
@property (nonatomic, strong) IBOutlet UIButton *button_refundShipping;
@property (weak, nonatomic) IBOutlet UIButton *button_search;
@property (strong, nonatomic) IBOutlet UIView *view_bottomHidden;
@property (strong, nonatomic) IBOutlet UIButton *button_payment;
@property (strong, nonatomic) IBOutlet UIButton *button_signForPickUp;

@property (nonatomic, strong) UIPopoverController *popOver;

@property (nonatomic, strong) NSMutableArray *array_orderList;
@property (nonatomic, strong) NSString *receiptPathForCustomer;
@property (nonatomic, strong) NSString *receiptPathForMerchant;

@property (nonatomic, strong) NSString *receiptForCustomer;
@property (nonatomic, strong) NSString *receiptForMerchant;

-(IBAction)orderTimeMenuCommonAction:(id)sender;
-(IBAction)bottomCommonButtonAction:(id)sender;
-(IBAction)zoomInOutButtonAction:(id)sender;
-(IBAction)commonHeaderButtonActionForSort:(id)sender;
- (IBAction)paymentButtonAction:(id)sender;
- (IBAction)signForPickupButtonAction:(id)sender;

@end

@implementation OrderHistoryVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    sortedButtonTag = 500;
    
    isForUpdateOrderStatus = NO;
    array_recieptOrders = [NSMutableArray array];
    lastRecieptIndex = -1;
    self.selectedOrderIndex = -1;
    selectedIndexForNotes = -1;
    self.orderID = @"";
    
    //09-03-2016
    selectedButtonTag = -1;
    //****************
    [self setDefaultsForController];
    
    isForUpdateOrderStatus = NO;
    
    [self getOrderListForSelectedTitle:@"Today" shouldScrollToMakeRowVisible:NO];
    
    //select the header button
    BOOL enableWholesale = [[[AppUserData sharedInstance] selectedLocation] enableWholesaleTab];
    [(UIButton *)[self.view_screenHeader viewWithTag:207] setHidden:enableWholesale];
    CGFloat x_margin = (self.view_screenHeader.frame.size.width - (100 * (enableWholesale ? 7 : 8) + 30))/2;
    for (int tag = 200; tag <= 208; tag++) {
        UIButton *btn = (UIButton *)[self.view_screenHeader viewWithTag:tag];
        if (btn && [btn isKindOfClass:[UIButton class]]) {
            [btn setFrame:CGRectMake(x_margin, 7, 100, 30)];
            [btn setSelected:NO];
            [btn setExclusiveTouch:YES];
        }
        x_margin += 105;
    }
    [(UIButton *)[self.view_screenHeader viewWithTag:201] setSelected:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[self navigationItem] setHidesBackButton:YES];
    [[self navigationController] setNavigationBarHidden:YES];
    
    //set navigationBar's title
    [[APPDELEGATE baseController] setScreenTitle:@"Order History"];
    [[APPDELEGATE baseController] setSelectedIndex:3];
    
    [self checkForNewOrderCheckedoutInPOS];
    
    //28/06/2016 5) The Order History screen should have the following buttons, Payments which will popup the same screen that this button opens on the Pending Invoices screen, Sign for Pickup/Delivery which will call the SignForInventoryPickup API.
    // 04/07/2016 Order History for wholesale needs to show company name instead of first and last name.  similar to the Pending Invoices screen.
    if ([[[AppUserData sharedInstance] selectedLocation] enableWholesaleTab]) {
        [self.view_bottomHidden setHidden:NO];
        [self.label_firstName setFrame:CGRectMake(120, 0, 220, 43)];
        self.label_firstName.textAlignment = NSTextAlignmentCenter;
        [self.label_firstName setText:@"Company Name"];
        [(UIButton *)[self.view_orderListHeader viewWithTag:501] setFrame:CGRectMake(120, 0, 220, 43)];
        [self.label_lastName setHidden:YES];
        [(UIButton *)[self.view_orderListHeader viewWithTag:502] setHidden:YES];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    if (self.presentedViewController)
        [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - * * * * Other Helper Methods * * * *
-(void)checkForNewOrderCheckedoutInPOS {
    switch ([APPDELEGATE array_newFinalizedOrderID].count) {
        case 0: break;
        case 1:
        case 2: {
            isForCheckedoutOrder = YES;
            [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForOrderDetailsForOrder:[[APPDELEGATE array_newFinalizedOrderID] firstObject] isFromPendingOrder:@"0"] forMethod:getOrderDetails];
        }
            break;
            
        default: {
            isForUpdateOrderStatus = NO;
            [self getOrderListForSelectedTitle:self.selectedTitle shouldScrollToMakeRowVisible:NO];
        }
            break;
    }
}

//New Method to call GetOrders with New Parameters on 3rd March
-(void)getOrderListForRefNumber:(NSString *)refNumber type:(NSString *)type subType:(NSString *)subType startDate:(NSDate *)startDate endDate:(NSDate *)endDate andPickUpLocation:(NSString *)pickUpLocation andEmployeeName:(NSString *)employeeName shouldScrollToMakeRowVisible:(BOOL)shouldScroll {
    
    shouldScrollToMakeSelectedIndexVisible = shouldScroll;
    
    [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForOrderListWithRefNumber:refNumber type:type subType:subType startDate:convertToString(startDate, dateFormateYear) endDate:convertToString(endDate, dateFormateYear) andPickUpLocation:pickUpLocation andEmployeeName:employeeName] forMethod:getOrderList];
    
}
-(void)getOrderListForSelectedTitle:(NSString*)title shouldScrollToMakeRowVisible:(BOOL)shouldScroll {
    
    self.selectedTitle = title;
    shouldScrollToMakeSelectedIndexVisible = shouldScroll;
    
    //remove all orderID's, if any
    //we are already refreshing list on ViewDidLoad, so it won't check in ViewWillAppear
    [[APPDELEGATE array_newFinalizedOrderID] removeAllObjects];
    
    if ([self.selectedTitle isIdenticaleTo:@"Pickup List"])
        [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForNotPickedUpOrderList] forMethod:getOrderList];
    else if ([self.selectedTitle isIdenticaleTo:@"Invoices"])
        [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForInvoiceOrderList] forMethod:getOrderList];
    else
        [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForOrderListWith:self.selectedTitle] forMethod:getOrderList];
}

-(void)manageButtonStatesForOrder : (OrderItem*)order {
    BOOL isOrderCharged = NO;
    BOOL isOrderPending = NO;
    
    if ([order.PaymentStatus isEqualToString:@"Charged"] || [order.PaymentStatus isEqualToString:@"Paid"])
        isOrderCharged = YES;
    if ([order.PaymentStatus isEqualToString:@"Not Charged"] || [order.PaymentStatus isEqualToString:@"Not Paid"] || [order.PaymentStatus isEqualToString:@"Pending"])
        isOrderPending = YES;
    
    [self.button_refundItems setEnabled:isOrderCharged];
    [self.button_refundOrder setEnabled:isOrderCharged];
    [self.button_refundShipping setEnabled:isOrderCharged];
    [self.button_printReceipt setEnabled:(isOrderCharged || [order.PaymentStatus isEqualToString:@"Credited"] || [order.PaymentStatus isEqualToString:@"Refunded"])];
    
    [self.button_modifyOrder setEnabled:isOrderPending];
    [self.button_checkOut setEnabled:isOrderPending];
    
    [self.button_signForOrder setEnabled:([order.ShippingStatus isEqualToString:@"Not Picked Up"] && isOrderCharged)];
    
    //Payment = 'Charged' || Payment = 'Paid/Charged' || Payment = 'Credited'
    [self.button_void setEnabled:(isOrderCharged || [order.PaymentStatus isEqualToString:@"Credited"] || [order.PaymentStatus isEqualToString:@"Refunded"])];
    
    // I want you to enable the Receipt button permanently.  That way they can print receipts as long as it finds a payment record
    [self.button_printReceipt setEnabled:YES];
    
    //1. we are going to need to disable (checkout and modify order) buttons also on the order history screens when the Status = 'Voided', Once an Item is voided the only button that should be enabled is the Receipt button.
    if ([order.Status isIdenticaleTo:@"Voided"]) {
        [self.button_modifyOrder setEnabled:NO];
        [self.button_checkOut setEnabled:NO];
    }
    
    // 09-03-2016 Work....(Enable swap button when payment status will be Paid or Charged.)
    [self.button_swap setEnabled:isOrderCharged];
    [self.button_payment setEnabled:[self.array_orderList count]>0];
    [self.button_signForPickUp setEnabled:([order.ShippingStatus isEqualToString:@"Not Picked Up"] && isOrderCharged)];
}

-(void)setDefaultsForController {
    
    self.array_orderList = [NSMutableArray array];
    
    [self.tableView_orderList setBackgroundColor:[UIColor whiteColor]];
    [self.tableView_orderList setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView_orderList setDataSource:self];
    [self.tableView_orderList setDelegate:self];
    
    [self.tableView_orderDetail setBackgroundColor:[UIColor clearColor]];
    [self.tableView_orderDetail setBackgroundView:nil];
    [self.tableView_orderDetail setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView_orderDetail setDataSource:self];
    [self.tableView_orderDetail setDelegate:self];
    
    [self.tableView_orderTotal setBackgroundColor:[UIColor clearColor]];
    [self.tableView_orderTotal setBackgroundView:nil];
    [self.tableView_orderTotal setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView_orderTotal setDataSource:self];
    [self.tableView_orderTotal setDelegate:self];
    [self.tableView_orderTotal setScrollEnabled:NO];
    
    [self.button_modifyOrder setEnabled:NO];
    [self.button_checkOut setEnabled:NO];
    [self.button_printReceipt setEnabled:NO];
    [self.button_refundItems setEnabled:NO];
    [self.button_refundOrder setEnabled:NO];
    [self.button_refundShipping setEnabled:NO];
    [self.button_signForOrder setEnabled:NO];
    [self.button_void setEnabled:NO];
    [self.button_swap setEnabled:NO];
    [self.button_payment setEnabled:NO];
    [self.button_signForPickUp setEnabled:NO];
    
    [self.button_modifyOrder setExclusiveTouch:YES];
    [self.button_checkOut setExclusiveTouch:YES];
    [self.button_printReceipt setExclusiveTouch:YES];
    [self.button_refundItems setExclusiveTouch:YES];
    [self.button_refundOrder setExclusiveTouch:YES];
    [self.button_refundShipping setExclusiveTouch:YES];
    [self.button_signForOrder setExclusiveTouch:YES];
    [self.button_void setExclusiveTouch:YES];
    [self.button_swap setExclusiveTouch:YES];
    [self.button_signForPickUp setExclusiveTouch:YES];
    [self.button_payment setExclusiveTouch:YES];
    
    [self setOrderListToZoom:NO withAnimation:NO];
}

-(void)printFiles:(NSArray*)pathArray {
    
    BOOL isSuccess = [PrinterAdaptor printReceipt:[UIImage imageWithContentsOfFile:[pathArray firstObject]] shouldOpenDrawer:NO shouldPromptOnFailure:YES];
    
    if (isSuccess && ([pathArray count] > 1))
        isSuccess = [PrinterAdaptor printReceipt:[UIImage imageWithContentsOfFile:[pathArray objectAtIndex:1]] shouldOpenDrawer:NO shouldPromptOnFailure:YES];
    if (!isSuccess) {
        [self retryForPrintReceipt];
    }else {
        [self callForPrintReciept:(lastRecieptIndex+1)];
    }
}

-(NSString*)getReceiptPathFile:(NSString*)filePath {
    
    return ([[NSFileManager defaultManager] fileExistsAtPath:filePath] ? filePath : nil);
}

-(void)didOrderCheckout {
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"POS_Station"] length] < 1) {
        [APPDELEGATE displayAlertWithTitle:@"" andMessage:kErrorMessage_NoPOSStation];
        return;
    }
    
    //make a entry to Local DB
    //the modify order and the check out are already there, you just need to include the orderid (TA#######) and recalculate = 1 for modify order and recalculate = 0 for Check Out
    OrderItem *orderItem = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
    NSString*newOrderID =[[DBHelper instance] makeNewEntryForOrder:orderItem withRecalculateStatus:@"0" forUser:[[AppUserData sharedInstance] loggedInUser] andIsForSWAP:NO isFromPendingOrder:NO];
    
    //move to check out screen
    TBL_UserOrder *userOrder = [[[DBHelper instance] getOrderWithOrderId:newOrderID forUser:[[AppUserData sharedInstance] loggedInUser]] firstObject];
    [self callWebAPIWithRequest:[SoapEnvelope calculateOrderForCheckOut:userOrder withGiftCardData:nil withGiftCardAmount:@""] forMethod:calculateForCheckOut];
}

#pragma mark - * * * * Button Action * * * *

-(IBAction)orderTimeMenuCommonAction:(UIButton *)sender {
    
    for (int tag = 200; tag <= 208; tag++) {
        UIButton *btn = (UIButton *)[self.view_screenHeader viewWithTag:tag];
        if (btn && [btn isKindOfClass:[UIButton class]])
            [btn setSelected:NO];
    }
    
    [(UIButton *)sender setSelected:YES];
    
    if (sender.tag == 200) {
        // Search Button
        SearchPopUpViewController *vC = [[SearchPopUpViewController alloc] initWithNibName:@"SearchPopUpViewController" bundle:nil];
        vC.delegate = self;
        [vC setModalPresentationStyle:UIModalPresentationFormSheet];
        [vC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        vC.preferredContentSize = CGSizeMake(675.0, 400.0);
        [self presentViewController:vC animated:NO completion:nil];
    }
    else {
        self.orderID = @"";
        isForUpdateOrderStatus = NO;
        [self getOrderListForSelectedTitle:[[(UIButton*)sender titleLabel] text] shouldScrollToMakeRowVisible:YES];
    }
}

-(IBAction)bottomCommonButtonAction:(id)sender {
    
    //means no order is selected yet
    if (self.selectedOrderIndex < 0) return;
    
    [self setOrderListToZoom:NO withAnimation:NO];
    
    switch ([sender tag]%300) {
        case 0: {
            //Modify Order
            
            //make a entry to Local DB
            //the modify order and the check out are already there, you just need to include the orderid (TA#######) and recalculate = 1 for modify order and recalculate = 0 for Check Out
            OrderItem *orderItem = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
            NSString*newOrderID = [[DBHelper instance] makeNewEntryForOrder:orderItem withRecalculateStatus:@"0" forUser:[[AppUserData sharedInstance] loggedInUser] andIsForSWAP:NO isFromPendingOrder:NO];
            
            //move to New Order screen
            [[APPDELEGATE baseController] modifyOrderWithOrder:newOrderID];
        }
            break;
            
        case 1: {
            //Check Out
            
            if ([[[AppUserData sharedInstance] selectedLocation] enablecashdrawerfunctions]) {
                NSString *assignedCashDrawer = [[NSUserDefaults standardUserDefaults] valueForKey:kCashDrawerId];
                BOOL canCompleteOrder = (assignedCashDrawer && [assignedCashDrawer length]>0);
                if (canCompleteOrder) {
                    [self didOrderCheckout];
                }
                else [APPDELEGATE displayAlertWithTitle:@"" andMessage:kErrorMessage_NoDrawerAssigned];
            }else {
                [self didOrderCheckout];
            }
        }
            break;
            
        case 2: {
            //Reciept
            
            OrderItem *orderItem = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
            
            ReceiptViewController *receiptViewController=[[ReceiptViewController alloc] initWithNibName:@"ReceiptViewController" bundle:nil];
            receiptViewController.delegate = self;
            receiptViewController.orderID = orderItem.orderID;
            receiptViewController.paymentID = nil;
            receiptViewController.str_customerEmail = orderItem.EmailAddress;
            receiptViewController.str_customerFirstName = orderItem.CustomerFirstName;
            receiptViewController.str_customerLastName = orderItem.CustomerLastName;
            
            [receiptViewController setModalPresentationStyle:UIModalPresentationFormSheet];
            [receiptViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            receiptViewController.preferredContentSize = CGSizeMake(750.0, 500.0);
            receiptViewController.isNotDigitalSignShow = YES;
            [self presentViewController:receiptViewController animated:NO completion:nil];
        }
            break;
            
        case 3: {
            //Refund Items
            selectedButtonTag = [sender tag];
            if (([[[AppUserData sharedInstance] selectedLocation] managerapproverefund] == NO) || [[[[AppUserData sharedInstance] loggedInUser] userType] isIdenticaleTo:@"manager"]) {
                
                OrderItem *orderItem = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForOrderForRefundForOrder:orderItem.orderID] forMethod:getOrderForRefund];
            }
            else {
                
                [[EnterPINViewController sharedInstance] getManegerialAccess:^(NSDictionary *dict, NSString *error) {
                    if (error){
                        [APPDELEGATE displayAlertWithTitle:@"" andMessage:error];
                    }
                    else if (dict) {
                        
                        OrderItem *orderItem = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                        [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForOrderForRefundForOrder:orderItem.orderID] forMethod:getOrderForRefund];
                    }
                }];
            }
        }
            break;
            
        case 4: {
            //Refund Order
            
            UIAlertView *refundAlert=[[UIAlertView alloc] initWithTitle:@"Are you sure you want to refund this order?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
            refundAlert.tag=1001;
            [refundAlert show];
            [APPDELEGATE setVisibleAlertView:refundAlert];
            [APPDELEGATE setDismissButtonIndex:1];
        }
            break;
            
        case 5: {
            //Sign For Order
            [self signForOrder];
        }
            break;
            
        case 6: {
            //Refund Shipping
            
            UIAlertView *refundAlert=[[UIAlertView alloc] initWithTitle:@"Are you sure you want to refund shipping?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
            refundAlert.tag=1002;
            [refundAlert show];
            [APPDELEGATE setVisibleAlertView:refundAlert];
            [APPDELEGATE setDismissButtonIndex:1];
        }
            break;
            
        case 7: {
            //Void Order
            
            UIAlertView *refundAlert=[[UIAlertView alloc] initWithTitle:@"Are you sure that you want to Void this order?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES", @"NO", nil];
            refundAlert.tag=1003;
            [refundAlert show];
            [APPDELEGATE setVisibleAlertView:refundAlert];
            [APPDELEGATE setDismissButtonIndex:1];
        }
            break;
            
        case 8: {
            //Swap
            selectedButtonTag = [sender tag];
            OrderItem *orderItem = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
            
            //Update : When they hit the swap button I need you to call GetOrderForRefund and use the info in that response when populating the New Order screen with the items available for refund. on 8 March, 2016
            [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForOrderForRefundForOrder:orderItem.orderID] forMethod:getOrderForRefund];
        }
            break;
            
        default:
            break;
    }
}

-(IBAction)zoomInOutButtonAction:(UIButton*)button {
    
    [self setOrderListToZoom:!button.selected withAnimation:YES];
}

-(IBAction)commonHeaderButtonActionForSort:(UIButton*)sender {
    
    [self sortDataForTag:[sender tag] shouldInvertSorting:YES];
}


-(void)sortDataForTag:(NSInteger)buttonTag shouldInvertSorting:(BOOL)shouldInvert {
    
    sortedButtonTag = buttonTag;
    
    for (int tag = 500; tag < 508; tag++) {
        UIButton *headerButton = (UIButton *)[self.view_orderListHeader viewWithTag:tag];
        if (headerButton && [headerButton isKindOfClass:[UIButton class]]) {
            [headerButton setImage:nil forState:UIControlStateNormal];
            [headerButton setImage:nil forState:UIControlStateSelected];
        }
    }
    
    OrderItem *selectedOrder;
    if (self.selectedOrderIndex>=0)
        selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
    
    NSString *sortKey = nil;
    UIButton *senderButton = (UIButton*)[self.view_orderListHeader viewWithTag:buttonTag];
    if (shouldInvert)
        senderButton.selected = !senderButton.selected;
    
    [senderButton setImage:[UIImage imageNamed:@"sortIcon_unsel.png"] forState:UIControlStateNormal];
    [senderButton setImage:[UIImage imageNamed:@"sortIcon_sel.png"] forState:UIControlStateSelected];
    
    
    switch (buttonTag%500) {
            
        case 0: sortKey = @"orderID";           break;    //Ref No. == OrderID
        case 1: sortKey = [[[AppUserData sharedInstance]selectedLocation]enableWholesaleTab] ? @"companyName" : @"CustomerFirstName"; break;    //CustomerFirstName
        case 2: sortKey = @"CustomerLastName";  break;    //CustomerFirstName
        case 3: sortKey = @"Subtype";              break;    //Type is replaced by Subtype
        case 4: sortKey = @"PaymentStatus";     break;    //PaymentStatus
        case 5: sortKey = @"Sort_PaymentDate";       break;    //Payment Date
        case 6: sortKey = @"ShippingStatus";    break;    //ShippingStatus
        case 7: sortKey = @"number_SaleTotal";  break;    //number_SaleTotal
            
        default: break;
    }
    
    if (sortKey != nil)
        [self.array_orderList sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:sortKey ascending:senderButton.selected]]];
    
    if (selectedOrder != nil) {
        self.selectedOrderIndex = [self.array_orderList indexOfObject:selectedOrder];
        selectedIndexForNotes = -1;
    }
    
    [self.tableView_orderList reloadData];
    
}

-(void)setOrderListToZoom:(BOOL)isZoom withAnimation:(BOOL)isAnimate{
    
    self.button_zoomInOut.selected = isZoom;
    
    CGRect newFrame = CGRectMake(12.0, 82.0, 1000.0, (isZoom ? 529.0 : 163.0));
    
    [UIView animateWithDuration:0.2 animations:^{
        [self.tableView_orderList setFrame:newFrame];
    } completion:^(BOOL finished) {
        if (finished && isAnimate && !isZoom && (self.selectedOrderIndex >= 0))
            [self.tableView_orderList scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedOrderIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }];
}

-(void)notesButtonAction : (id)sender {
    
    //Notes
    selectedIndexForNotes = [sender tag];
    if ((selectedIndexForNotes>=0) || (selectedIndexForNotes < self.array_orderList.count)) {
        OrderItem *orderItem = [self.array_orderList objectAtIndex:selectedIndexForNotes];
        
        AddNoteViewController *notesVC = [[AddNoteViewController alloc] initWithNibName:@"AddNoteViewController" bundle:nil];
        notesVC.delegate = self;
        notesVC.str_notes = orderItem.Notes;
        [notesVC setModalPresentationStyle:UIModalPresentationFormSheet];
        [notesVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        notesVC.preferredContentSize = CGSizeMake(670.0, 320.0);
        [self presentViewController:notesVC animated:NO completion:nil];
    }
}


- (IBAction)paymentButtonAction:(id)sender {
    
    OrderItem *selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
    WholesalePaymentViewController *paymentVC = [[WholesalePaymentViewController alloc] initWithNibName:@"WholesalePaymentViewController" bundle:nil];
    paymentVC.preferredContentSize = CGSizeMake(700.0, 500.0);
    paymentVC.orderId = selectedOrder.orderID;
    paymentVC.paymentId = nil;
    [paymentVC setModalPresentationStyle:UIModalPresentationFormSheet];
    [paymentVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:paymentVC animated:NO completion:nil];
}

- (IBAction)signForPickupButtonAction:(id)sender {
    [self signForOrder];
}


-(void)signForOrder {
    OrderItem *selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
    
    SignaturePanelViewController *signVC = [[SignaturePanelViewController alloc] initWithNibName:@"SignaturePanelViewController" bundle:nil];
    [signVC setDelegate:self];
    signVC.isFromCheckout = NO;
    [signVC setName:selectedOrder.CustomerName];
    [signVC setModalPresentationStyle:UIModalPresentationFormSheet];
    [signVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    signVC.preferredContentSize = CGSizeMake(850.0, 500.0);
    [self presentViewController:signVC animated:NO completion:nil];
}
#pragma mark - * * * * SearchPopUpDelegate * * * *
-(void)passingSearchPopUpDataToControllerWithRefNo:(NSString *)refNo type:(NSString *)strType subType :(NSString *)strSubType startDate:(NSDate *)startDate endDate:(NSDate *)endDate andPickUpLocation:(NSString *)strPickUpLocation andEmployeeName:(NSString *)employeeName{
    
    if(!refNo.length)
        refNo = @"%";
    
    if(refNo.length || strType.length || strSubType.length || startDate || endDate || strPickUpLocation.length || employeeName.length) {
        [self getOrderListForRefNumber:refNo type:strType subType:strSubType startDate:startDate endDate:endDate andPickUpLocation:strPickUpLocation andEmployeeName:employeeName shouldScrollToMakeRowVisible:YES];
    }
}

#pragma mark - * * * * NotesAddedDelegate * * * *
-(void)notesAdded:(NSString*)notes {
    
    NSString *newNotes= [notes removeWhiteSpacesFromString];
    
    if ((selectedIndexForNotes>=0) || (selectedIndexForNotes < self.array_orderList.count)) {
        OrderItem *orderItem = [self.array_orderList objectAtIndex:selectedIndexForNotes];
        orderItem.Notes = newNotes;
        selectedIndexForNotes = -1;
        
        [self callWebAPIWithRequest:[SoapEnvelope addNotes:notes ToOrder:orderItem.orderID forCustomer:orderItem.CustomerID] forMethod:addNotesToOrder];
    }
}


#pragma mark - * * * * UIAlertViewDelegate Methods * * * *

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        
        switch (alertView.tag) {
                
            case 1001: {
                
                //Refund Tip
                OrderItem *selectedOrderItem = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                if ([selectedOrderItem.tipTotal doubleValue] <= 0.00) {
                    //Refund order if selected order don't have tip
                    if (([[[AppUserData sharedInstance] selectedLocation] managerapproverefund] == NO) || [[[[AppUserData sharedInstance] loggedInUser] userType] isIdenticaleTo:@"manager"]) {
                        
                        OrderItem *selectedOrder;
                        if (self.selectedOrderIndex>=0)
                            selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                        
                        if ([[[AppUserData sharedInstance] selectedLocation] allowrefundincashoption] && [selectedOrder.PaymentStatus isEqualToString:@"Charged"]) {
                            
                            UIAlertView *refundAlert=[[UIAlertView alloc] initWithTitle:@"Would you prefer to refund this amount in cash?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
                            refundAlert.tag=1005;
                            [refundAlert show];
                            [APPDELEGATE setVisibleAlertView:refundAlert];
                            [APPDELEGATE setDismissButtonIndex:1];
                        }else {
                            [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundOrderForOrder:selectedOrder.orderID andTipReturnStatus:@"0" withPaymentStatus:NO] forMethod:refundOrder];
                        }
                    }
                    else {
                        
                        [[EnterPINViewController sharedInstance] getManegerialAccess:^(NSDictionary *dict, NSString *error) {
                            if (error){
                                [APPDELEGATE displayAlertWithTitle:@"" andMessage:error];
                            }
                            else if (dict) {
                                
                                OrderItem *selectedOrder;
                                if (self.selectedOrderIndex>=0)
                                    selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                                
                                if ([[[AppUserData sharedInstance] selectedLocation] allowrefundincashoption] && [selectedOrder.PaymentStatus isEqualToString:@"Charged"]) {
                                    UIAlertView *refundAlert=[[UIAlertView alloc] initWithTitle:@"Would you prefer to refund this amount in cash?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
                                    refundAlert.tag=1005;
                                    [refundAlert show];
                                    [APPDELEGATE setVisibleAlertView:refundAlert];
                                    [APPDELEGATE setDismissButtonIndex:1];
                                }else {
                                    [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundOrderForOrder:selectedOrder.orderID andTipReturnStatus:@"0" withPaymentStatus:NO] forMethod:refundOrder];
                                }
                            }
                        }];
                    }
                }else {
                    //Refund order if selected order have tip
                    UIAlertView *refundAlert=[[UIAlertView alloc] initWithTitle:@"Would you like to refund the tip for this order?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
                    refundAlert.tag=1004;
                    [refundAlert show];
                    [APPDELEGATE setVisibleAlertView:refundAlert];
                    [APPDELEGATE setDismissButtonIndex:1];
                }
                
            }
                break;
                
            case 1002: {
                //Refund Shipping
                
                if (([[[AppUserData sharedInstance] selectedLocation] managerapproverefund] == NO) || [[[[AppUserData sharedInstance] loggedInUser] userType] isIdenticaleTo:@"manager"]) {
                    
                    OrderItem *selectedOrder;
                    if (self.selectedOrderIndex>=0)
                        selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                    
                    if ([[[AppUserData sharedInstance] selectedLocation] allowrefundincashoption] && [selectedOrder.PaymentStatus isEqualToString:@"Charged"]) {
                        UIAlertView *refundAlert=[[UIAlertView alloc] initWithTitle:@"Would you prefer to refund this amount in cash?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
                        refundAlert.tag=1006;
                        [refundAlert show];
                        [APPDELEGATE setVisibleAlertView:refundAlert];
                        [APPDELEGATE setDismissButtonIndex:1];
                    }else {
                        [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundShippingForOrder:selectedOrder.orderID withPaymentStatus:NO] forMethod:refundShipping];
                    }
                }
                else {
                    
                    [[EnterPINViewController sharedInstance] getManegerialAccess:^(NSDictionary *dict, NSString *error) {
                        if (error){
                            [APPDELEGATE displayAlertWithTitle:@"" andMessage:error];
                        }
                        else if (dict) {
                            
                            OrderItem *selectedOrder;
                            if (self.selectedOrderIndex>=0)
                                selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                            
                            if ([[[AppUserData sharedInstance] selectedLocation] allowrefundincashoption] && [selectedOrder.PaymentStatus isEqualToString:@"Charged"]) {
                                UIAlertView *refundAlert=[[UIAlertView alloc] initWithTitle:@"Would you prefer to refund this amount in cash?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
                                refundAlert.tag=1006;
                                [refundAlert show];
                                [APPDELEGATE setVisibleAlertView:refundAlert];
                                [APPDELEGATE setDismissButtonIndex:1];
                            }else {
                                [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundShippingForOrder:selectedOrder.orderID withPaymentStatus:NO] forMethod:refundShipping];
                            }
                        }
                    }];
                }
            }
                break;
                
            case 1003: {
                //Void Order
                
                if (([[[AppUserData sharedInstance] selectedLocation] managerapprovevoid] == NO) || [[[[AppUserData sharedInstance] loggedInUser] userType] isIdenticaleTo:@"manager"]) {
                    
                    TBL_UserOrder *selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                    [self callWebAPIWithRequest:[SoapEnvelope getVoidOrederReqfor:selectedOrder.orderID] forMethod:voidOrder];
                }
                else {
                    
                    [[EnterPINViewController sharedInstance] getManegerialAccess:^(NSDictionary *dict, NSString *error) {
                        if (error){
                            [APPDELEGATE displayAlertWithTitle:@"" andMessage:error];
                        }
                        else if (dict) {
                            
                            TBL_UserOrder *selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                            [self callWebAPIWithRequest:[SoapEnvelope getVoidOrederReqfor:selectedOrder.orderID] forMethod:voidOrder];
                        }
                    }];
                }
            }
                break;
            case 1004:{
                //Refund Order
                if (([[[AppUserData sharedInstance] selectedLocation] managerapproverefund] == NO) || [[[[AppUserData sharedInstance] loggedInUser] userType] isIdenticaleTo:@"manager"]) {
                    
                    OrderItem *selectedOrder;
                    if (self.selectedOrderIndex>=0)
                        selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                    
                    if ([[[AppUserData sharedInstance] selectedLocation] allowrefundincashoption] && [selectedOrder.PaymentStatus isEqualToString:@"Charged"]) {
                        UIAlertView *refundAlert=[[UIAlertView alloc] initWithTitle:@"Would you prefer to refund this amount in cash?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
                        refundAlert.tag=1005;
                        [refundAlert show];
                        [APPDELEGATE setVisibleAlertView:refundAlert];
                        [APPDELEGATE setDismissButtonIndex:1];
                    }else {
                        [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundOrderForOrder:selectedOrder.orderID andTipReturnStatus:@"1" withPaymentStatus:NO] forMethod:refundOrder];
                    }
                }
                else {
                    
                    [[EnterPINViewController sharedInstance] getManegerialAccess:^(NSDictionary *dict, NSString *error) {
                        if (error){
                            [APPDELEGATE displayAlertWithTitle:@"" andMessage:error];
                        }
                        else if (dict) {
                            
                            OrderItem *selectedOrder;
                            if (self.selectedOrderIndex>=0)
                                selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                            
                            if ([[[AppUserData sharedInstance] selectedLocation] allowrefundincashoption] && [selectedOrder.PaymentStatus isEqualToString:@"Charged"]) {
                                UIAlertView *refundAlert=[[UIAlertView alloc] initWithTitle:@"Would you prefer to refund this amount in cash?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
                                refundAlert.tag=1005;
                                [refundAlert show];
                                [APPDELEGATE setVisibleAlertView:refundAlert];
                                [APPDELEGATE setDismissButtonIndex:1];
                            }else {
                                [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundOrderForOrder:selectedOrder.orderID andTipReturnStatus:@"1" withPaymentStatus:NO] forMethod:refundOrder];
                            }
                        }
                    }];
                }
                
            }
                break;
                
            case 1005:{
                //Refund Order
                if (([[[AppUserData sharedInstance] selectedLocation] managerapproverefund] == NO) || [[[[AppUserData sharedInstance] loggedInUser] userType] isIdenticaleTo:@"manager"]) {
                    
                    OrderItem *selectedOrder;
                    if (self.selectedOrderIndex>=0)
                        selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                    
                    [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundOrderForOrder:selectedOrder.orderID andTipReturnStatus:@"1" withPaymentStatus:YES] forMethod:refundOrder];
                }
                else {
                    
                    [[EnterPINViewController sharedInstance] getManegerialAccess:^(NSDictionary *dict, NSString *error) {
                        if (error){
                            [APPDELEGATE displayAlertWithTitle:@"" andMessage:error];
                        }
                        else if (dict) {
                            
                            OrderItem *selectedOrder;
                            if (self.selectedOrderIndex>=0)
                                selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                            
                            [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundOrderForOrder:selectedOrder.orderID andTipReturnStatus:@"1" withPaymentStatus:YES] forMethod:refundOrder];

                        }
                    }];
                }
                
            }
                break;
                
            case 1006: {
                //Refund Shipping
                
                if (([[[AppUserData sharedInstance] selectedLocation] managerapproverefund] == NO) || [[[[AppUserData sharedInstance] loggedInUser] userType] isIdenticaleTo:@"manager"]) {
                    
                    OrderItem *selectedOrder;
                    if (self.selectedOrderIndex>=0)
                        selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                    
                    [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundShippingForOrder:selectedOrder.orderID withPaymentStatus:YES] forMethod:refundShipping];

                }
                else {
                    
                    [[EnterPINViewController sharedInstance] getManegerialAccess:^(NSDictionary *dict, NSString *error) {
                        if (error){
                            [APPDELEGATE displayAlertWithTitle:@"" andMessage:error];
                        }
                        else if (dict) {
                            
                            OrderItem *selectedOrder;
                            if (self.selectedOrderIndex>=0)
                                selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                            [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundShippingForOrder:selectedOrder.orderID withPaymentStatus:YES] forMethod:refundShipping];

                        }
                    }];
                }
            }
                break;
                
            case 1007: {
                //Refund Items
                
                if (([[[AppUserData sharedInstance] selectedLocation] managerapproverefund] == NO) || [[[[AppUserData sharedInstance] loggedInUser] userType] isIdenticaleTo:@"manager"]) {
                    
                    OrderItem *selectedOrder;
                    if (self.selectedOrderIndex>=0)
                        selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                    
                    [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundItemsForOrder:selectedOrder.orderID withItems:[NSMutableArray arrayWithArray:selectedItems] withPaymentStatus:YES] forMethod:refundItems];
                    
                }
                else {
                    
                    [[EnterPINViewController sharedInstance] getManegerialAccess:^(NSDictionary *dict, NSString *error) {
                        if (error){
                            [APPDELEGATE displayAlertWithTitle:@"" andMessage:error];
                        }
                        else if (dict) {
                            
                            OrderItem *selectedOrder;
                            if (self.selectedOrderIndex>=0)
                                selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                            [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundItemsForOrder:selectedOrder.orderID withItems:[NSMutableArray arrayWithArray:selectedItems] withPaymentStatus:YES] forMethod:refundItems];
                            
                        }
                    }];
                }
            }
                break;
            default:
                break;
        }
    }else if (buttonIndex == 1){
        
        switch (alertView.tag) {
            case 1004:{
                //Refund Order
                if (([[[AppUserData sharedInstance] selectedLocation] managerapproverefund] == NO) || [[[[AppUserData sharedInstance] loggedInUser] userType] isIdenticaleTo:@"manager"]) {
                    
                    OrderItem *selectedOrder;
                    if (self.selectedOrderIndex>=0)
                        selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                    
                    if ([[[AppUserData sharedInstance] selectedLocation] allowrefundincashoption] && [selectedOrder.PaymentStatus isEqualToString:@"Charged"]) {
                        UIAlertView *refundAlert=[[UIAlertView alloc] initWithTitle:@"Would you prefer to refund this amount in cash?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
                        refundAlert.tag=1005;
                        [refundAlert show];
                        [APPDELEGATE setVisibleAlertView:refundAlert];
                        [APPDELEGATE setDismissButtonIndex:1];
                    }else {
                        [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundOrderForOrder:selectedOrder.orderID andTipReturnStatus:@"1" withPaymentStatus:NO] forMethod:refundOrder];
                    }
                }
                else {
                    
                    [[EnterPINViewController sharedInstance] getManegerialAccess:^(NSDictionary *dict, NSString *error) {
                        if (error){
                            [APPDELEGATE displayAlertWithTitle:@"" andMessage:error];
                        }
                        else if (dict) {
                            
                            OrderItem *selectedOrder;
                            if (self.selectedOrderIndex>=0)
                                selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                            
                            if ([[[AppUserData sharedInstance] selectedLocation] allowrefundincashoption] && [selectedOrder.PaymentStatus isEqualToString:@"Charged"]) {
                                UIAlertView *refundAlert=[[UIAlertView alloc] initWithTitle:@"Would you prefer to refund this amount in cash?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
                                refundAlert.tag=1005;
                                [refundAlert show];
                                [APPDELEGATE setVisibleAlertView:refundAlert];
                                [APPDELEGATE setDismissButtonIndex:1];
                            }else {
                                [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundOrderForOrder:selectedOrder.orderID andTipReturnStatus:@"1" withPaymentStatus:NO] forMethod:refundOrder];
                            }
                        }
                    }];
                }
                
            }
                break;
                
            case 1005: {
                //Refund Order
                
                if (([[[AppUserData sharedInstance] selectedLocation] managerapproverefund] == NO) || [[[[AppUserData sharedInstance] loggedInUser] userType] isIdenticaleTo:@"manager"]) {
                    
                    OrderItem *selectedOrder;
                    if (self.selectedOrderIndex>=0)
                        selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                    
                    [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundOrderForOrder:selectedOrder.orderID andTipReturnStatus:@"1" withPaymentStatus:NO] forMethod:refundOrder];
                    
                }
                else {
                    
                    [[EnterPINViewController sharedInstance] getManegerialAccess:^(NSDictionary *dict, NSString *error) {
                        if (error){
                            [APPDELEGATE displayAlertWithTitle:@"" andMessage:error];
                        }
                        else if (dict) {
                            
                            OrderItem *selectedOrder;
                            if (self.selectedOrderIndex>=0)
                                selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                            [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundOrderForOrder:selectedOrder.orderID andTipReturnStatus:@"1" withPaymentStatus:NO] forMethod:refundOrder];
                            
                        }
                    }];
                }
            }
                break;
                
            case 1006: {
                //Refund Shipping
                
                if (([[[AppUserData sharedInstance] selectedLocation] managerapproverefund] == NO) || [[[[AppUserData sharedInstance] loggedInUser] userType] isIdenticaleTo:@"manager"]) {
                    
                    OrderItem *selectedOrder;
                    if (self.selectedOrderIndex>=0)
                        selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                    
                    [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundShippingForOrder:selectedOrder.orderID withPaymentStatus:NO] forMethod:refundShipping];
                    
                }
                else {
                    
                    [[EnterPINViewController sharedInstance] getManegerialAccess:^(NSDictionary *dict, NSString *error) {
                        if (error){
                            [APPDELEGATE displayAlertWithTitle:@"" andMessage:error];
                        }
                        else if (dict) {
                            
                            OrderItem *selectedOrder;
                            if (self.selectedOrderIndex>=0)
                                selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                            [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundShippingForOrder:selectedOrder.orderID withPaymentStatus:NO] forMethod:refundShipping];
                            
                        }
                    }];
                }
            }
                break;
                
            case 1007: {
                //Refund Items
                
                if (([[[AppUserData sharedInstance] selectedLocation] managerapproverefund] == NO) || [[[[AppUserData sharedInstance] loggedInUser] userType] isIdenticaleTo:@"manager"]) {
                    
                    OrderItem *selectedOrder;
                    if (self.selectedOrderIndex>=0)
                        selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                    
                    [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundItemsForOrder:selectedOrder.orderID withItems:[NSMutableArray arrayWithArray:selectedItems] withPaymentStatus:NO] forMethod:refundItems];
                    
                }
                else {
                    
                    [[EnterPINViewController sharedInstance] getManegerialAccess:^(NSDictionary *dict, NSString *error) {
                        if (error){
                            [APPDELEGATE displayAlertWithTitle:@"" andMessage:error];
                        }
                        else if (dict) {
                            
                            OrderItem *selectedOrder;
                            if (self.selectedOrderIndex>=0)
                                selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                            [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundItemsForOrder:selectedOrder.orderID withItems:[NSMutableArray arrayWithArray:selectedItems] withPaymentStatus:NO] forMethod:refundItems];
                        }
                    }];
                }
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - * * * * ReceiptDelegate Method* * * *
-(void)orderCompletionDidDismissedWithData:(NSArray*)paymentData {
    
    if (paymentData.count == 0) return;
    
    NSMutableArray *array_emailOrders = [NSMutableArray array];
    [array_recieptOrders removeAllObjects];
    
    for (PaymentItem *payment in paymentData) {
        
        switch (payment.selectedRecieptType) {
                
            case ReceiptType_Email:
                [array_emailOrders addObject:payment];
                break;
                
            case ReceiptType_Print:
                [array_recieptOrders addObject:payment];
                break;
                
            case ReceiptType_Both: {
                [array_emailOrders addObject:payment];
                [array_recieptOrders addObject:payment];
            }
                break;
                
            case ReceiptType_None:
            default:
                break;
        }
    }
    
    if ([array_emailOrders count] > 0) {
        [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForEmailOrderForData:array_emailOrders] forMethod:emailSalesCreditReceipt];
    }
    else {
        [self callForPrintReciept:0];
    }
}

-(void)callForPrintReciept:(NSInteger)index {
    
    if ((array_recieptOrders.count > 0) && (index <= (array_recieptOrders.count-1))) {
        lastRecieptIndex = index;
        
        PaymentItem *payment = [array_recieptOrders objectAtIndex:lastRecieptIndex];
        [self callWebAPIWithRequest:[SoapEnvelope getReceiptForOrder:payment.OrderId paymentID:payment.PaymentId cash:payment.CashReceived andChange:payment.ChangeGiven forSoapAction:@"GetHTMLReceiptForPrinting"] forMethod:getHTMLReceiptForPrinting];
    }
    else {
        lastRecieptIndex = -1;
        
        [MBProgressHUD hideAllHUDsForView:[APPDELEGATE navigationController].view animated:YES];
    }
}

#pragma mark -  * * * * UITableViewDataSource Methods * * * *
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numberOfRows = 0;
    
    if([tableView isEqual:self.tableView_orderList]) {
        numberOfRows = [self.array_orderList count];
    }
    else if([tableView isEqual:self.tableView_orderDetail]) {
        if (self.selectedOrderIndex >= 0) {
            OrderItem *orderItem = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
            numberOfRows = orderItem.array_selectedInventory.count;
        }
    }
    else if([tableView isEqual:self.tableView_orderTotal]) {
        if ([[[AppUserData sharedInstance] selectedLocation] enableWholesaleTab]) {
            numberOfRows = 4;
        }else {
            numberOfRows = 6;
        }
    }
    
    return numberOfRows;
}

-(void)updateDetailForOrder:(OrderItem*)orderItem {
    
    if (orderItem) {
        if (orderItem.Status.length > 0)
            [self.label_orderNumber setText:[NSString stringWithFormat:@"Details For: %@ - Type: %@ - Status : %@",orderItem.orderID,orderItem.Type,orderItem.Status]];
        else
            [self.label_orderNumber setText:[NSString stringWithFormat:@"Details For: %@ - Type: %@",orderItem.orderID,orderItem.Type]];
    }
    else {
        [self.label_orderNumber setText:@"Details For:"];
    }
    
    [self manageButtonStatesForOrder:orderItem];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([tableView isEqual:self.tableView_orderList]) {
        
        static NSString *OrderListCellIdentifier = @"OrderHistoryListCellID";
        OrderHistoryListCell *orderListcell = [tableView dequeueReusableCellWithIdentifier:OrderListCellIdentifier];
        if(orderListcell==nil) {
            
            NSArray *topLevelObjectsProducts = [[NSBundle mainBundle] loadNibNamed:@"OrderHistoryListCell" owner:self options:nil];
            orderListcell = (OrderHistoryListCell *)[topLevelObjectsProducts objectAtIndex:0];
            [orderListcell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [orderListcell.contentView setBackgroundColor:[UIColor clearColor]];
            [orderListcell setBackgroundColor:[UIColor clearColor]];
        }
        [orderListcell.button_notes addTarget:self action:@selector(notesButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [orderListcell.button_notes setTag:indexPath.row];
        [orderListcell.button_notes setSelected:(indexPath.row == self.selectedOrderIndex)];
        
        UIColor *bgColo = ((indexPath.row%2)==0) ? kAppLightGrayColor : kAppGrayColor;
        [orderListcell setBackgroundColor:bgColo];
        [orderListcell.contentView setBackgroundColor:bgColo];
        
        UIColor *textColor = [UIColor blackColor];
        
        if (indexPath.row == self.selectedOrderIndex) {
            textColor = [UIColor colorWithRed:7.0/255.0 green:68.0/255.0 blue:208.0/255.0 alpha:1.0];
            [orderListcell setBackgroundColor:textColor];
            [orderListcell.contentView setBackgroundColor:textColor];
            
            textColor = [UIColor whiteColor];
        }
        
        orderListcell.label_refNu.textColor = textColor;
        orderListcell.label_customerFirstName.textColor = textColor;
        orderListcell.label_customerLastName.textColor = textColor;
        orderListcell.label_product.textColor = textColor;
        orderListcell.label_payment.textColor = textColor;
        orderListcell.label_date.textColor = textColor;
        orderListcell.label_shipStatus.textColor = textColor;
        orderListcell.label_amount.textColor = textColor;
        
        OrderItem *orderItem = [self.array_orderList objectAtIndex:indexPath.row];
        orderListcell.label_refNu.text = orderItem.orderID;
        if ([[[AppUserData sharedInstance] selectedLocation] enableWholesaleTab]) {
            orderListcell.label_customerFirstName.text = orderItem.companyName;
            [orderListcell.label_customerFirstName setFrame:CGRectMake(120, 0, 220, 40)];
        }else {
            orderListcell.label_customerFirstName.text = orderItem.CustomerFirstName;
            orderListcell.label_customerLastName.text = orderItem.CustomerLastName;
        }
        orderListcell.label_product.text = orderItem.Subtype;
        orderListcell.label_payment.text = orderItem.PaymentStatus;
        //10-03-2016 Changes(I need you to display the <PaymentDate> value returned with GetOrders API on the Order History Screens instead of the <DateCreated> value in the Date column in the list.)
        orderListcell.label_date.text = orderItem.PaymentDate;
        //*****************************************************************************************
        orderListcell.label_shipStatus.text = orderItem.ShippingStatus;
        orderListcell.label_amount.text = [NSString stringWithFormat:@"$%0.2f",[orderItem.SaleTotal doubleValue]];
        
        return orderListcell;
    }
    else if([tableView isEqual:self.tableView_orderDetail]) {
        
        static NSString *OrderListCellIdentifier = @"OrderHistoryDetailCellID";
        OrderHistoryDetailCell *orderDetailcell = [tableView dequeueReusableCellWithIdentifier:OrderListCellIdentifier];
        if(orderDetailcell==nil) {
            
            NSArray *topLevelObjectsProducts = [[NSBundle mainBundle] loadNibNamed:@"OrderHistoryDetailCell" owner:self options:nil];
            orderDetailcell = (OrderHistoryDetailCell *)[topLevelObjectsProducts objectAtIndex:0];
            [orderDetailcell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [orderDetailcell.contentView setBackgroundColor:[UIColor clearColor]];
            [orderDetailcell setBackgroundColor:[UIColor clearColor]];
        }
        
        UIColor *bgColo = ((indexPath.row%2)==0) ? kAppLightGrayColor : kAppGrayColor;
        [orderDetailcell setBackgroundColor:bgColo];
        [orderDetailcell.contentView setBackgroundColor:bgColo];
        
        if (self.selectedOrderIndex >= 0) {
            
            OrderItem *orderItem;
            if (self.selectedOrderIndex>=0)
                orderItem = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
            [self updateDetailForOrder:orderItem];
            
            SelectedInventoryItem *inventory = [orderItem.array_selectedInventory objectAtIndex:indexPath.row];
            orderDetailcell.label_title.text = inventory.Name;
            orderDetailcell.label_qty.text = inventory.Quantity;
            orderDetailcell.label_price.text = [NSString stringWithFormat:@"$%0.2f",[inventory.UnitPrice doubleValue]];
            orderDetailcell.label_subTotal.text = [NSString stringWithFormat:@"$%0.2f",[inventory.Price doubleValue]];
            orderDetailcell.label_discount.text = [NSString stringWithFormat:@"%0.2f%c",[inventory.DiscountRate doubleValue],'%'];
            orderDetailcell.label_total.text = [NSString stringWithFormat:@"$%0.2f",[inventory.SellingPrice doubleValue]];
        }
        
        return orderDetailcell;
    }
    else if([tableView isEqual:self.tableView_orderTotal]) {
        
        static NSString *CellIdentifier = @"CustomerRecordsCustomCell2ID";
        CustomerRecordsCustomCell2 *recordCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if(recordCell==nil) {
            
            NSArray *topLevelObjectsProducts = [[NSBundle mainBundle] loadNibNamed:@"CustomerRecordsCustomCell2" owner:self options:nil];
            recordCell = (CustomerRecordsCustomCell2 *)[topLevelObjectsProducts objectAtIndex:0];
            [recordCell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [recordCell.contentView setBackgroundColor:[UIColor clearColor]];
            [recordCell setBackgroundColor:[UIColor clearColor]];
            
        }
        
        UIColor *bgColo = ((indexPath.row%2)==0) ? kAppLightGrayColor : kAppGrayColor;
        [recordCell setBackgroundColor:bgColo];
        [recordCell.contentView setBackgroundColor:bgColo];
        
        
        recordCell.labelTotal.textColor = [UIColor blackColor];
        [recordCell.labelTotal setFont:[UIFont systemFontOfSize:14.0]];
        [recordCell.labelType setFont:[UIFont systemFontOfSize:14.0]];
        recordCell.labelType.textColor = kAppThemeColor;
        recordCell.labelTotal.textColor=[UIColor blackColor];
        
        OrderItem *orderItem;
        if (self.selectedOrderIndex >= 0)
            orderItem = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
        
        
        switch (indexPath.row) {
                
            case 0: {
                
                recordCell.labelType.text = @"Items Total:";
                recordCell.labelTotal.text = [NSString stringWithFormat:@"$%0.2f",[orderItem.SellingPrice doubleValue]];
            }
                break;
                
            case 1: {
                
                recordCell.labelType.text = @"Sales Tax (%):";
                recordCell.labelTotal.text = [NSString stringWithFormat:@"$%0.2f",[orderItem.SalesTax doubleValue]];
            }
                break;
                
            case 2: {
                
                recordCell.labelType.text = @"S&H:";
                recordCell.labelTotal.text = [NSString stringWithFormat:@"$%0.2f",[orderItem.ShippingNHandling doubleValue]];
            }
                break;
                
            case 3: {
                
                recordCell.labelType.text = @"Total:";
                recordCell.labelTotal.text = [NSString stringWithFormat:@"$%0.2f",[orderItem.SaleTotal doubleValue]];
            }
                break;
                
            case 4: {
                recordCell.labelType.text = @"Tips:";
                recordCell.labelTotal.text = [NSString stringWithFormat:@"$%0.2f",[orderItem.tipTotal doubleValue]];
            }
                break;
                
            case 5: {
                recordCell.labelType.text  = @"Total w/ Tip:";
                [recordCell.labelType setFont:[UIFont boldSystemFontOfSize:14.5]];
                [recordCell.labelTotal setFont:[UIFont boldSystemFontOfSize:15.5]];
                recordCell.labelTotal.textColor=kAppThemeColor;
                recordCell.labelTotal.text = [NSString stringWithFormat:@"$%0.2f",[orderItem.totalWithTip doubleValue]];
            }
                break;
                
            default:
                break;
        }
        
        return recordCell;
    }
    
    return [UITableViewCell new];
}

#pragma mark - * * * * UITableViewDelegate Methods * * * *
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat rowHeight = 0.0;
    if([tableView isEqual:self.tableView_orderTotal] && ![[[AppUserData sharedInstance] selectedLocation] enableWholesaleTab]) {
         rowHeight = 25.0;
    }
    else
        rowHeight = 40.0;
    
    return rowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat viewHeight = 0.0;
    if([tableView isEqual:self.tableView_orderTotal])
        viewHeight = 0.0;
    else
        viewHeight = 43.0;
    
    return viewHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = nil;
    if([tableView isEqual:self.tableView_orderList]) {
        headerView = self.view_orderListHeader;
    }
    else if([tableView isEqual:self.tableView_orderDetail]) {
        headerView = self.view_orderDetailHeader;
    }
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectZero];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if ([tableView isEqual:self.tableView_orderList]) {
        self.selectedOrderIndex = indexPath.row;
        selectedIndexForNotes = -1;
        
        OrderItem *orderItem = [self.array_orderList objectAtIndex:indexPath.row];
        [self.label_orderItemCount setText:@"Total Items: 0"];
        self.orderID = orderItem.orderID;
        isForCheckedoutOrder = NO;
        
        [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForOrderDetailsForOrder:orderItem.orderID isFromPendingOrder:@"0"] forMethod:getOrderDetails];
    }
}

#pragma mark - * * * RefundItemsDelegate Method * * *
-(void)refundItemsSelected:(NSMutableArray*)items {
    
    OrderItem *orderItem;
    if (self.selectedOrderIndex>=0)
        orderItem = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
    
    selectedItems = items;
    if ([orderItem.PaymentStatus isEqualToString:@"Charged"] && [[[AppUserData sharedInstance] selectedLocation] allowrefundincashoption]) {
        UIAlertView *refundAlert=[[UIAlertView alloc] initWithTitle:@"Would you prefer to refund this amount in cash?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES", @"NO", nil];
        refundAlert.tag=1007;
        [refundAlert show];
        [APPDELEGATE setVisibleAlertView:refundAlert];
        [APPDELEGATE setDismissButtonIndex:1];
    }else {
        [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForRefundItemsForOrder:orderItem.orderID withItems:items withPaymentStatus:NO] forMethod:refundItems];
    }
}

#pragma mark - * * * SignatureCapturedDelegate Method * * *
-(void)signatureCapturedWithImage:(UIImage*)sign andName:(NSString *)name {
    
    OrderItem *selectedOrder;
    if (self.selectedOrderIndex>=0)
        selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
    
    [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForSignForOrder:selectedOrder.orderID encodedImage:[UIImageJPEGRepresentation(sign, 1.0) base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn] withExtension:@"jpg" firstName:[name getFirstName] andLastName:[name getLastName]] forMethod:signForOrder];
}

#pragma mark - Webservices Calling

-(void)callWebAPIWithRequest:(NSString *)soapContent forMethod:(WebMethodType)method{
    
    RequestResponseHandler *objHandler = [[RequestResponseHandler alloc] init];
    objHandler.delegate = self;
    
    [objHandler call_SOAP_POSTMethodWithRequest:soapContent andMethodName:method andController:[APPDELEGATE navigationController]];
}

#pragma mark - RequestResponseHandlerDelegate
-(void)ResponseContentFromServer:(RequestResponseHandler *)responseObj withData:(id)jsonData forMethod:(WebMethodType)methodType {
    
    if ([responseObj.responseCode intValue] == SERVER_RESPONSE_SUCCESS_CODE) {
        
        switch (methodType) {
            case addNotesToOrder: {
                
            }
                break;
                
            case getOrderList: {
                
                if (isForUpdateOrderStatus) {
                    
                    isForUpdateOrderStatus = NO;
                    
                    OrderItem *selectedOrder;
                    if (self.selectedOrderIndex>=0)
                        selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                    NSArray *orderList = [[jsonData objectForKey:@"OrderArray"] objectForKey:@"Order"];
                    
                    if (orderList && [orderList isKindOfClass:[NSDictionary class]])
                        [OrderItem updateOrder:selectedOrder with:(NSDictionary*)orderList];
                    else
                        [OrderItem updateOrder:selectedOrder with:[orderList firstObject]];
                }
                else {
                    [self.array_orderList removeAllObjects];
                    self.selectedOrderIndex = -1;
                    selectedIndexForNotes = -1;
                    
                    [self.label_orderNumber setText:[NSString stringWithFormat:@"Details For:"]];
                    [self.label_orderCount setText:[NSString stringWithFormat:@"Total Orders: %lu",(unsigned long)self.array_orderList.count]];
                    [self.label_orderItemCount setText:@"Total Items: 0"];
                    
                    
                    NSArray *orderList = [[jsonData objectForKey:@"OrderArray"] objectForKey:@"Order"];
                    if (orderList && [orderList isKindOfClass:[NSDictionary class]]) {
                        [self.array_orderList addObject:[OrderItem getOrderItemFrom:(NSDictionary*)orderList]];
                    }
                    else {
                        for (NSDictionary *item in orderList)
                            [self.array_orderList addObject:[OrderItem getOrderItemFrom:item]];
                    }
                    
                    [self sortDataForTag:sortedButtonTag shouldInvertSorting:NO];
                    
                    if (self.array_orderList.count > 0) {
                        self.selectedOrderIndex = 0;
                        selectedIndexForNotes = -1;
                        
                        if ([self.orderID length] > 0) {
                            
                            for (OrderItem *orderItem in self.array_orderList) {
                                if ([orderItem.orderID isEqualToString:self.orderID]) {
                                    self.selectedOrderIndex = [self.array_orderList indexOfObject:orderItem];
                                    selectedIndexForNotes = -1;
                                    
                                    break;
                                }
                            }
                        }
                        
                        [self.tableView_orderList reloadData];
                        
                        if (shouldScrollToMakeSelectedIndexVisible == YES) {
                            if (self.selectedOrderIndex>=0 && self.selectedOrderIndex < self.array_orderList.count)
                                [self.tableView_orderList scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedOrderIndex inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                        }
                        
                        OrderItem *firstOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                        self.orderID = firstOrder.orderID;
                        isForCheckedoutOrder = NO;
                        [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForOrderDetailsForOrder:firstOrder.orderID isFromPendingOrder:@"0"] forMethod:getOrderDetails];
                        
                        [self updateDetailForOrder:firstOrder];
                    }
                }
            }
                break;
                
            case getOrderDetails: {
                
                if (isForCheckedoutOrder) {
                    isForCheckedoutOrder = NO;
                    
                    OrderItem *searchedOrderItem = nil;
                    for (OrderItem *orderItem in self.array_orderList) {
                        if ([orderItem.orderID isEqualToString:[[APPDELEGATE array_newFinalizedOrderID] firstObject] ])
                            searchedOrderItem = orderItem;
                    }
                    NSDictionary *orederItemJSON = [jsonData objectForKey:@"OrderDetail"];
                    
                    if (searchedOrderItem) {
                        [OrderItem updateOrderItem:searchedOrderItem with:orederItemJSON];
                        
                        [self manageButtonStatesForOrder:searchedOrderItem];
                        [self.label_orderItemCount setText:[NSString stringWithFormat:@"Total Items: %lu",(unsigned long)searchedOrderItem.array_selectedInventory.count]];
                    }
                    else {
                        [self.array_orderList insertObject:[OrderItem getOrderItemFrom:orederItemJSON] atIndex:0];
                        
                        if (self.selectedOrderIndex>=0) self.selectedOrderIndex++;
                        
                    }
                }
                else {
                    
                    OrderItem *selectedOrder;
                    if (self.selectedOrderIndex>=0)
                        selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                    
                    NSDictionary *orederItem = [jsonData objectForKey:@"OrderDetail"];
                    [OrderItem updateOrderItem:selectedOrder with:orederItem];
                    [self manageButtonStatesForOrder:selectedOrder];
                    [self.label_orderItemCount setText:[NSString stringWithFormat:@"Total Items: %lu",(unsigned long)selectedOrder.array_selectedInventory.count]];
                }
            }
                break;
                
            case signForOrder: {
                
                OrderItem *selectedOrder;
                if (self.selectedOrderIndex>=0)
                    selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                
                NSDictionary *orederItem = [jsonData objectForKey:@"OrderChange"];
                [selectedOrder setShippingStatus:[orederItem getTextValueForKey:@"ShippingStatus"]];
                
                [self manageButtonStatesForOrder:selectedOrder];
                [self.label_orderItemCount setText:[NSString stringWithFormat:@"Total Items: %lu",(unsigned long)selectedOrder.array_selectedInventory.count]];
            }
                break;
                
            case refundItems: {
                
                [APPDELEGATE displayAlertWithTitle:@"" andMessage:responseObj.responseMessage];
                
                isForUpdateOrderStatus = YES;
                OrderItem *selectedOrder;
                if (self.selectedOrderIndex>=0)
                    selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                selectedOrder.PaymentStatus = [jsonData getTextValueForKey:@"PaymentStatus"];
                
                OrderItem *newOrder = [[OrderItem alloc] init];
                NSDictionary *orederItem = [jsonData objectForKey:@"OrderDetail"];
                [OrderItem updateOrderItem:newOrder with:orederItem];
                [self.array_orderList addObject:newOrder];
                
                self.selectedOrderIndex = self.array_orderList.count-1;
                selectedIndexForNotes = -1;
                
                self.orderID = newOrder.orderID;
                isForCheckedoutOrder = NO;
                
                [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForOrderDetailsForOrder:newOrder.orderID isFromPendingOrder:@"0"] forMethod:getOrderDetails];
                [self.tableView_orderList reloadData];
                [self.tableView_orderList scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedOrderIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                
                [self.label_orderCount setText:[NSString stringWithFormat:@"Total Orders: %lu",(unsigned long)self.array_orderList.count]];
                [self.label_orderItemCount setText:@"Total Items: 0"];
                
                NSString *status = [orederItem getTextValueForKey:@"Status"];
                if (status.length > 0)
                    selectedOrder.Status = [orederItem getTextValueForKey:@"Status"];
                selectedOrder.ShippingStatus = [orederItem getTextValueForKey:@"ShippingStatus"];
                [self updateDetailForOrder:selectedOrder];
            }
                break;
                
            case refundOrder: {
                
                [APPDELEGATE displayAlertWithTitle:@"" andMessage:responseObj.responseMessage];
                
                OrderItem *selectedOrder;
                if (self.selectedOrderIndex>=0)
                    selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                
                selectedOrder.PaymentStatus = [jsonData getTextValueForKey:@"PaymentStatus"];
                
                OrderItem *newOrder = [[OrderItem alloc] init];
                NSDictionary *orederItem = [jsonData objectForKey:@"OrderDetail"];
                [OrderItem updateOrderItem:newOrder with:orederItem];
                
                [self.array_orderList addObject:newOrder];
                
                //When refunding an order the new refunded order returned should be selected.  Currently it is adding the refunded order to the list but it is not selecting it.
                self.selectedOrderIndex = self.array_orderList.count-1;
                selectedIndexForNotes = -1;
                
                self.orderID = newOrder.orderID;
                isForCheckedoutOrder = NO;
                
                [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForOrderDetailsForOrder:newOrder.orderID isFromPendingOrder:@"0"] forMethod:getOrderDetails];
                [self.tableView_orderList reloadData];
                [self.tableView_orderList scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedOrderIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                
                [self.label_orderCount setText:[NSString stringWithFormat:@"Total Orders: %lu",(unsigned long)self.array_orderList.count]];
                [self.label_orderItemCount setText:@"Total Items: 0"];
                
                
                NSString *status = [orederItem getTextValueForKey:@"Status"];
                if (status.length > 0)
                    selectedOrder.Status = [orederItem getTextValueForKey:@"Status"];
                selectedOrder.ShippingStatus = [orederItem getTextValueForKey:@"ShippingStatus"];
                [self updateDetailForOrder:selectedOrder];
            }
                break;
                
            case refundShipping: {
                
                [APPDELEGATE displayAlertWithTitle:@"" andMessage:responseObj.responseMessage];
                
                OrderItem *selectedOrder;
                if (self.selectedOrderIndex>=0)
                    selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                
                selectedOrder.PaymentStatus = [jsonData getTextValueForKey:@"PaymentStatus"];
                selectedOrder.ShippingStatus = [jsonData getTextValueForKey:@"ShipStatus"];
                
                OrderItem *newOrder = [[OrderItem alloc] init];
                NSDictionary *orederItem = [jsonData objectForKey:@"OrderDetail"];
                [OrderItem updateOrderItem:newOrder with:orederItem];
                [self.array_orderList addObject:newOrder];
                
                self.selectedOrderIndex = self.array_orderList.count-1;
                selectedIndexForNotes = -1;
                
                self.orderID = newOrder.orderID;
                isForCheckedoutOrder = NO;
                
                [self callWebAPIWithRequest:[SoapEnvelope getSOAPRequestForOrderDetailsForOrder:newOrder.orderID isFromPendingOrder:@"0"] forMethod:getOrderDetails];
                [self.tableView_orderList reloadData];
                [self.tableView_orderList scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedOrderIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                
                [self.label_orderCount setText:[NSString stringWithFormat:@"Total Orders: %lu",(unsigned long)self.array_orderList.count]];
                [self.label_orderItemCount setText:@"Total Items: 0"];
                
                
                NSString *status = [orederItem getTextValueForKey:@"Status"];
                if (status.length > 0)
                    selectedOrder.Status = [orederItem getTextValueForKey:@"Status"];
                selectedOrder.ShippingStatus = [orederItem getTextValueForKey:@"ShippingStatus"];
                [self updateDetailForOrder:selectedOrder];
            }
                break;
                
            case voidOrder: {
                
                [APPDELEGATE displayAlertWithTitle:@"" andMessage:responseObj.responseMessage];
                
                OrderItem *selectedOrder;
                if (self.selectedOrderIndex>=0)
                    selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                
                NSDictionary *orederItem = [jsonData objectForKey:@"OrderDetail"];
                
                [selectedOrder setPaymentStatus:[orederItem getTextValueForKey:@"PaymentStatus"]];
                [selectedOrder setShippingStatus:[orederItem getTextValueForKey:@"ShippingStatus"]];
                [self manageButtonStatesForOrder:selectedOrder];
                
                NSString *status = [orederItem getTextValueForKey:@"Status"];
                if (status.length > 0)
                    selectedOrder.Status = [orederItem getTextValueForKey:@"Status"];
                selectedOrder.ShippingStatus = [orederItem getTextValueForKey:@"ShippingStatus"];
                [self updateDetailForOrder:selectedOrder];
            }
                break;
                
            case calculateForCheckOut: {
                
                OrderItem *selectedOrder;
                if (self.selectedOrderIndex>=0)
                    selectedOrder = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
                
                NSMutableArray *inventory = [NSMutableArray array];
                NSArray *inventoryData = [[jsonData objectForKey:@"InventoryArray"] objectForKey:@"InventorySelection"];
                
                if ([inventoryData isKindOfClass:[NSDictionary class]]) {
                    [inventory addObject:[SelectedInventoryItem getInventoryItemfrom:(NSDictionary*)inventoryData withOrderStatus:@""]];
                }
                else {
                    for (NSDictionary *data in inventoryData)
                        [inventory addObject:[SelectedInventoryItem getInventoryItemfrom:data withOrderStatus:@""]];
                }
                
                TransactionRecord *transactionRec = [TransactionRecord getTransactionrecordFrom:[jsonData objectForKey:@"TransactionRecord"]];
                transactionRec.orderID = selectedOrder.orderID;
                transactionRec.recalculateStatus = @"0";
                CheckOutVC *checkOutVC = [[CheckOutVC alloc] initWithNibName:@"CheckOutVC" bundle:nil];
                checkOutVC.transactionRecord = transactionRec;
                checkOutVC.inventoryArray = inventory;
                [[self navigationController] pushViewController:checkOutVC animated:YES];
            }
                break;
                
            case getOrderForRefund: {
                
                if (selectedButtonTag == 308) {
                    
                    NSDictionary *orederItem = [jsonData objectForKey:@"OrderDetail"];
                    OrderItem *orderItemRefund = [OrderItem getOrderItemFrom:(NSDictionary*)orederItem];
                    [OrderItem updateOrderItem:orderItemRefund with:orederItem];
                    
                    // Swap response
                    NSString *newOrderID = [[DBHelper instance] makeNewEntryForOrder:orderItemRefund withRecalculateStatus:@"1" forUser:[[AppUserData sharedInstance] loggedInUser] andIsForSWAP:YES isFromPendingOrder:NO];
                    [[APPDELEGATE baseController] modifyOrderWithOrder:newOrderID];
                    
                }
                else if(selectedButtonTag == 303) {
                    // Refund items response
                    NSDictionary *orederItem = [jsonData objectForKey:@"OrderDetail"];
                    NSArray *inventoryList = [[orederItem objectForKey:@"InventoryArray"] objectForKey:@"InventorySelection"];
                    
                    NSMutableArray *itemsArray = [NSMutableArray array];
                    
                    if ([inventoryList isKindOfClass:[NSDictionary class]]) {
                        [itemsArray addObject:[SelectedInventoryItem getInventoryItemfrom:(NSDictionary*)inventoryList withOrderStatus:@""]];
                    }
                    else {
                        for (NSDictionary *item in inventoryList)
                            [itemsArray addObject:[SelectedInventoryItem getInventoryItemfrom:item withOrderStatus:@""]];
                    }
                    if ([itemsArray count] > 0) {
                        
                        RefundItemsViewController *refundItems = [[RefundItemsViewController alloc] initWithNibName:@"RefundItemsViewController" bundle:nil];
                        refundItems.array_purchasedItems = itemsArray;
                        [refundItems setDelegate:self];
                        [refundItems setModalPresentationStyle:UIModalPresentationFormSheet];
                        [refundItems setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                        refundItems.preferredContentSize = CGSizeMake(550.0, 450.0);
                        [self presentViewController:refundItems animated:NO completion:nil];
                    }
                    else {
                        [APPDELEGATE displayAlertWithTitle:@"" andMessage:@"No item found."];
                    }
                    
                }
            }
                break;
                
            default:
                break;
        }
        //20/05/2016 Then we also need to sum the amount, tip, and totalwithtip values on the order history screens and display them on the top to the left of Total Orders: #
        double sumOfSaleTotal = 0, sumOfTips = 0, sumOfTotalAmount = 0;
        for (OrderItem *orders in self.array_orderList) {
            
            sumOfSaleTotal = sumOfSaleTotal + [orders.SaleTotal doubleValue];
            sumOfTips = sumOfTips + ([orders.Tip length] ? [orders.Tip doubleValue]: [orders.tipTotal doubleValue]);
            sumOfTotalAmount = sumOfTotalAmount + [orders.SaleTotal doubleValue] + ([orders.Tip length] ? [orders.Tip doubleValue]: [orders.tipTotal doubleValue]);
        }
        if ([[[AppUserData sharedInstance] selectedLocation] enableWholesaleTab]) {
            [self.label_orderCount setText:[NSString stringWithFormat:@"Total Orders: %lu - Total Amount: %@",(unsigned long)self.array_orderList.count,getUSCurrencyFormat(sumOfSaleTotal)]];
        }else {
            [self.label_orderCount setText:[NSString stringWithFormat:@"Total Orders: %lu - Total Sales: %@ - Total Tips: %@ - Total Amount: %@",(unsigned long)self.array_orderList.count,getUSCurrencyFormat(sumOfSaleTotal),getUSCurrencyFormat(sumOfTips),getUSCurrencyFormat(sumOfTotalAmount)]];
        }
       
    }
    else {
        
        [APPDELEGATE displayAlertWithTitle:@"" andMessage:responseObj.responseMessage];
    }
    
    //no matter service calls sucessfully or get an error need to take the next step
    switch (methodType) {
            
        case emailSalesCreditReceipt: {
            
            [self callForPrintReciept:0];
        }
            break;
            
        case getHTMLReceiptForPrinting: {
            
            OrderItem *orderItem = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
            
            self.receiptPathForCustomer = nil;
            self.receiptPathForMerchant = nil;
            
            self.receiptForCustomer = [[jsonData objectForKey:@"ReceiptDetailRecord"] getTextValueForKey:@"HTMLReceiptCustomerCopy"];
            self.receiptForMerchant = [[jsonData objectForKey:@"ReceiptDetailRecord"] getTextValueForKey:@"HTMLReceiptMerchantCopy"];
            
            if ([self.receiptForCustomer length]) {
                
                PaymentItem *payment = [array_recieptOrders objectAtIndex:lastRecieptIndex];
                NSString *copyName = [NSString stringWithFormat:@"%@_%@",orderItem.orderID,payment.PaymentId];
                
                self.htmlConverter = [HtmlToImage createFileWithHTMLContent:self.receiptForCustomer
                                                                pathForFile:[pathForOrderRecieptWithID(orderItem.orderID) stringByAppendingPathComponent:getFileNameForCustomerCopy(copyName)]
                                                                   delegate:self
                                                                   pageSize:CGSizeMake(kReceiptWidth, MAXFLOAT)
                                                                    margins:UIEdgeInsetsZero];
            }
            else {
                [APPDELEGATE displayAlertWithTitle:@"Error!" andMessage:@"Unable to get receipt. Please try again later."];
                [self callForPrintReciept:(lastRecieptIndex+1)];
            }
        }
            break;
        default:
            break;
    }
    
    [self.tableView_orderList reloadData];
    [self.tableView_orderDetail reloadData];
    [self.tableView_orderTotal reloadData];
}

#pragma mark - HtmlToImageDelegate
- (void)HTMLtoImageOrPDFDidSucceed:(HtmlToImage*)htmlToPDF {
    
    if (self.receiptPathForCustomer == nil) {
        self.receiptPathForCustomer = htmlToPDF.imagePath;
        self.htmlConverter = nil;
        
        if ([self.receiptForMerchant length]) {
            OrderItem *orderItem = [self.array_orderList objectAtIndex:self.selectedOrderIndex];
            PaymentItem *payment = [array_recieptOrders objectAtIndex:lastRecieptIndex];
            NSString *copyName = [NSString stringWithFormat:@"%@_%@",orderItem.orderID,payment.PaymentId];
            
            self.htmlConverter = [HtmlToImage createFileWithHTMLContent:self.receiptForMerchant
                                                            pathForFile:[pathForOrderRecieptWithID(orderItem.orderID) stringByAppendingPathComponent:getFileNameForMerchantCopy(copyName)]
                                                               delegate:self
                                                               pageSize:CGSizeMake(kReceiptWidth, MAXFLOAT)
                                                                margins:UIEdgeInsetsZero];
        }
        else {
            [self printFiles:[NSArray arrayWithObjects:self.receiptPathForCustomer, nil]];
        }
    }
    else {
        self.receiptPathForMerchant = htmlToPDF.imagePath;
        
        [self printFiles:[NSArray arrayWithObjects:self.receiptPathForMerchant, self.receiptPathForCustomer, nil]];
    }
}

- (void)HTMLtoImageOrPDFDidFail:(HtmlToImage*)htmlToPDF {
    
    [APPDELEGATE displayAlertWithTitle:@"Sorry!" andMessage:@"Unable to print receipt. Please try again later."];
    self.receiptPathForCustomer = nil;
    self.receiptPathForMerchant = nil;
}

#pragma mark - Method for Retry operation when Printer is not connected successfully with iPad
-(void)retryForPrintReceipt {
    [MBProgressHUD hideAllHUDsForView:[APPDELEGATE navigationController].view animated:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Printer is unavailable..."
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleAlert];
    NSAttributedString *message = [[NSAttributedString alloc] initWithString:@"Press Retry to resubmit print job." attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:15.0f]}];
    [alert setValue:message forKey:@"attributedMessage"];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Retry"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              [self  callForPrintReciept:lastRecieptIndex];
                                                          }];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               lastRecieptIndex = -1;
                                                           }];
    
    [alert addAction:secondAction];
    [alert addAction:firstAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
