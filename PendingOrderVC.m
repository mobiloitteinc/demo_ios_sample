//
//  PendingOrderVC.m
//  Active Club Solutions -iPAD App
//
//  Created by Krishna Kant Kaira on 29/09/14.
//  Copyright (c) 2014 Mobiloitte Technologies. All rights reserved.
//

#import "PendingOrderVC.h"

#import "OrderPanelCategoryTableViewCell.h"
#import "PendingPopoverCustomCell.h"
#import "CustomerSelectionPopOver.h"
#import "MarqueeLabel.h"
#import "EnterPINViewController.h"
#import "SendItemsViewController.h"
#import "SplitItemsViewController.h"
#import "ItemDetailsViewController.h"


#define kErrorMessage_otherUserOrder @"You cannot edit another user's order. Please use the \"Add to Order\" option."

@interface PendingOrderVC() <UIPopoverControllerDelegate, UISearchDisplayDelegate, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UITextFieldDelegate, RequestResponseHandlerDelegate, SplitItemsDelegate, ItemDetailsDelegate> {
    
    //flag value to determine wether 'deleteOrder' API is for 'Clear All Unpaid Orders' function
    BOOL isDeleteAllOrder;
    
    //flag value to determine wether checkout API is for 'saveOrder' only
    BOOL isForSaveOrder;
    
    //used to carry current index of selected order to be deleted for 'Clear All Unpaid Orders' function
    NSInteger currentOrderToBeDeleted;
    
    //used to carry index of selected inventory to be deleted
    NSInteger selectedInventoryIndexToBeDeleted;
    
    //used to carry index of selected order from left panel for SHARED orders ONLY!
    NSInteger selectedSharedOrderIndex;
    
    //flag value to determine wether checkout API is for 'SendItems' only
    BOOL isForSendItems;
    
    BOOL isForAddToOrder;
    
    // Used to carry index for split item.
    NSInteger selectedOrderIndex;
}

@property (nonatomic, strong) IBOutlet ACSButton *button_delete;
@property (nonatomic, strong) IBOutlet ACSButton *button_addToOrder;
@property (nonatomic, strong) IBOutlet ACSButton *button_checkOut;
@property (nonatomic, strong) IBOutlet ACSButton *button_saveOrder;
@property (nonatomic, strong) IBOutlet ACSButton *button_clearAllUnpaidOrders;

@property (nonatomic, strong) IBOutlet MarqueeLabel *label_customerNotes;
@property (nonatomic, strong) IBOutlet UITableView *tableView_pendingOrders;
@property (nonatomic, strong) IBOutlet UITableView *tableView_orderDetail;

@property (nonatomic, strong) IBOutlet UILabel *label_totalQuantity;
@property (nonatomic, strong) IBOutlet UILabel *label_totalAmount;
@property (nonatomic, strong) IBOutlet UILabel *label_membershipClub;
@property (nonatomic, strong) IBOutlet UILabel *label_customerName;
@property (strong, nonatomic) IBOutlet UILabel *label_subTotal;

@property (nonatomic, strong) NSMutableArray *array_pendingOrders_Local;
@property (nonatomic, strong) NSMutableArray *array_pendingOrders_Shared;
@property (nonatomic, strong) NSMutableArray *array_pendingOrderData;

@property (nonatomic, strong) AppUserData *appUser;

@property (nonatomic, strong) TBL_UserOrder *userSelectedPendingOrder;

@property (nonatomic, strong) NSString *selectedPendingOrderID;

@property (nonatomic, strong) IBOutlet UILabel *label_POSCustomer;
@property (nonatomic, strong) IBOutlet UIView *view_nonPosCustomer;
@property (nonatomic, strong) IBOutlet UISegmentedControl *segment_pendingOrder;

@property (strong, nonatomic) IBOutlet ACSButton *button_sendTo;


-(IBAction)CommonButtonAction:(id)sender;
-(IBAction)segmentSelected:(UISegmentedControl *)sender;

- (IBAction)sendToButtonAction:(id)sender;

@end

@implementation PendingOrderVC

#pragma mark - * * * * ViewController life cycle methods * * * *
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setupContollerDefaults];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[self navigationItem] setHidesBackButton:YES];
    [[self navigationController] setNavigationBarHidden:YES];
    
    //set navigationBar's title
    [[APPDELEGATE baseController] setScreenTitle:@"Pending Orders"];
    [[APPDELEGATE baseController] setSelectedIndex:1];
    isForAddToOrder = NO;
    
    [self refreshPendingOrdersData];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    if (self.presentedViewController)
        [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - * * * * Other Helper Methods * * *
-(void)setupContollerDefaults {
    
    self.array_pendingOrders_Local = [NSMutableArray array];
    self.array_pendingOrders_Shared = [NSMutableArray array];
    self.array_pendingOrderData = [NSMutableArray array];
    
    self.appUser = [AppUserData sharedInstance];
    [self.segment_pendingOrder setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue" size:16.0],NSFontAttributeName, kAppThemeColor, NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    [self.segment_pendingOrder setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"HelveticaNeue" size:16.0],NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    
    selectedInventoryIndexToBeDeleted = -1;
    currentOrderToBeDeleted = -1;
    selectedSharedOrderIndex = -1;
    selectedOrderIndex = -1;
    isDeleteAllOrder = NO;
    isForSaveOrder = NO;
    isForAddToOrder = NO;
    
    [self.button_delete setExclusiveTouch:YES];
    [self.button_checkOut setExclusiveTouch:YES];
    [self.button_addToOrder setExclusiveTouch:YES];
    [self.button_clearAllUnpaidOrders setExclusiveTouch:YES];
    [self.button_saveOrder setExclusiveTouch:YES];
    [self.button_sendTo setExclusiveTouch:YES];
    
    [[self tableView_pendingOrders] setDelegate:self];
    [[self tableView_pendingOrders] setDataSource:self];
    [self.tableView_pendingOrders setBackgroundView:nil];
    [self.tableView_pendingOrders setBackgroundColor:[UIColor clearColor]];
    
    [[self tableView_orderDetail] setDelegate:self];
    [[self tableView_orderDetail] setDataSource:self];
    
    [self hideSendItemsButton];
}

-(void)hideSendItemsButton {
    
    BOOL disbleLacationPrinter = ![[[AppUserData sharedInstance]selectedLocation]enableLocationPrinters];
    if (disbleLacationPrinter) {
        
        [self.button_sendTo setHidden:YES];
        [self.label_subTotal setFrame:CGRectMake(19, 8, 250, 39)];
    }

}

-(void)refreshPendingOrdersData {
    
    switch (self.segment_pendingOrder.selectedSegmentIndex) {
            
        case 0:  {
            //Local
            
            [self.array_pendingOrderData removeAllObjects];
            [self.array_pendingOrders_Local removeAllObjects];
            self.selectedPendingOrderID = @"";
            self.userSelectedPendingOrder = nil;
            
            NSArray *sortingDescriptor = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"userOrderToAppUser.userFullName" ascending:YES],[NSSortDescriptor sortDescriptorWithKey:@"orderNumber" ascending:NO], nil];
            
            self.array_pendingOrders_Local = [NSMutableArray arrayWithArray:[[[DBHelper instance] getAllPendingOrderswithLocation:[[[AppUserData sharedInstance] selectedLocation] locationID]] sortedArrayUsingDescriptors:sortingDescriptor]];
            
            [self.button_clearAllUnpaidOrders setEnabled:(self.array_pendingOrders_Local.count > 0)];
        }
            break;
            
        case 1: {
            //Shared
            
            [self.array_pendingOrders_Shared removeAllObjects];
            selectedSharedOrderIndex = -1;
            
            RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
            objHandler.delegate = self;
            [objHandler call_SOAP_POSTMethodWithRequest:[SoapEnvelope getPendingOrderListForLocation:[[[AppUserData sharedInstance] selectedLocation] locationName]] andMethodName:getPendingOrders andController:[APPDELEGATE navigationController]];
            
            if ((self.array_pendingOrders_Shared.count > 0) && (selectedSharedOrderIndex >= 0)) {
                
                OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                [self.button_clearAllUnpaidOrders setEnabled:[orderItem.Status isIdenticaleTo:@"Open"]];
            }
            else {
                [self.button_clearAllUnpaidOrders setEnabled:NO];
            }
        }
            break;
            
        default:
            break;
    }
    
    selectedInventoryIndexToBeDeleted = -1;
    currentOrderToBeDeleted = -1;
    isDeleteAllOrder = NO;
    isForSaveOrder = NO;
    
    [self.label_customerName setText:@""];
    [self.label_membershipClub setText:@""];
    [self.label_customerNotes setText:@""];
    
    [self refreshUserPendingOrderProducts];
}

-(void)refreshUserPendingOrderProducts {
    
    [self.label_customerName setText:@""];
    [self.label_membershipClub setText:@""];
    [self.label_customerNotes setText:@""];
    
    switch (self.segment_pendingOrder.selectedSegmentIndex) {
            
        case 0:  {
            //Local
            
            [self.array_pendingOrderData removeAllObjects];
            
            for (TBL_UserOrder *order in self.array_pendingOrders_Local) {
                if ([order.orderID isEqualToString:self.selectedPendingOrderID]) {
                    self.userSelectedPendingOrder = order;
                    break;
                }
            }
            
            if (self.userSelectedPendingOrder) {
                
                [self.array_pendingOrderData addObjectsFromArray:[[self.userSelectedPendingOrder userOrderToProduct] allObjects]];
                
                [self.label_customerName setText:self.userSelectedPendingOrder.userOrderToCustomer.customerFullName];
                [self.label_membershipClub setText:self.userSelectedPendingOrder.userOrderToCustomer.customerMemberShipClub];
                [self.label_customerNotes setText:self.userSelectedPendingOrder.userOrderToCustomer.customerNotes];
            }
            
            // If you refund the bottom item it puts the refunded item above it.  All items in green should stay at the top of the list, then the items in blue show up beneath them, then any new items added in black should be the last items shown. Mixing and matching the colors is confusing in the list.  They should be grouped together.
            //Green -> Blue -> Black
            //isSwap -> isRefunded -> shouldPayFor
            NSArray *sortDescriptor = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"isSwap" ascending:NO], [NSSortDescriptor sortDescriptorWithKey:@"isRefunded" ascending:YES], [NSSortDescriptor sortDescriptorWithKey:@"shouldPayFor" ascending:YES],[NSSortDescriptor sortDescriptorWithKey:@"sequenceNumber" ascending:NO], nil];

            [self.array_pendingOrderData sortUsingDescriptors:sortDescriptor];
        }
            break;
            
        case 1: {
            //Shared
            if (selectedSharedOrderIndex >= 0) {
                OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                
                [self.label_customerName setText:orderItem.CustomerName];
                [self.label_membershipClub setText:orderItem.CustomerMembershipClub];
                [self.label_customerNotes setText:orderItem.CustomerNotes];
                NSArray *sortDescriptor = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"canRefund" ascending:NO], [NSSortDescriptor sortDescriptorWithKey:@"isRefunded" ascending:NO], [NSSortDescriptor sortDescriptorWithKey:@"shouldPayFor" ascending:NO], nil];
                
                [orderItem.array_selectedInventory sortUsingDescriptors:sortDescriptor];
            }
        }
            break;
            
        default:
            break;
    }
    
    [self.tableView_orderDetail reloadData];
    [self.tableView_pendingOrders reloadData];
    
    [self setupAndManageControllersUI];
}

-(void)setupAndManageControllersUI {
    
    BOOL isPruductAvailable = NO;
    long double  totalPrice = 0.0f;
    long long totalQty = 0;
    [self.label_POSCustomer setHidden:YES];
    [self.view_nonPosCustomer setHidden:YES];
    [self.button_clearAllUnpaidOrders setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.button_clearAllUnpaidOrders setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
    
    switch (self.segment_pendingOrder.selectedSegmentIndex) {
            
        case 0:  {
            //Local
            [self.button_clearAllUnpaidOrders setTitle:@"Remove Orders from POS" forState:UIControlStateNormal];
            
            isPruductAvailable = (self.array_pendingOrderData.count > 0);
            
            for (TBL_Product  *product in self.array_pendingOrderData) {
                if (product.shouldPayFor) {
                    
                    /*
                     Ref: Mail (Thu, Jan 15, 2015 at 9:29 AM)
                     2)      On Pending Orders screen, if OverridePrice > 0 then it should be used instead of the regular price to calculate the value under Price and the Item Subtotal at the bottom.
                     */
                    if ([product.priceOverride doubleValue] > 0)
                        totalPrice += [product.productQuantity longLongValue] * [product.priceOverride doubleValue];
                    else
                        totalPrice += [product.productQuantity longLongValue] * [product.productUnitPrice doubleValue];
                    
                    totalQty += [product.productQuantity longLongValue];
                }
                else if (product.isRefunded) {
                    if ([product.priceOverride doubleValue] > 0)
                        totalPrice -= [product.productQuantity longLongValue] * [product.priceOverride doubleValue];
                    else
                        totalPrice -= [product.productQuantity longLongValue] * [product.productUnitPrice doubleValue];
                }
            }
            
            if (self.userSelectedPendingOrder) {
                BOOL isDefaultCustomer = [self.userSelectedPendingOrder.userOrderToCustomer.customerFullName isEqualToString:kDefaultcustomer];
                [self.label_POSCustomer setHidden:!isDefaultCustomer];
                [self.view_nonPosCustomer setHidden:isDefaultCustomer];
            }
            
            self.userSelectedPendingOrder.orderItemQty = [NSString stringWithFormat:@"%lld",totalQty];
            self.userSelectedPendingOrder.orderTotalPrice = [NSString stringWithFormat:@"%Lf",totalPrice];
            
            [self.button_clearAllUnpaidOrders setEnabled:(self.array_pendingOrders_Local.count > 0)];
            
            [self.button_checkOut setEnabled:isPruductAvailable];
            [self.button_delete setEnabled:isPruductAvailable];
            [self.button_addToOrder setEnabled:isPruductAvailable];
            [self.button_saveOrder setEnabled:isPruductAvailable];
            [self.button_sendTo setEnabled:isPruductAvailable];
        }
            break;
            
        case 1: {
            //Shared
            [self.button_clearAllUnpaidOrders setTitle:@"Close Open Order" forState:UIControlStateNormal];
            BOOL canCheckout = NO;
            BOOL candeleteOrder = NO;
            if (selectedSharedOrderIndex >= 0) {
                OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                
                for (SelectedInventoryItem *inventory in orderItem.array_selectedInventory) {
                    
                    //if there is any inventory that can not be refunded (either it is already refunded, OR its a new item); order can be CheckedOut.
                    if (!inventory.canRefund) canCheckout = YES;
                    
                    if (inventory.isRefunded) {
                        totalPrice -= fabs([inventory.Quantity longLongValue] * [inventory.UnitPrice doubleValue]);
                    }
                    else if (inventory.shouldPayFor) {
                        totalPrice += [inventory.Quantity longLongValue] * [inventory.UnitPrice doubleValue];
                        totalQty += [inventory.Quantity longLongValue];
                    }
                }
                
                candeleteOrder = ((selectedSharedOrderIndex >= 0) && ![orderItem.Status isIdenticaleTo:@"Open"]);
                
                BOOL isDefaultCustomer = [orderItem.CustomerName isEqualToString:kDefaultcustomer];
                [self.label_POSCustomer setHidden:!isDefaultCustomer];
                [self.view_nonPosCustomer setHidden:isDefaultCustomer];
            }
            
            if ((self.array_pendingOrders_Shared.count > 0) && (selectedSharedOrderIndex >= 0)) {
                
                OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                [self.button_clearAllUnpaidOrders setEnabled:[orderItem.Status isIdenticaleTo:@"Open"]];
            }
            else {
                [self.button_clearAllUnpaidOrders setEnabled:NO];
            }
                
            [self.button_checkOut setEnabled:canCheckout];
            [self.button_delete setEnabled:candeleteOrder];
            [self.button_addToOrder setEnabled:(selectedSharedOrderIndex >= 0)];
            [self.button_saveOrder setEnabled:canCheckout];
            [self.button_sendTo setEnabled:canCheckout];
        }
            break;
            
        default:
            break;
    }
    
    self.label_totalAmount.text = [NSString stringWithFormat:@"$%.2Lf",totalPrice];
    self.label_totalQuantity.text = [NSString stringWithFormat:@"%lld",totalQty];
}

#pragma mark - * * * * IBAction * * * *

- (IBAction)CommonButtonAction:(id)sender {
    
    NSInteger index= [sender tag];
    [self.view endEditing:YES];
    
    switch (index) {
            
        case 57: {
            //Check out
            
            if ([[[AppUserData sharedInstance] selectedLocation] enablecashdrawerfunctions]) {
                NSString *assignedCashDrawer = [[NSUserDefaults standardUserDefaults] valueForKey:kCashDrawerId];
                BOOL canCompleteOrder = (assignedCashDrawer && [assignedCashDrawer length]>0);
                if (canCompleteOrder) [self makeWebAPICallTocheckOutIsForSave:NO];
                else [APPDELEGATE displayAlertWithTitle:@"" andMessage:kErrorMessage_NoDrawerAssigned];
            }else {
                [self makeWebAPICallTocheckOutIsForSave:NO];
            }

        }
            break;
            
        case 58: {
            //Delete Order
            switch (self.segment_pendingOrder.selectedSegmentIndex) {
                    
                case 0:  {
                    //Local
                    
                    if ([self isLoggedInUsersOrder] || [[[[AppUserData sharedInstance] loggedInUser] userType] isIdenticaleTo:@"manager"]) {
                        
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Are you sure you want to delete this order?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
                        alert.tag = 9;
                        [alert show];
                        
                        [APPDELEGATE setVisibleAlertView:alert];
                        [APPDELEGATE setDismissButtonIndex:1];
                    }
                    else {
                        [APPDELEGATE displayAlertWithTitle:@"Sorry!" andMessage:kErrorMessage_otherUserOrder];
                    }
                }
                    break;
                    
                case 1: {
                    //Shared
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Are you sure you want to delete this order?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES",@"NO", nil];
                    alert.tag = 9;
                    [alert show];
                    
                    [APPDELEGATE setVisibleAlertView:alert];
                    [APPDELEGATE setDismissButtonIndex:1];
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case 59: {
            //Add To Order Button
            isForAddToOrder = YES;
            switch (self.segment_pendingOrder.selectedSegmentIndex) {
                    
                case 0:  {
                    //Local
                    
                    //if its not loggedin user's order, remove it from other user's order and add it to Logged in user's order
                    if (![self isLoggedInUsersOrder])
                        [[DBHelper instance] shiftOrder:self.userSelectedPendingOrder toUser:self.appUser.loggedInUser];
                    
                    [[APPDELEGATE baseController] modifyOrderWithOrder:self.userSelectedPendingOrder.orderID];
                }
                    break;
                    
                case 1: {
                    //Shared
                    if (selectedSharedOrderIndex >= 0) {
                        
                        OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                        if ([orderItem.recalculate integerValue] == 1) {
                            
                            BOOL canCheckout = NO;
                            for (SelectedInventoryItem *inventory in orderItem.array_selectedInventory) {
                                if (!inventory.canRefund) {
                                    canCheckout = YES;
                                    break;
                                }
                            }
                            
                            if (canCheckout) {
                                [self makeWebAPICallTocheckOutIsForSave:YES];
                            }
                            else {
                                NSString*newOrderID =[[DBHelper instance] makeNewEntryForOrder:orderItem withRecalculateStatus:orderItem.recalculate forUser:[[AppUserData sharedInstance] loggedInUser] andIsForSWAP:NO isFromPendingOrder:YES];
                                [[APPDELEGATE baseController] modifyOrderWithOrder:newOrderID];
                            }
                        }
                        else {
                            NSString*newOrderID =[[DBHelper instance] makeNewEntryForOrder:orderItem withRecalculateStatus:orderItem.recalculate forUser:[[AppUserData sharedInstance] loggedInUser] andIsForSWAP:NO isFromPendingOrder:YES];
                            [[APPDELEGATE baseController] modifyOrderWithOrder:newOrderID];
                        }
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case 60: {
            //Clear All Unpaid Orders
            
            switch (self.segment_pendingOrder.selectedSegmentIndex) {
                case 0: {
                    //Local
                    if (self.array_pendingOrders_Local.count > 0) {
                        [self showConfirmationToDeleteAllUnpaidOrders];
                    }
                    else {
                        [APPDELEGATE displayAlertWithTitle:@"" andMessage:@"No unpaid order to remove."];
                    }
                }
                    break;
                    
                case 1: {
                    //Shared
                    
                    if (self.array_pendingOrders_Shared.count > 0) {
                        if ((([[[AppUserData sharedInstance] selectedLocation] managerapproveclearunpaid] == NO) || [[[[AppUserData sharedInstance] loggedInUser] userType] isIdenticaleTo:@"manager"])) {
                            
                            [self showConfirmationToDeleteAllUnpaidOrders];
                        }
                        else {
                            [[EnterPINViewController sharedInstance] getManegerialAccess:^(NSDictionary *dict, NSString *error) {
                                if (error)
                                    [APPDELEGATE displayAlertWithTitle:@"" andMessage:error];
                                else if (dict) {
                                    
                                    [self showConfirmationToDeleteAllUnpaidOrders];
                                }
                            }];
                        }
                    }
                    else {
                        [APPDELEGATE displayAlertWithTitle:@"" andMessage:@"No unpaid order to remove."];
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case 61: {
            //Save Order
            
            [self makeWebAPICallTocheckOutIsForSave:YES];
        }
            break;
            
        default:
            break;
    }
}

- (IBAction)segmentSelected:(UISegmentedControl *)sender {
    
    if ((self.segment_pendingOrder.selectedSegmentIndex == 0) && (selectedSharedOrderIndex >= 0)) {
        [self.segment_pendingOrder setSelectedSegmentIndex:1];
        
        OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
        if ([orderItem.recalculate integerValue] == 1) {
            BOOL canCheckout = NO;
            for (SelectedInventoryItem *inventory in orderItem.array_selectedInventory) {
                if (!inventory.canRefund) {
                    canCheckout = YES;
                    break;
                }
            }
            
            if (canCheckout) {
                UIAlertView * confirmation = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Would you like to save your changes to Order#: %@?",orderItem.orderID] message:@"" cancelButtonTitle:nil otherButtonTitles:[NSArray arrayWithObjects:@"YES", @"NO",nil] onCancel:^{
                    
                } onDismiss:^(NSInteger buttonIndex) {
                    switch (buttonIndex) {
                        case 0: {
                            //NO
                            orderItem.recalculate = @"0";
                            [self.segment_pendingOrder setSelectedSegmentIndex:0];
                            [self refreshPendingOrdersData];
                        }
                            break;
                            
                        case -1: {
                            //YES
                            [self makeWebAPICallTocheckOutIsForSave:YES];
                        }
                            break;
                            
                        default:
                            break;
                    }
                }];
                
                [confirmation show];
                [APPDELEGATE setVisibleAlertView:confirmation];
                [APPDELEGATE setDismissButtonIndex:0];
            }
            else {
                orderItem.recalculate = @"0";
                [self.segment_pendingOrder setSelectedSegmentIndex:0];
                [self refreshPendingOrdersData];
            }
        }
        else {
            orderItem.recalculate = @"0";
            [self.segment_pendingOrder setSelectedSegmentIndex:0];
            [self refreshPendingOrdersData];
        }
    }
    else
        [self refreshPendingOrdersData];
}

- (IBAction)sendToButtonAction:(id)sender {
    
    isForSendItems = YES;
    RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
    objHandler.delegate = self;
    switch (self.segment_pendingOrder.selectedSegmentIndex) {
        case 0: {
            //Local
            [objHandler call_SOAP_POSTMethodWithRequest:[SoapEnvelope calculateOrderForCheckOut:self.userSelectedPendingOrder withGiftCardData:nil withGiftCardAmount:@""] andMethodName:calculateForCheckOut andController:[APPDELEGATE navigationController]];
        }
            break;
           
        case 1: {
            // Shared
            OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
            NSString*newOrderID = [[DBHelper instance] makeNewEntryForOrder:orderItem withRecalculateStatus:orderItem.recalculate forUser:[[AppUserData sharedInstance] loggedInUser] andIsForSWAP:NO isFromPendingOrder:YES];
            [objHandler call_SOAP_POSTMethodWithRequest:[SoapEnvelope calculateOrderForCheckOut:[[[DBHelper instance] getOrderWithOrderId:newOrderID forUser:[[AppUserData sharedInstance] loggedInUser]] firstObject] withGiftCardData:nil withGiftCardAmount:@""] andMethodName:calculateForCheckOut andController:[APPDELEGATE navigationController]];
            
        }
            break;

        default:
            break;
    }
}

-(void)showConfirmationToDeleteAllUnpaidOrders {
    
    UIAlertView * confirmation = [[UIAlertView alloc] initWithTitle:((self.segment_pendingOrder.selectedSegmentIndex == 0) ? @"Are you sure you want to delete all unpaid orders?" : @"Are you sure you want to close this open order?") message:@"" cancelButtonTitle:nil otherButtonTitles:[NSArray arrayWithObjects:@"YES", @"NO", nil] onCancel:^{
        
    } onDismiss:^(NSInteger buttonIndex) {
        
        switch (buttonIndex) {
            case 0: {
                //NO
                
            }
                break;
                
            case -1: {
                [self deleteAllUnpaidOrders];
            }
                break;
                
            default:
                break;
        }
    }];
    
    [confirmation show];
    [APPDELEGATE setVisibleAlertView:confirmation];
    [APPDELEGATE setDismissButtonIndex:0];
}

-(void)deleteAllUnpaidOrders {
    
    switch (self.segment_pendingOrder.selectedSegmentIndex) {
            
        case 0:  {
            //Local
            if (self.array_pendingOrders_Local.count > 0) {
                
                currentOrderToBeDeleted = self.array_pendingOrders_Local.count-1;
                isDeleteAllOrder = YES;
                
                TBL_UserOrder *pendingOrder = [self.array_pendingOrders_Local objectAtIndex:currentOrderToBeDeleted];
                
                if ([pendingOrder.orderID rangeOfString:@"TA" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                    
                    RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
                    objHandler.delegate = self;
                    [objHandler call_SOAP_POSTMethodWithRequest:[SoapEnvelope getSOAPRequestToDeleteOrder:pendingOrder.orderID] andMethodName:deleteOrder andController:[APPDELEGATE navigationController]];
                }
                else {
                    
                    [[DBHelper instance] deleteOrder:pendingOrder forUser:self.appUser.loggedInUser];
                    [self refreshPendingOrdersData];
                    
                    [self deleteAllUnpaidOrders];
                }
            }
            else {
                isDeleteAllOrder = NO;
                currentOrderToBeDeleted = -1;
                
                [self refreshPendingOrdersData];
            }
        }
            break;
            
        case 1: {
            //Shared
            if ((self.array_pendingOrders_Shared.count > 0) && (selectedSharedOrderIndex >= 0)) {
                
                OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                isDeleteAllOrder = NO;
                RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
                objHandler.delegate = self;
                [objHandler call_SOAP_POSTMethodWithRequest:[SoapEnvelope closeOpenOrderWithID:orderItem.orderID] andMethodName:closeOpenOrder andController:[APPDELEGATE navigationController]];
            }
        }
            break;
            
        default:
            break;
    }
}

-(BOOL)isLoggedInUsersOrder {
    
    //#warning - need to update the logic!! Sometimes the TBL_User object become 'nil' while getting it from the TBL_Order obj
    
    return [self.userSelectedPendingOrder.userOrderToAppUser isEqual:self.appUser.loggedInUser];
}

#pragma mark - * * * * Custome TableCell Button action * * * *
-(void)deleteProduct:(id)sender {
    [self.view endEditing:YES];
    
    switch (self.segment_pendingOrder.selectedSegmentIndex) {
            
        case 0:  {
            
            //Local
            if ([self isLoggedInUsersOrder]) {
                
                selectedInventoryIndexToBeDeleted = [sender tag];
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete this item?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES", @"NO", nil];
                alert.tag= 19;
                [alert show];
                
                [APPDELEGATE setVisibleAlertView:alert];
                [APPDELEGATE setDismissButtonIndex:1];
            }
            else {
                [APPDELEGATE displayAlertWithTitle:@"Sorry!" andMessage:kErrorMessage_otherUserOrder];
            }
        }
            break;
            
        case 1: {
            //Shared
            selectedInventoryIndexToBeDeleted = [sender tag];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete this item?" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"YES", @"NO", nil];
            alert.tag= 19;
            [alert show];
            
            [APPDELEGATE setVisibleAlertView:alert];
            [APPDELEGATE setDismissButtonIndex:1];
        }
            break;
            
        default:
            break;
    }
}

-(void)increaseProductCount :(id)sender {
    
    [self.view endEditing:YES];
    switch (self.segment_pendingOrder.selectedSegmentIndex) {
            
        case 0:  {
            //Local
            if ([self isLoggedInUsersOrder]) {
                
                TBL_Product*product = [self.array_pendingOrderData objectAtIndex:[sender tag]];
                if ([product.productQuantity integerValue] < [product.onHand integerValue]) {
                    product.productQuantity = [NSString stringWithFormat:@"%d",(int)([product.productQuantity integerValue]+1)];
                    [self.userSelectedPendingOrder setRecalculateStatus:@"1"];
                    [self refreshUserPendingOrderProducts];
                }
                else {
                    [APPDELEGATE displayAlertWithTitle:@"" andMessage:(product.isRefunded ? @"Maximum refund quantity reached." : @"Maximum quantity reached.")];
                }
            }
            else {
                [APPDELEGATE displayAlertWithTitle:@"Sorry!" andMessage:kErrorMessage_otherUserOrder];
            }
        }
            break;
            
        case 1: {
            //Shared
            OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
            SelectedInventoryItem *inventory = [orderItem.array_selectedInventory objectAtIndex:[sender tag]];
            if ([inventory.Quantity integerValue] < [inventory.onhand integerValue]) {
                inventory.Quantity = [NSString stringWithFormat:@"%d",(int)([inventory.Quantity integerValue]+1)];
                orderItem.recalculate = @"1";
                [self refreshUserPendingOrderProducts];
            }
            else {
                [APPDELEGATE displayAlertWithTitle:@"" andMessage:(inventory.isRefunded ? @"Maximum refund quantity reached." : @"Maximum quantity reached.")];
            }
        }
            break;
            
        default:
            break;
    }
}

-(void)reduceProductCount :(id)sender {
    [self.view endEditing:YES];
    
    switch (self.segment_pendingOrder.selectedSegmentIndex) {
            
        case 0:  {
            //Local
            
            if ([self isLoggedInUsersOrder]) {
                
                TBL_Product*product = [self.array_pendingOrderData objectAtIndex:[sender tag]];
                if ([product.productQuantity integerValue] == 1)
                    [[DBHelper instance] deleteProduct:product fromOrder:self.userSelectedPendingOrder];
                else
                    product.productQuantity = [NSString stringWithFormat:@"%d",(int)([product.productQuantity integerValue]-1)];
                [self.userSelectedPendingOrder setRecalculateStatus:@"1"];
                [self refreshUserPendingOrderProducts];
                
                //if invetntory is blank now, delete the order from server
                if ([self.array_pendingOrderData count] == 0) {
                    if ([self.userSelectedPendingOrder.orderID rangeOfString:@"TA" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                        
                        isDeleteAllOrder = NO;
                        RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
                        objHandler.delegate = self;
                        [objHandler call_SOAP_POSTMethodWithRequest:[SoapEnvelope getSOAPRequestToDeleteOrder:self.userSelectedPendingOrder.orderID] andMethodName:deleteOrder andController:[APPDELEGATE navigationController]];
                    }
                    else {
                        [[DBHelper instance] deleteOrder:self.userSelectedPendingOrder forUser:self.appUser.loggedInUser];
                        [self refreshPendingOrdersData];
                    }
                }
            }
            else {
                [APPDELEGATE displayAlertWithTitle:@"Sorry!" andMessage:kErrorMessage_otherUserOrder];
            }
        }
            break;
            
        case 1: {
            //Shared
            OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
            SelectedInventoryItem *inventory = [orderItem.array_selectedInventory objectAtIndex:[sender tag]];
            orderItem.recalculate = @"1";

            if ([inventory.Quantity integerValue] > 1)
                inventory.Quantity = [NSString stringWithFormat:@"%d",(int)([inventory.Quantity integerValue]-1)];
            
            [self refreshUserPendingOrderProducts];
        }
            break;
            
        default:
            break;
    }
}

-(void)refundProductItem :(id)sender {
    [self.view endEditing:YES];
    switch (self.segment_pendingOrder.selectedSegmentIndex) {
            
        case 0:  {
            //Local
            
            if ([self isLoggedInUsersOrder]) {
                
                TBL_Product*product = [self.array_pendingOrderData objectAtIndex:[sender tag]];
                if (![[DBHelper instance] insertNewRefundItemTo:self.userSelectedPendingOrder withProduct:product]) {
                    [APPDELEGATE displayAlertWithTitle:@"" andMessage:@"Maximum refund quantity reached."];
                }
                [self.userSelectedPendingOrder setRecalculateStatus:@"1"];
                
                [self refreshUserPendingOrderProducts];
            }
            else {
                [APPDELEGATE displayAlertWithTitle:@"Sorry!" andMessage:kErrorMessage_otherUserOrder];
            }
        }
            break;
            
        case 1: {
            //Shared
            OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
            SelectedInventoryItem *inventory = [orderItem.array_selectedInventory objectAtIndex:[sender tag]];
            
            if (inventory.canRefund) {
                
                BOOL isInventoryRefunded = NO;
                
                NSArray *products = [orderItem.array_selectedInventory filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.InventoryStockId == %@ && SELF.canRefund == NO",inventory.InventoryStockId]];
                SelectedInventoryItem *searchedProduct = [products firstObject];
                
                if (searchedProduct) {
                    if ([searchedProduct.Quantity integerValue] < [searchedProduct.onhand integerValue]) {
                        searchedProduct.Quantity = [NSString stringWithFormat:@"%ld",((long)[searchedProduct.Quantity integerValue]+1)];
                        searchedProduct.canRefund = NO;
                        searchedProduct.isRefunded = YES;
                        searchedProduct.shouldPayFor = NO;
                        isInventoryRefunded = YES;
                        orderItem.recalculate = @"1";

                    }
                }
                else {
                    
                    SelectedInventoryItem *newInventory = [[SelectedInventoryItem alloc] init];
                    newInventory.canRefund = NO;
                    newInventory.Quantity = [NSString stringWithFormat:@"%ld",(long)([newInventory.Quantity integerValue]+1)];
                    newInventory.isRefunded = YES;
                    newInventory.shouldPayFor = NO;
                    
                    newInventory.SelectionId = inventory.SelectionId;
                    newInventory.TransactionId = inventory.TransactionId;
                    newInventory.CustomerId = inventory.CustomerId;
                    newInventory.InventoryStockId = inventory.InventoryStockId;
                    newInventory.Label = inventory.Label;
                    newInventory.Name = inventory.Name;
                    newInventory.inventoryDescription = inventory.inventoryDescription;
                    newInventory.UnitPrice = inventory.UnitPrice;
                    newInventory.Price = inventory.Price;
                    newInventory.DiscountRate = inventory.DiscountRate;
                    newInventory.DiscountAmount = inventory.DiscountAmount;
                    newInventory.SellingPrice = inventory.SellingPrice;
                    newInventory.SalesTaxTate = inventory.SalesTaxTate;
                    newInventory.SalesTax = inventory.SalesTax;
                    newInventory.onhand = inventory.onhand;
                    newInventory.ornorder = inventory.ornorder;
                    newInventory.available = inventory.available;
                    newInventory.allocated = inventory.allocated;
                    newInventory.POSItemDiscount = inventory.POSItemDiscount;
                    newInventory.POSPriceOverride = inventory.POSPriceOverride;
                    newInventory.POSRefund = inventory.POSRefund;
                    newInventory.onhand = inventory.Quantity;
                    
                    [orderItem.array_selectedInventory addObject:newInventory];
                    isInventoryRefunded = YES;
                    orderItem.recalculate = @"1";

                }
                
                if (!isInventoryRefunded) [APPDELEGATE displayAlertWithTitle:@"" andMessage:@"Maximum refund quantity reached."];
                
                [self refreshUserPendingOrderProducts];
            }
        }
            break;
            
        default:
            break;
    }
}

-(void)splitProductItem:(UIButton *)sender {
    [self.view endEditing:YES];
    selectedOrderIndex = [sender tag];
    switch (self.segment_pendingOrder.selectedSegmentIndex) {
            
        case 0:  {
            //Local
            
            if ([self isLoggedInUsersOrder]) {
                
                TBL_Product*product = [self.array_pendingOrderData objectAtIndex:[sender tag]];
                SelectedInventoryItem *inventory = [[SelectedInventoryItem alloc] init];
                inventory.Name = product.productName;
                inventory.Quantity = product.productQuantity;
                SplitItemsViewController *splitVC = [[SplitItemsViewController alloc] initWithNibName:@"SplitItemsViewController" bundle:nil];
                [splitVC setInventoryItem:inventory];
                [splitVC setDelegate:self];
                [splitVC setPreferredContentSize:CGSizeMake(537.0, 300.0)];
                [splitVC setModalPresentationStyle:UIModalPresentationFormSheet];
                [splitVC setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
                [self presentViewController:splitVC animated:YES completion:nil];
            }
            break;
            
        case 1: {
            //Shared
            OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
            SelectedInventoryItem *inventory = [orderItem.array_selectedInventory objectAtIndex:[sender tag]];
            SplitItemsViewController *splitVC = [[SplitItemsViewController alloc] initWithNibName:@"SplitItemsViewController" bundle:nil];
            [splitVC setInventoryItem:inventory];
            [splitVC setDelegate:self];
            [splitVC setPreferredContentSize:CGSizeMake(537.0, 300.0)];
            [splitVC setModalPresentationStyle:UIModalPresentationFormSheet];
            [splitVC setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
            [self presentViewController:splitVC animated:YES completion:nil];
        }
            break;
            
        default:
            break;
        }
    }
}


#pragma mark - SplitItemsDelegate method
-(void)splitSlectedItemsWithOldQuantity:(NSString *)oldQuantity andNewQuantity:(NSString *)newQuantity {
    
    switch (self.segment_pendingOrder.selectedSegmentIndex) {
        case 0: {
            // Local
            TBL_Product *productItem = [self.array_pendingOrderData objectAtIndex:selectedOrderIndex];
            productItem.productQuantity = oldQuantity;
            
            TBL_Product *objProducts = (TBL_Product *)[NSEntityDescription insertNewObjectForEntityForName:@"TBL_Product" inManagedObjectContext:[[DBHelper instance] managedObjectContext]];
            objProducts.productID = productItem.productID;
            objProducts.productName = productItem.productName;
            objProducts.productQuantity = newQuantity;
            objProducts.productUnitPrice = productItem.productUnitPrice;
            objProducts.isSwap = productItem.isSwap;
            objProducts.isRefunded = productItem.isRefunded;
            objProducts.shouldPayFor = productItem.shouldPayFor;
            objProducts.itemDiscount = productItem.itemDiscount;
            objProducts.inventryID = productItem.inventryID;
            objProducts.onHand = productItem.onHand;
            objProducts.stockValue = productItem.stockValue;
            objProducts.priceOverride = productItem.priceOverride;
            objProducts.isNewProductAdded = productItem.isNewProductAdded;
            objProducts.sequenceNumber = productItem.sequenceNumber;
            objProducts.productDetails = productItem.productDetails;
            [self.userSelectedPendingOrder addUserOrderToProductObject:objProducts];
            [self.array_pendingOrderData addObject:objProducts];
            selectedOrderIndex = -1;
            [[DBHelper instance] saveContext];
            self.userSelectedPendingOrder.recalculateStatus = @"1";
            [self.tableView_orderDetail reloadData];
        }
            break;
            
        case 1: {
            // Shared
            OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
            SelectedInventoryItem *inventory = [orderItem.array_selectedInventory objectAtIndex:selectedOrderIndex];
            inventory.Quantity = oldQuantity;
            
            SelectedInventoryItem *newInventory = [[SelectedInventoryItem alloc] init];
            newInventory.canRefund = [inventory canRefund];
            newInventory.Quantity = newQuantity;
            newInventory.isRefunded = [inventory isRefunded];
            newInventory.shouldPayFor = [inventory shouldPayFor];
            newInventory.SelectionId = nil;
            newInventory.TransactionId = inventory.TransactionId;
            newInventory.CustomerId = inventory.CustomerId;
            newInventory.InventoryStockId = inventory.InventoryStockId;
            newInventory.Label = inventory.Label;
            newInventory.Name = inventory.Name;
            newInventory.inventoryDescription = inventory.inventoryDescription;
            newInventory.UnitPrice = inventory.UnitPrice;
            newInventory.Price = inventory.Price;
            newInventory.DiscountRate = inventory.DiscountRate;
            newInventory.DiscountAmount = inventory.DiscountAmount;
            newInventory.SellingPrice = inventory.SellingPrice;
            newInventory.SalesTaxTate = inventory.SalesTaxTate;
            newInventory.SalesTax = inventory.SalesTax;
            newInventory.onhand = inventory.onhand;
            newInventory.ornorder = inventory.ornorder;
            newInventory.available = inventory.available;
            newInventory.allocated = inventory.allocated;
            newInventory.POSItemDiscount = inventory.POSItemDiscount;
            newInventory.POSPriceOverride = inventory.POSPriceOverride;
            newInventory.POSRefund = inventory.POSRefund;
            newInventory.onhand = inventory.Quantity;
            [orderItem.array_selectedInventory addObject:newInventory];
            selectedOrderIndex = -1;
            [self.tableView_orderDetail reloadData];
        }
            break;
        default:
            break;
    }
}


#pragma mark - * * * * UIAlertDelegate Methods * * * *
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        
        switch (alertView.tag) {
                
            case 19: {
                
                //Delete Item From Order
                switch (self.segment_pendingOrder.selectedSegmentIndex) {
                        
                    case 0:  {
                        //Local
                        
                        if ([self.array_pendingOrderData count] == 1) {
                            
                            if ([self.userSelectedPendingOrder.orderID rangeOfString:@"TA" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                                
                                isDeleteAllOrder = NO;
                                RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
                                objHandler.delegate = self;
                                [objHandler call_SOAP_POSTMethodWithRequest:[SoapEnvelope getSOAPRequestToDeleteOrder:self.userSelectedPendingOrder.orderID] andMethodName:deleteOrder andController:[APPDELEGATE navigationController]];
                            }
                            else {
                                [[DBHelper instance] deleteOrder:self.userSelectedPendingOrder forUser:self.appUser.loggedInUser];
                                [self refreshPendingOrdersData];
                            }
                        }
                        else {
                            [[DBHelper instance] deleteProduct:[self.array_pendingOrderData objectAtIndex:selectedInventoryIndexToBeDeleted] fromOrder:self.userSelectedPendingOrder];
                            [self.userSelectedPendingOrder setRecalculateStatus:@"1"];
                            [self refreshUserPendingOrderProducts];
                        }
                    }
                        break;
                        
                    case 1: {
                        //Shared
                        
                        OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                        orderItem.recalculate = @"1";
                        [orderItem.array_selectedInventory removeObjectAtIndex:selectedInventoryIndexToBeDeleted];
                        [self refreshUserPendingOrderProducts];
                    }
                        break;
                        
                    default:
                        break;
                }
            }
                break;
                
            case 9: {
                //Delete Order
                switch (self.segment_pendingOrder.selectedSegmentIndex) {
                        
                    case 0:  {
                        //Local
                        
                        if ([self.userSelectedPendingOrder.orderID rangeOfString:@"TA" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                            
                            isDeleteAllOrder = NO;
                            RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
                            objHandler.delegate = self;
                            [objHandler call_SOAP_POSTMethodWithRequest:[SoapEnvelope getSOAPRequestToDeleteOrder:self.userSelectedPendingOrder.orderID] andMethodName:deleteOrder andController:[APPDELEGATE navigationController]];
                        }
                        else {
                            [[DBHelper instance] deleteOrder:self.userSelectedPendingOrder forUser:self.appUser.loggedInUser];
                            [self refreshPendingOrdersData];
                            
                        }
                    }
                        break;
                        
                    case 1: {
                        //Shared
                        OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                        isDeleteAllOrder = NO;
                        RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
                        objHandler.delegate = self;
                        [objHandler call_SOAP_POSTMethodWithRequest:[SoapEnvelope getSOAPRequestToDeleteOrder:orderItem.orderID] andMethodName:deleteOrder andController:[APPDELEGATE navigationController]];
                    }
                        break;
                        
                    default:
                        break;
                }
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - * * * * UITableviewDelegateMethods * * * *

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numberOfRows = 0;
    switch (self.segment_pendingOrder.selectedSegmentIndex) {
            
        case 0:  {
            //Local
            
            if ([tableView isEqual:self.tableView_pendingOrders])
                numberOfRows = [self.array_pendingOrders_Local count];
            else
                numberOfRows = [self.array_pendingOrderData count];
        }
            break;
            
        case 1: {
            //Shared
            
            if ([tableView isEqual:self.tableView_pendingOrders])
                numberOfRows = [self.array_pendingOrders_Shared count];
            else if (selectedSharedOrderIndex >= 0)
                numberOfRows = [[(OrderItem*)[self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex] array_selectedInventory] count];
        }
            break;
            
        default:
            break;
    }
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([tableView isEqual:self.tableView_pendingOrders]) {
        
        static NSString *PendingPopoverCustomCellID= @"PendingPopoverCustomCellID";
        PendingPopoverCustomCell *pendingOrderCell = (PendingPopoverCustomCell *)[tableView dequeueReusableCellWithIdentifier:PendingPopoverCustomCellID];
        
        if(pendingOrderCell == nil) {
            
            static NSString *cellNib = @"PendingPopoverCustomCell" ;
            NSArray *topLevelObjectsProducts = [[NSBundle mainBundle] loadNibNamed:cellNib owner:self options:nil];
            pendingOrderCell = (PendingPopoverCustomCell *)[topLevelObjectsProducts objectAtIndex:0];
        }
        
        switch (self.segment_pendingOrder.selectedSegmentIndex) {
                
            case 0:  {
                //Local
                
                TBL_UserOrder *order = [self.array_pendingOrders_Local objectAtIndex:indexPath.row];
                
                [pendingOrderCell.labelUserName setText:order.userOrderToAppUser.userFullName];
                //When a TA####### is present we need to display that instead of Order #1, #2, etc. on the Pending Orders screen.
                if (order.orderID && (order.orderID.length > 0) && ([order.orderID rangeOfString:@"TA" options:NSCaseInsensitiveSearch].location != NSNotFound))
                    [pendingOrderCell.labelOrderNumber setText:[NSString stringWithFormat:@"%@",order.orderID]];
                else  if ((order.openOrderID && order.openOrderID.length >0) && ([order.openOrderID rangeOfString:@"TA" options:NSCaseInsensitiveSearch].location != NSNotFound))
                    [pendingOrderCell.labelOrderNumber setText:[NSString stringWithFormat:@"%@",order.openOrderID]];
                else
                    [pendingOrderCell.labelOrderNumber setText:[NSString stringWithFormat:@"Order #%@",order.orderNumber]];
                
                [pendingOrderCell.labelCustomer setText:order.userOrderToCustomer.customerFullName];
                [pendingOrderCell.labelCost setText:[NSString stringWithFormat:@"Total: $%0.2f",[order.orderTotalPrice doubleValue]]];
                
                UIColor *textColor = (([self.selectedPendingOrderID isEqualToString: order.orderID]) ? [UIColor whiteColor] : [UIColor blackColor]);
                pendingOrderCell.labelUserName.textColor = textColor;
                pendingOrderCell.labelOrderNumber.textColor = textColor;
                pendingOrderCell.labelCustomer.textColor = textColor;
                pendingOrderCell.labelCost.textColor = (([self.selectedPendingOrderID isEqualToString: order.orderID]) ? [UIColor whiteColor] : kAppThemeColor);
                
                if ([self.selectedPendingOrderID isEqualToString: order.orderID])
                    [pendingOrderCell.contentView setBackgroundColor:[UIColor colorWithRed:7.0/255.0 green:68.0/255.0 blue:208.0/255.0 alpha:1.0]];
                else
                    [pendingOrderCell.contentView setBackgroundColor:[UIColor colorWithRed:247.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0]];
            }
                break;
                
            case 1: {
                //Shared
                
                OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:indexPath.row];
                UIColor *textColor = ((indexPath.row == selectedSharedOrderIndex) ? [UIColor whiteColor] : (([orderItem.Status isIdenticaleTo:@"Open"]) ? kAppGreenColor : [UIColor blackColor]));
                
                pendingOrderCell.labelUserName.textColor = textColor;
                pendingOrderCell.labelOrderNumber.textColor = textColor;
                pendingOrderCell.labelCustomer.textColor = textColor;
                pendingOrderCell.labelCost.textColor = ((indexPath.row == selectedSharedOrderIndex) ? [UIColor whiteColor] : kAppThemeColor);
                
                [pendingOrderCell.labelUserName setText:orderItem.RepName];
                [pendingOrderCell.labelOrderNumber setText:orderItem.orderID];
                [pendingOrderCell.labelCustomer setText:orderItem.CustomerName];
                [pendingOrderCell.labelCost setText:[NSString stringWithFormat:@"Total: $%0.2f",[orderItem.InventorySubTotal doubleValue]]];
                
                if (indexPath.row == selectedSharedOrderIndex)
                    [pendingOrderCell.contentView setBackgroundColor:[UIColor colorWithRed:7.0/255.0 green:68.0/255.0 blue:208.0/255.0 alpha:1.0]];
                else
                    [pendingOrderCell.contentView setBackgroundColor:[UIColor colorWithRed:247.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0]];
            }
                break;
                
            default:
                break;
        }
        
        return pendingOrderCell;
    }
    else {
        
        static NSString *CellIdentifier=@"NewOrderTableViewCellID";
        NewOrderTableViewCell *orderCell = (NewOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if(orderCell == nil) {
            
            static NSString *cellNib = @"NewOrderTableViewCell" ;
            NSArray *topLevelObjectsProducts = [[NSBundle mainBundle] loadNibNamed:cellNib owner:self options:nil];
            orderCell = (NewOrderTableViewCell *)[topLevelObjectsProducts objectAtIndex:0];
            
            [orderCell.buttonDelete addTarget:self action:@selector(deleteProduct:) forControlEvents:UIControlEventTouchUpInside];
            [orderCell.buttonAdd addTarget:self action:@selector(increaseProductCount:) forControlEvents:UIControlEventTouchUpInside];
            [orderCell.buttonRemove addTarget:self action:@selector(reduceProductCount:) forControlEvents:UIControlEventTouchUpInside];
            [orderCell.button_refund addTarget:self action:@selector(refundProductItem:) forControlEvents:UIControlEventTouchUpInside];
            [orderCell.button_split addTarget:self action:@selector(splitProductItem:) forControlEvents:UIControlEventTouchUpInside];

        }
        
        UIColor *bgColo = ((indexPath.row%2)==0) ? kAppLightGrayColor : kAppGrayColor;
        [orderCell setBackgroundColor:bgColo];
        [orderCell.contentView setBackgroundColor:bgColo];
        
        orderCell.buttonAdd.tag = indexPath.row;
        orderCell.buttonRemove.tag = indexPath.row;
        orderCell.buttonDelete.tag = indexPath.row;
        orderCell.button_refund.tag = indexPath.row;
        orderCell.button_split.tag = indexPath.row;
        orderCell.textFieldQuantity.tag = indexPath.row+999;
        
        orderCell.textFieldQuantity.delegate=self;
        
        switch (self.segment_pendingOrder.selectedSegmentIndex) {
                
            case 0:  {
                //Local
                
                TBL_Product *productItem = [self.array_pendingOrderData objectAtIndex:indexPath.row];
                UIColor *textColor = (productItem.isSwap ? (productItem.isRefunded ? kAppBlueColor : kAppGreenColor) : [UIColor blackColor]);
                BOOL canEditProduct = (productItem.shouldPayFor || productItem.isRefunded);
                
                [orderCell.button_refund setHidden:canEditProduct];
                [orderCell.buttonAdd setHidden:!canEditProduct];
                [orderCell.buttonDelete setHidden:!canEditProduct];
                [orderCell.buttonRemove setHidden:!canEditProduct];
                orderCell.textFieldQuantity.enabled=canEditProduct;
                
                if ([productItem.productQuantity integerValue] > 1) {
                    [orderCell.button_split setHidden:!canEditProduct];
                }else {
                    [orderCell.button_split setHidden:YES];
                }
                
                [orderCell.labelProductName setTextColor:textColor];
                [orderCell.textFieldQuantity setTextColor:textColor];
                [orderCell.labelTotalCost setTextColor:textColor];
                
                //Point Number 3: Can we remove those numbers or are you using them for something. on 26 Feb, 2016
                orderCell.labelProductName.text = [NSString stringWithFormat:@"%@",productItem.productName];
                if (productItem.isRefunded)
                    orderCell.labelTotalCost.text = [NSString stringWithFormat:@"$-%.2f",[productItem.productUnitPrice doubleValue]*[productItem.productQuantity intValue]];
                else
                    orderCell.labelTotalCost.text = [NSString stringWithFormat:@"$%.2f",[productItem.productUnitPrice doubleValue]*[productItem.productQuantity intValue]];
                orderCell.textFieldQuantity.text = [NSString stringWithFormat:@"%@",productItem.productQuantity];
                [orderCell.buttonAdd setEnabled:([productItem.productQuantity integerValue] < [productItem.onHand integerValue])];
                [orderCell.buttonRemove setEnabled:([productItem.productQuantity integerValue] > 1)];
            }
                break;
                
            case 1: {
                //Shared
                OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                SelectedInventoryItem *inventory = [orderItem.array_selectedInventory objectAtIndex:indexPath.row];
                
                UIColor *textColor = (inventory.canRefund ? kAppGreenColor : (inventory.isRefunded && [orderItem.POSOpenOrderId length] ? kAppBlueColor : [UIColor blackColor]));
                BOOL canEditProduct = !inventory.canRefund;
                
                [orderCell.button_refund setHidden:canEditProduct];
                [orderCell.buttonAdd setHidden:!canEditProduct];
                [orderCell.buttonDelete setHidden:!canEditProduct];
                [orderCell.buttonRemove setHidden:!canEditProduct];
                orderCell.textFieldQuantity.enabled=canEditProduct;
                
                if ([inventory.Quantity integerValue] > 1) {
                    [orderCell.button_split setHidden:!canEditProduct];
                }else {
                    [orderCell.button_split setHidden:YES];
                }
                
                [orderCell.labelProductName setTextColor:textColor];
                [orderCell.textFieldQuantity setTextColor:textColor];
                [orderCell.labelTotalCost setTextColor:textColor];
                
                orderCell.labelProductName.text = inventory.Name;
                
                if (inventory.isRefunded)
                    orderCell.labelTotalCost.text = [NSString stringWithFormat:@"$-%.2f",fabs([inventory.UnitPrice doubleValue]*[inventory.Quantity doubleValue])];
                else
                    orderCell.labelTotalCost.text = [NSString stringWithFormat:@"$%.2f",fabs([inventory.UnitPrice doubleValue]*[inventory.Quantity doubleValue])];
                orderCell.textFieldQuantity.text = inventory.Quantity;
                [orderCell.buttonAdd setEnabled:([inventory.Quantity integerValue] < [inventory.onhand integerValue])];
                [orderCell.buttonRemove setEnabled:([inventory.Quantity integerValue] > 1)];
            }
                break;
                
            default:
                break;
        }
        
        return orderCell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([tableView isEqual:self.tableView_pendingOrders]) {
        [self.view endEditing:YES];
        
        switch (self.segment_pendingOrder.selectedSegmentIndex) {
                
            case 0:  {
                //Local
                
                self.userSelectedPendingOrder = [self.array_pendingOrders_Local objectAtIndex:indexPath.row];
                self.selectedPendingOrderID = self.userSelectedPendingOrder.orderID;
                [self refreshUserPendingOrderProducts];
            }
                break;
                
            case 1: {
                //Shared
                
                if (selectedSharedOrderIndex >= 0) {
                    OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                    if ([orderItem.recalculate integerValue] == 1) {
                        
                        BOOL canCheckout = NO;
                        for (SelectedInventoryItem *inventory in orderItem.array_selectedInventory) {
                            if (!inventory.canRefund) {
                                canCheckout = YES;
                                break;
                            }
                        }
                        
                        if (canCheckout) {
                            UIAlertView * confirmation = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Would you like to save your changes to Order#: %@?",orderItem.orderID] message:@"" cancelButtonTitle:nil otherButtonTitles:[NSArray arrayWithObjects:@"YES", @"NO",nil] onCancel:^{
                                
                            } onDismiss:^(NSInteger buttonIndex) {
                                switch (buttonIndex) {
                                    case 0: {
                                        //NO
                                        orderItem.recalculate = @"0";
                                        [self selectNewSharedpendingOrderAtindex:indexPath.row];
                                    }
                                        break;
                                        
                                    case -1: {
                                        //YES
                                        [self makeWebAPICallTocheckOutIsForSave:YES];
                                    }
                                        break;
                                        
                                    default:
                                        break;
                                }
                            }];
                            
                            [confirmation show];
                            [APPDELEGATE setVisibleAlertView:confirmation];
                            [APPDELEGATE setDismissButtonIndex:0];
                        }
                        else {
                            orderItem.recalculate = @"0";
                            [self selectNewSharedpendingOrderAtindex:indexPath.row];
                        }
                    }
                    else {
                        orderItem.recalculate = @"0";
                        [self selectNewSharedpendingOrderAtindex:indexPath.row];
                    }
                }
                else
                    [self selectNewSharedpendingOrderAtindex:indexPath.row];
            }
                break;
                
            default:
                break;
        }
    }else {
        switch (self.segment_pendingOrder.selectedSegmentIndex) {
            case 0: {
                TBL_Product *productItem = [self.array_pendingOrderData objectAtIndex:indexPath.row];
                selectedOrderIndex = indexPath.row;
                ItemDetailsViewController *controller = [[ItemDetailsViewController alloc] initWithNibName:@"ItemDetailsViewController" bundle:nil];
                controller.product = productItem;
                [controller setDelegate:self];
                [controller setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                [controller setModalPresentationStyle:UIModalPresentationFormSheet];
                [controller setPreferredContentSize:CGSizeMake(800, 670)];
                [self presentViewController:controller animated:YES completion:nil];
            }
                break;
                
            case 1: {
                selectedOrderIndex = indexPath.row;
                OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                SelectedInventoryItem *inventory = [orderItem.array_selectedInventory objectAtIndex:indexPath.row];
                ItemDetailsViewController *controller = [[ItemDetailsViewController alloc] initWithNibName:@"ItemDetailsViewController" bundle:nil];
                [controller setSelectedInventoryItem:inventory];
                [controller setDelegate:self];
                [controller setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                [controller setModalPresentationStyle:UIModalPresentationFormSheet];
                [controller setPreferredContentSize:CGSizeMake(800, 670)];
                [self presentViewController:controller animated:YES completion:nil];
                
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - * * * * ItemDetailsDelegate Methods* * * *
-(void)getNoteWith:(NSString *)strNote {
    
    switch (self.segment_pendingOrder.selectedSegmentIndex) {
        case 0: {
            if (strNote) {
                TBL_Product *productItem = [self.array_pendingOrderData objectAtIndex:selectedOrderIndex];
                [productItem setProductNote:strNote];
                [[DBHelper instance] saveContext];
            }
            selectedOrderIndex = -1;
        }
            break;
            
        case 1: {
            if (strNote) {
                OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                SelectedInventoryItem *inventory = [orderItem.array_selectedInventory objectAtIndex:selectedOrderIndex];
                [inventory setNotes:strNote];
            }
            selectedOrderIndex = -1;
        }
            break;
        default:
            break;
    }
    
}

-(void)selectNewSharedpendingOrderAtindex:(NSInteger)index {
    selectedSharedOrderIndex = index;
    
    OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
    RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
    objHandler.delegate = self;
    [objHandler call_SOAP_POSTMethodWithRequest:[SoapEnvelope getSOAPRequestForOrderDetailsForOrder:orderItem.orderID isFromPendingOrder:@"1"] andMethodName:getOrderDetails andController:[APPDELEGATE navigationController]];
}

#pragma mark - * * * * UITableViewDelegate Method * * * *
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return (([tableView isEqual:self.tableView_pendingOrders]) ? 62.0 : 46.0);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectZero];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectZero];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

#pragma  mark - * * * * UITextFieldDelegate Methods* * * *
-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    if([textField.text isVAlidNumberFormat]) {
        
        switch (self.segment_pendingOrder.selectedSegmentIndex) {
                
            case 0:  {
                //Local
                
                TBL_Product *productItem = [self.array_pendingOrderData objectAtIndex:(textField.tag-999)];
                productItem.productQuantity = [NSString stringWithFormat:@"%ld",(long)[textField.text integerValue]];
                [self.userSelectedPendingOrder setRecalculateStatus:@"1"];
                
                [self refreshUserPendingOrderProducts];
            }
                break;
                
            case 1: {
                //Shared
                
                OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                SelectedInventoryItem *inventory = [orderItem.array_selectedInventory objectAtIndex:(textField.tag-999)];
                inventory.Quantity = [NSString stringWithFormat:@"%ld",(long)[textField.text integerValue]];
                [self refreshUserPendingOrderProducts];
            }
                break;
                
            default:
                break;
        }
        
    }
    else
        [APPDELEGATE displayAlertWithTitle:@"Error!" andMessage:@"Number only."];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *newTextToBe =  [textField.text stringByReplacingCharactersInRange:range withString:string];
    BOOL shouldEdit = NO;
    
    if (textField.tag >= 999) {
        switch (self.segment_pendingOrder.selectedSegmentIndex) {
                
            case 0:  {
                //Local
                
                TBL_Product *productItem = [self.array_pendingOrderData objectAtIndex:textField.tag-999];
                shouldEdit = ([newTextToBe isVAlidNumberFormat] && ([newTextToBe integerValue]>= 0) && ([newTextToBe integerValue] <= [productItem.onHand integerValue]));
                
                if (shouldEdit) [self.userSelectedPendingOrder setRecalculateStatus:@"1"];
            }
                break;
                
            case 1: {
                //Shared
                
                OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                SelectedInventoryItem *inventory = [orderItem.array_selectedInventory objectAtIndex:(textField.tag-999)];
                shouldEdit = ([newTextToBe isVAlidNumberFormat] && ([newTextToBe integerValue]>= 0) && ([newTextToBe integerValue] <= [inventory.onhand integerValue]));
                
                if (shouldEdit) {
                    orderItem.recalculate = @"1";
                    inventory.Quantity = newTextToBe;
                }
            }
                break;
                
            default:
                break;
        }
        
        return shouldEdit;
    }
    
    return (([newTextToBe length] < 10) && [newTextToBe isVAlidNumberFormat]);
}

#pragma mark - * * * * Webservices Calling * * * *
-(void)makeWebAPICallTocheckOutIsForSave:(BOOL)forSave  {
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"POS_Station"] length] < 1) {
        [APPDELEGATE displayAlertWithTitle:@"" andMessage:kErrorMessage_NoPOSStation];
        return;
    }
    
    switch (self.segment_pendingOrder.selectedSegmentIndex) {
            
        case 0:  {
            //Local
            isForSaveOrder = forSave;
            RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
            objHandler.delegate = self;
            [objHandler call_SOAP_POSTMethodWithRequest:[SoapEnvelope calculateOrderForCheckOut:self.userSelectedPendingOrder withGiftCardData:nil withGiftCardAmount:@""] andMethodName:calculateForCheckOut andController:[APPDELEGATE navigationController]];
        }
            break;
            
        case 1: {
            //Shared
            if (selectedSharedOrderIndex >= 0) {
                OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];

                if (forSave) {
                    if ([orderItem.array_selectedInventory count] > 0) {
                        //save order
                        isForSaveOrder = forSave;
                        OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                        NSString*newOrderID = [[DBHelper instance] makeNewEntryForOrder:orderItem withRecalculateStatus:orderItem.recalculate forUser:[[AppUserData sharedInstance] loggedInUser] andIsForSWAP:NO isFromPendingOrder:YES];
                        RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
                        objHandler.delegate = self;
                        [objHandler call_SOAP_POSTMethodWithRequest:[SoapEnvelope calculateOrderForCheckOut:[[[DBHelper instance] getOrderWithOrderId:newOrderID forUser:[[AppUserData sharedInstance] loggedInUser]] firstObject] withGiftCardData:nil withGiftCardAmount:@""] andMethodName:calculateForCheckOut andController:[APPDELEGATE navigationController]];
                    }
                    else {
                        //Delete Order
                        
                        OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                        isDeleteAllOrder = NO;
                        RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
                        objHandler.delegate = self;
                        [objHandler call_SOAP_POSTMethodWithRequest:[SoapEnvelope getSOAPRequestToDeleteOrder:orderItem.orderID] andMethodName:deleteOrder andController:[APPDELEGATE navigationController]];
                    }
                }
                else {
                    BOOL canCheckout = NO;
                    for (SelectedInventoryItem *inventory in orderItem.array_selectedInventory) {
                        if (!inventory.canRefund) {
                            canCheckout = YES;
                            break;
                        }
                    }
                    
                    if (canCheckout) {
                        
                        isForSaveOrder = forSave;
                        OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                        NSString*newOrderID = [[DBHelper instance] makeNewEntryForOrder:orderItem withRecalculateStatus:orderItem.recalculate forUser:[[AppUserData sharedInstance] loggedInUser] andIsForSWAP:NO isFromPendingOrder:YES];
                        RequestResponseHandler  *objHandler = [[RequestResponseHandler alloc] init];
                        objHandler.delegate = self;
                        [objHandler call_SOAP_POSTMethodWithRequest:[SoapEnvelope calculateOrderForCheckOut:[[[DBHelper instance] getOrderWithOrderId:newOrderID forUser:[[AppUserData sharedInstance] loggedInUser]] firstObject] withGiftCardData:nil withGiftCardAmount:@""] andMethodName:calculateForCheckOut andController:[APPDELEGATE navigationController]];
                    }
                    else {
                        //No product available for checkout
                    }
                }
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - * * * * RequestResponseHandlerDelegate * * * *
-(void)ResponseContentFromServer:(RequestResponseHandler *)responseObj withData:(id)jsonData forMethod:(WebMethodType)methodType {
    
    if ([responseObj.responseCode intValue] == SERVER_RESPONSE_SUCCESS_CODE) {
        
        switch (methodType) {
                
            case getPendingOrders: {
                
                [self.array_pendingOrders_Shared removeAllObjects];
                
                NSArray *orderList = [[jsonData objectForKey:@"OrderArray"] objectForKey:@"Order"];
                if (orderList && [orderList isKindOfClass:[NSDictionary class]]) {
                    [self.array_pendingOrders_Shared addObject:[OrderItem getOrderItemFrom:(NSDictionary*)orderList]];
                }
                else {
                    for (NSDictionary *item in orderList)
                        [self.array_pendingOrders_Shared addObject:[OrderItem getOrderItemFrom:item]];
                }
                
                [self.tableView_pendingOrders reloadData];
            }
                break;
                
            case getOrderDetails: {
                
                OrderItem *selectedOrder;
                if (selectedSharedOrderIndex >= 0)
                    selectedOrder = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                NSDictionary *orederItem = [jsonData objectForKey:@"OrderDetail"];
                [OrderItem updateOrderItem:selectedOrder with:orederItem];
                
                for (SelectedInventoryItem*inventory in selectedOrder.array_selectedInventory) {
                    if ([selectedOrder.Status isIdenticaleTo:@"Open"] || [inventory.TransactionId isEqualToString:selectedOrder.POSOpenOrderId]) {
                        //GREEN
                        inventory.canRefund = YES;
                        inventory.isRefunded = NO;
                        inventory.shouldPayFor = NO;
                    }
                    else if ([inventory.TransactionId isEqualToString:selectedOrder.orderID] && ([inventory.UnitPrice doubleValue] < 0)) {
                        //BLUE
                        inventory.canRefund = NO;
                        inventory.isRefunded = YES;
                        inventory.shouldPayFor = NO;
                    }
                    else {
                        //BLACK
                        inventory.canRefund = NO;
                        inventory.isRefunded = NO;
                        inventory.shouldPayFor = YES;
                    }
                }
                
                if (isForSendItems) {
                    
                    isForSendItems = NO;
                    NSMutableArray *inventory = [NSMutableArray array];
                    NSArray *inventoryData = [[[jsonData objectForKey:@"OrderDetail"]objectForKey:@"InventoryArray"] objectForKey:@"InventorySelection"];
                    
                    if ([inventoryData isKindOfClass:[NSDictionary class]]) {
                        [inventory addObject:[SelectedInventoryItem getInventoryItemfrom:(NSDictionary*)inventoryData withOrderStatus:@""]];
                    }
                    else {
                        for (NSDictionary *data in inventoryData)
                            [inventory addObject:[SelectedInventoryItem getInventoryItemfrom:data withOrderStatus:@""]];
                    }
                    SendItemsViewController *sendVC = [[SendItemsViewController alloc] initWithNibName:@"SendItemsViewController" bundle:nil];
                    sendVC.array_itemOrdered = inventory;
                    sendVC.orderID = [[jsonData objectForKey:@"OrderDetail"] getTextValueForKey:@"Id"];
                    [sendVC setModalPresentationStyle:UIModalPresentationFormSheet];
                    [sendVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                    [sendVC setPreferredContentSize:CGSizeMake(550, 500)];
                    [self presentViewController:sendVC animated:NO completion:nil];
                }
                else {
                    [self refreshUserPendingOrderProducts];
                }
            }
                break;
                
            case calculateForCheckOut: {
                
                switch (self.segment_pendingOrder.selectedSegmentIndex) {
                        
                    case 0:  {
                        //Local
                        
                        NSMutableArray *inventory = [NSMutableArray array];
                        NSArray *inventoryData = [[jsonData objectForKey:@"InventoryArray"] objectForKey:@"InventorySelection"];
                        
                        if ([inventoryData isKindOfClass:[NSDictionary class]]) {
                            [inventory addObject:[SelectedInventoryItem getInventoryItemfrom:(NSDictionary*)inventoryData withOrderStatus:@""]];
                        }
                        else {
                            for (NSDictionary *data in inventoryData)
                                [inventory addObject:[SelectedInventoryItem getInventoryItemfrom:data withOrderStatus:@""]];
                        }
                        
                        for (SelectedInventoryItem *item in inventory) {
                            for (TBL_Product *productItem in [self.userSelectedPendingOrder.userOrderToProduct allObjects]) {
                                if ([productItem.productID isIdenticaleTo:item.InventoryStockId]) {
                                    item.overriddenPrice = productItem.priceOverride;
                                    item.overriddenDiscount = productItem.itemDiscount;
                                    break;
                                }
                            }
                        }
                        
                        TransactionRecord *transactionRec = [TransactionRecord getTransactionrecordFrom:[jsonData objectForKey:@"TransactionRecord"]];
                        transactionRec.CustomerName = self.userSelectedPendingOrder.userOrderToCustomer.customerFullName;
                        transactionRec.CustomerFirstName = self.userSelectedPendingOrder.userOrderToCustomer.customerFirstName;
                        transactionRec.CustomerLastName = self.userSelectedPendingOrder.userOrderToCustomer.customerLastName;
                        [self.userSelectedPendingOrder setIsNewOrder:NO];
                        [self.userSelectedPendingOrder setRecalculateStatus:@"0"];
                        //if its not loggedin user's order, remove it from other user's order and add it to Logged in user's order
                        if (![self isLoggedInUsersOrder]) {
                            
                            [[DBHelper instance] shiftOrder:self.userSelectedPendingOrder toUser:self.appUser.loggedInUser];
                        }
                        self.userSelectedPendingOrder.orderID = transactionRec.orderID;
                        self.userSelectedPendingOrder.paymentAmount = @"0.00";
                        [[DBHelper instance] saveContext];
                        
                        if (isForSaveOrder) {
                            isForSaveOrder = NO;
                            
                            [APPDELEGATE displayAlertWithTitle:nil andMessage:@"Your order has been saved and moved to the Shared List."];
                        }else if (isForSendItems) {
                            RequestResponseHandler *handler = [[RequestResponseHandler alloc] init];
                            [handler setDelegate:self];
                            [handler call_SOAP_POSTMethodWithRequest:[SoapEnvelope getSOAPRequestForOrderDetailsForOrder:self.userSelectedPendingOrder.orderID  isFromPendingOrder:@"1"] andMethodName:getOrderDetails andController:[APPDELEGATE navigationController]];
                        }
                        else {
                            CheckOutVC *checkOutVC = [[CheckOutVC alloc] initWithNibName:@"CheckOutVC" bundle:nil];
                            checkOutVC.transactionRecord = transactionRec;
                            checkOutVC.inventoryArray = inventory;
                            transactionRec.recalculateStatus = @"0";
                            [[self navigationController] pushViewController:checkOutVC animated:YES];
                        }
                    }
                        break;
                        
                    case 1: {
                        //Shared
                        
                        if (isForSaveOrder) {
                            OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                            NSMutableArray *inventory = [NSMutableArray array];
                            NSArray *inventoryData = [[jsonData objectForKey:@"InventoryArray"] objectForKey:@"InventorySelection"];
                            
                            if ([inventoryData isKindOfClass:[NSDictionary class]]) {
                                [inventory addObject:[SelectedInventoryItem getInventoryItemfrom:(NSDictionary*)inventoryData withOrderStatus:orderItem.Status]];
                            }
                            else {
                                for (NSDictionary *data in inventoryData)
                                    [inventory addObject:[SelectedInventoryItem getInventoryItemfrom:data withOrderStatus:orderItem.Status]];
                            }

                            TBL_UserOrder *local_orderObj = [[[DBHelper instance] getOrderWithOrderId:orderItem.orderID forUser:[[AppUserData sharedInstance] loggedInUser]] firstObject];
                            
                            for (TBL_Product *product in [local_orderObj.userOrderToProduct allObjects]) {
                                for (SelectedInventoryItem *item in inventory) {
                                    if ([item.refundSelectionId isEqualToString:product.refundSelectionId]) {
                                        product.selectionID = item.SelectionId;
                                        break;
                                    }
                                }
                            }

                            orderItem.recalculate = @"0";
                            orderItem.orderID = [jsonData getTextValueForKey:@"OrderId"];
                            orderItem.Status = [jsonData getTextValueForKey:@"Status"];
                            orderItem.POSOpenOrderId = [jsonData getTextValueForKey:@"OpenOrderId"];
                            isForSaveOrder = NO;
                            [self refreshPendingOrdersData];
                            if (isForAddToOrder) {
                                isForAddToOrder = NO;
                                NSString*newOrderID =[[DBHelper instance] makeNewEntryForOrder:orderItem withRecalculateStatus:orderItem.recalculate forUser:[[AppUserData sharedInstance] loggedInUser] andIsForSWAP:NO isFromPendingOrder:YES];
                                [[APPDELEGATE baseController] modifyOrderWithOrder:newOrderID];
                            }
                        }else if (isForSendItems) {
                            OrderItem *selectedOrder = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                            RequestResponseHandler *handler = [[RequestResponseHandler alloc] init];
                            [handler setDelegate:self];
                            [handler call_SOAP_POSTMethodWithRequest:[SoapEnvelope getSOAPRequestForOrderDetailsForOrder:selectedOrder.orderID  isFromPendingOrder:@"1"] andMethodName:getOrderDetails andController:[APPDELEGATE navigationController]];
                        }
                        else {
                            
                            OrderItem *orderItem = [self.array_pendingOrders_Shared objectAtIndex:selectedSharedOrderIndex];
                            TBL_UserOrder *local_orderObj = [[[DBHelper instance] getOrderWithOrderId:orderItem.orderID forUser:[[AppUserData sharedInstance] loggedInUser]] firstObject];
                            
                            NSMutableArray *inventory = [NSMutableArray array];
                            NSArray *inventoryData = [[jsonData objectForKey:@"InventoryArray"] objectForKey:@"InventorySelection"];
                            
                            if ([inventoryData isKindOfClass:[NSDictionary class]]) {
                                [inventory addObject:[SelectedInventoryItem getInventoryItemfrom:(NSDictionary*)inventoryData withOrderStatus:orderItem.Status]];
                            }
                            else {
                                for (NSDictionary *data in inventoryData)
                                    [inventory addObject:[SelectedInventoryItem getInventoryItemfrom:data withOrderStatus:orderItem.Status]];
                            }
                            
                            for (SelectedInventoryItem *item in inventory) {
                                for (TBL_Product *productItem in [local_orderObj.userOrderToProduct allObjects]) {
                                    if ([productItem.productID isIdenticaleTo:item.InventoryStockId] && [item.SelectionId isIdenticaleTo:productItem.selectionID]) {
                                        item.overriddenPrice = productItem.priceOverride;
                                        item.overriddenDiscount = productItem.itemDiscount;
                                        break;
                                    }
                                }
                            }
                            
                            TransactionRecord *transactionRec = [TransactionRecord getTransactionrecordFrom:[jsonData objectForKey:@"TransactionRecord"]];
                            transactionRec.CustomerName = local_orderObj.userOrderToCustomer.customerFullName;
                            transactionRec.CustomerFirstName = local_orderObj.userOrderToCustomer.customerFirstName;
                            transactionRec.CustomerLastName = local_orderObj.userOrderToCustomer.customerLastName;
                            
                            [local_orderObj setIsNewOrder:NO];
                            [local_orderObj setRecalculateStatus:@"0"];
                            //if its not loggedin user's order, remove it from other user's order and add it to Logged in user's order
                            
                            [[DBHelper instance] shiftOrder:local_orderObj toUser:self.appUser.loggedInUser];
                            
                            local_orderObj.orderID = transactionRec.orderID;
                            local_orderObj.paymentAmount = @"0.00";
                            local_orderObj.isShipOrderApplied = [transactionRec isShipping];
                            [[DBHelper instance] saveContext];
                            
                            CheckOutVC *checkOutVC = [[CheckOutVC alloc] initWithNibName:@"CheckOutVC" bundle:nil];
                            checkOutVC.transactionRecord = transactionRec;
                            checkOutVC.inventoryArray = inventory;
                            transactionRec.recalculateStatus = @"0";
                            [[self navigationController] pushViewController:checkOutVC animated:YES];
                        }
                    }
                        break;
                        
                    default:
                        break;
                }
            }
                break;
                
            case closeOpenOrder: {
                
                switch (self.segment_pendingOrder.selectedSegmentIndex) {
                        
                    case 1: {
                        //Shared
                        
                        if (selectedSharedOrderIndex >= 0) {
                            [self.array_pendingOrders_Shared removeObjectAtIndex:selectedSharedOrderIndex];
                            [self.tableView_pendingOrders deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:selectedSharedOrderIndex inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
                        }
                        
                        selectedSharedOrderIndex = -1;
                        self.selectedPendingOrderID = @"";
                        self.userSelectedPendingOrder = nil;
                        
                        [self.label_customerName setText:@""];
                        [self.label_membershipClub setText:@""];
                        [self.label_customerNotes setText:@""];
                        [self refreshUserPendingOrderProducts];
                    }
                        break;
                        
                    default:
                        break;
                }
            }
                break;
                
            case deleteOrder: {
                
                switch (self.segment_pendingOrder.selectedSegmentIndex) {
                        
                    case 0:  {
                        //Local
                        
                        if (isDeleteAllOrder) {
                            
                            TBL_UserOrder *pendingOrder = [self.array_pendingOrders_Local objectAtIndex:currentOrderToBeDeleted];
                            [[DBHelper instance] deleteOrder:pendingOrder forUser:self.appUser.loggedInUser];
                            [self refreshPendingOrdersData];
                            
                            [self deleteAllUnpaidOrders];
                        }
                        else {
                            
                            [[DBHelper instance] deleteOrder:self.userSelectedPendingOrder forUser:self.appUser.loggedInUser];
                            [self refreshPendingOrdersData];
                        }
                    }
                        break;
                        
                    case 1: {
                        //Shared
                        
                        if (selectedSharedOrderIndex >= 0) {
                            [self.array_pendingOrders_Shared removeObjectAtIndex:selectedSharedOrderIndex];
                            [self.tableView_pendingOrders deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:selectedSharedOrderIndex inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
                        }
                        
                        selectedSharedOrderIndex = -1;
                        self.selectedPendingOrderID = @"";
                        self.userSelectedPendingOrder = nil;
                        [self.label_customerName setText:@""];
                        [self.label_membershipClub setText:@""];
                        [self.label_customerNotes setText:@""];
                        [self refreshUserPendingOrderProducts];
                    }
                        break;
                        
                    default:
                        break;
                }
            }
                break;
                
            default:
                break;
        }
    }
    else {
        
        if ((methodType == deleteOrder) && isDeleteAllOrder)
            [self deleteAllUnpaidOrders];
        
        [APPDELEGATE displayAlertWithTitle:@"" andMessage:responseObj.responseMessage];
    }
}

@end
